from wx import App

from wxfb.layout import wxFB_dialog_options


class dialogOptions(wxFB_dialog_options):
    def __init__(self, parent, xBuffer, RPS, plotRatio):

        # Initialize parent class
        wxFB_dialog_options.__init__(self, parent)

        self.parent = parent

        # Set the values from parent
        self.spinCtrl_XBuffer.SetValue(xBuffer)
        self.choice_RPS.SetStringSelection(str(RPS))
        self.choice_PlotRatio.SetStringSelection(plotRatio)


class dialogOptions_test(App):
    '''Class to test dialogOptions.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close after tests run'''
        super(dialogOptions_test, self).__init__()

        # Create dialog with sample data
        dialog = dialogOptions(
            parent=None,
            xBuffer=60,
            RPS=1,
            plotRatio="x1",
            )

        # Depending on auto_close, dialog needs opened differently
        if auto_close:
            dialog.Show()
        else:
            dialog.ShowModal()
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogOptions_test(False).MainLoop()
