#! python3
'''Find Python2 on Windows and suggest a new PATH'''
from os import environ
from os.path import dirname, join
from subprocess import PIPE, run


if __name__ == '__main__':
    result = run(
        ('py', '-2', '-c', 'from sys import executable; print(executable)'),
        stdout=PIPE,
    )
    result.check_returncode()
    path = [environ['Path'], ]
    for i in ('', 'Scripts'):
        path.append('{};'.format(join(dirname(result.stdout.decode()), i)))
    print(''.join(path))
