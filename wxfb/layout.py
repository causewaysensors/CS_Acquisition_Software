# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Dec 21 2016)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

from panel_CalibrateLog import panel_CalibrateLog
from panel_CalibrateGraph import panel_CalibrateGraph
from panel_AcquisitionGraph import panel_AcquisitionGraph
from panel_AcquisitionLog import panel_AcquisitionLog
from panel_ViewerList import panel_ViewerList
from panel_ViewerPlot import panel_ViewerPlot
import wx
import wx.xrc
import wx.grid
import wx.animate

###########################################################################
## Class wxFB_dialog_calibrateChip
###########################################################################

class wxFB_dialog_calibrateChip ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Calibration Results - Chip", pos = wx.DefaultPosition, size = wx.Size( 608,563 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		fgSizer72 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer72.AddGrowableCol( 0 )
		fgSizer72.SetFlexibleDirection( wx.BOTH )
		fgSizer72.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.textCtrl_fileSetupInfo = wx.TextCtrl( self, wx.ID_ANY, u"Hold ctrl and select the flags to calibrate from", wx.DefaultPosition, wx.Size( -1,95 ), wx.TE_MULTILINE|wx.TE_NO_VSCROLL|wx.TE_READONLY|wx.TE_WORDWRAP|wx.NO_BORDER )
		self.textCtrl_fileSetupInfo.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )

		fgSizer72.Add( self.textCtrl_fileSetupInfo, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 20 )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_250.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer72.Add( self.m_bitmap2, 0, wx.ALL, 5 )


		bSizer41.Add( fgSizer72, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 10 )

		self.listpan = panel_CalibrateLog( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer41.Add( self.listpan, 1, wx.EXPAND |wx.ALL, 5 )

		fgSizer241 = wx.FlexGridSizer( 1, 4, 0, 10 )
		fgSizer241.AddGrowableCol( 0 )
		fgSizer241.AddGrowableCol( 3 )
		fgSizer241.SetFlexibleDirection( wx.BOTH )
		fgSizer241.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer241.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_OK = wx.Button( self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer241.Add( self.btn_OK, 0, wx.ALL, 10 )

		self.btn_Cancel = wx.Button( self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer241.Add( self.btn_Cancel, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )


		fgSizer241.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		bSizer41.Add( fgSizer241, 0, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.btn_OK.Bind( wx.EVT_BUTTON, self.OnButton_MoveClamp )
		self.btn_Cancel.Bind( wx.EVT_BUTTON, self.OnButton_SetAndExit )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnButton_MoveClamp( self, event ):
		event.Skip()

	def OnButton_SetAndExit( self, event ):
		event.Skip()


###########################################################################
## Class wxFB_dialog_calibrateSensorsResults
###########################################################################

class wxFB_dialog_calibrateSensorsResults ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Calibration Results - Sensors", pos = wx.DefaultPosition, size = wx.Size( 541,438 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		fgSizer39 = wx.FlexGridSizer( 6, 1, 0, 0 )
		fgSizer39.AddGrowableCol( 0 )
		fgSizer39.AddGrowableRow( 4 )
		fgSizer39.SetFlexibleDirection( wx.BOTH )
		fgSizer39.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_250.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer39.Add( self.m_bitmap2, 0, wx.ALIGN_CENTER|wx.ALL, 10 )

		fgSizer431 = wx.FlexGridSizer( 1, 2, 0, 30 )
		fgSizer431.SetFlexibleDirection( wx.BOTH )
		fgSizer431.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText581 = wx.StaticText( self, wx.ID_ANY, u"Status", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText581.Wrap( -1 )
		self.m_staticText581.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fgSizer431.Add( self.m_staticText581, 0, wx.ALL, 5 )

		self.text_Status = wx.StaticText( self, wx.ID_ANY, u"Failed High", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_Status.Wrap( -1 )
		self.text_Status.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fgSizer431.Add( self.text_Status, 0, wx.ALIGN_CENTER|wx.ALL, 5 )


		fgSizer39.Add( fgSizer431, 1, wx.ALIGN_CENTER, 5 )

		fgSizer43 = wx.FlexGridSizer( 2, 3, 0, 30 )
		fgSizer43.SetFlexibleDirection( wx.BOTH )
		fgSizer43.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText58 = wx.StaticText( self, wx.ID_ANY, u"Sensor 1", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText58.Wrap( -1 )
		fgSizer43.Add( self.m_staticText58, 0, wx.ALL, 5 )

		self.text_Integration1 = wx.StaticText( self, wx.ID_ANY, u"125ms", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_Integration1.Wrap( -1 )
		fgSizer43.Add( self.text_Integration1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.text_Gain = wx.StaticText( self, wx.ID_ANY, u"x8", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_Gain.Wrap( -1 )
		fgSizer43.Add( self.text_Gain, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_staticText59 = wx.StaticText( self, wx.ID_ANY, u"Sensor 2", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText59.Wrap( -1 )
		fgSizer43.Add( self.m_staticText59, 0, wx.ALL, 5 )

		self.text_Integration2 = wx.StaticText( self, wx.ID_ANY, u"250ms", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_Integration2.Wrap( -1 )
		fgSizer43.Add( self.text_Integration2, 0, wx.ALIGN_CENTER|wx.ALL, 5 )


		fgSizer39.Add( fgSizer43, 1, wx.ALIGN_CENTER, 5 )

		self.grid_CalibrateSensors = wx.grid.Grid( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )

		# Grid
		self.grid_CalibrateSensors.CreateGrid( 4, 3 )
		self.grid_CalibrateSensors.EnableEditing( False )
		self.grid_CalibrateSensors.EnableGridLines( True )
		self.grid_CalibrateSensors.EnableDragGridSize( False )
		self.grid_CalibrateSensors.SetMargins( 0, 0 )

		# Columns
		self.grid_CalibrateSensors.EnableDragColMove( False )
		self.grid_CalibrateSensors.EnableDragColSize( True )
		self.grid_CalibrateSensors.SetColLabelSize( 30 )
		self.grid_CalibrateSensors.SetColLabelValue( 0, u"8Hz" )
		self.grid_CalibrateSensors.SetColLabelValue( 1, u"4Hz" )
		self.grid_CalibrateSensors.SetColLabelValue( 2, u"2Hz" )
		self.grid_CalibrateSensors.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		# Rows
		self.grid_CalibrateSensors.EnableDragRowSize( True )
		self.grid_CalibrateSensors.SetRowLabelSize( 80 )
		self.grid_CalibrateSensors.SetRowLabelValue( 0, u"x1" )
		self.grid_CalibrateSensors.SetRowLabelValue( 1, u"x8" )
		self.grid_CalibrateSensors.SetRowLabelValue( 2, u"x16" )
		self.grid_CalibrateSensors.SetRowLabelValue( 3, u"x111" )
		self.grid_CalibrateSensors.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		# Label Appearance

		# Cell Defaults
		self.grid_CalibrateSensors.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
		fgSizer39.Add( self.grid_CalibrateSensors, 0, wx.ALIGN_CENTER|wx.ALL, 5 )


		fgSizer39.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		fgSizer24 = wx.FlexGridSizer( 1, 3, 0, 10 )
		fgSizer24.AddGrowableCol( 0 )
		fgSizer24.AddGrowableCol( 2 )
		fgSizer24.SetFlexibleDirection( wx.BOTH )
		fgSizer24.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_OK = wx.Button( self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_OK, 0, wx.ALL, 10 )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer39.Add( fgSizer24, 1, wx.EXPAND, 5 )


		bSizer41.Add( fgSizer39, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class wxFB_dialog_calibrateChipResults
###########################################################################

class wxFB_dialog_calibrateChipResults ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Calibration Results - Chip", pos = wx.DefaultPosition, size = wx.Size( 608,563 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		fgSizer39 = wx.FlexGridSizer( 4, 1, 0, 0 )
		fgSizer39.AddGrowableCol( 0 )
		fgSizer39.AddGrowableRow( 1 )
		fgSizer39.SetFlexibleDirection( wx.BOTH )
		fgSizer39.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_250.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer39.Add( self.m_bitmap2, 0, wx.ALIGN_CENTER|wx.ALL, 10 )

		self.graphpan = panel_CalibrateGraph( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer39.Add( self.graphpan, 1, wx.EXPAND |wx.ALL, 5 )

		fgSizer391 = wx.FlexGridSizer( 3, 2, 0, 20 )
		fgSizer391.SetFlexibleDirection( wx.BOTH )
		fgSizer391.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText71 = wx.StaticText( self, wx.ID_ANY, u"Slope", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText71.Wrap( -1 )
		fgSizer391.Add( self.m_staticText71, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.text_Slope = wx.StaticText( self, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_Slope.Wrap( -1 )
		fgSizer391.Add( self.text_Slope, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_staticText75 = wx.StaticText( self, wx.ID_ANY, u"Y Intercept", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText75.Wrap( -1 )
		fgSizer391.Add( self.m_staticText75, 0, wx.ALL, 5 )

		self.text_YIntercept = wx.StaticText( self, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_YIntercept.Wrap( -1 )
		fgSizer391.Add( self.text_YIntercept, 0, wx.ALL, 5 )

		self.m_staticText72 = wx.StaticText( self, wx.ID_ANY, u"R2", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText72.Wrap( -1 )
		fgSizer391.Add( self.m_staticText72, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.text_R2 = wx.StaticText( self, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_R2.Wrap( -1 )
		fgSizer391.Add( self.text_R2, 0, wx.ALIGN_CENTER|wx.ALL, 5 )


		fgSizer39.Add( fgSizer391, 1, wx.ALIGN_CENTER, 5 )

		fgSizer24 = wx.FlexGridSizer( 1, 3, 0, 10 )
		fgSizer24.AddGrowableCol( 0 )
		fgSizer24.AddGrowableCol( 2 )
		fgSizer24.SetFlexibleDirection( wx.BOTH )
		fgSizer24.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_OK = wx.Button( self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_OK, 0, wx.ALL, 10 )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer39.Add( fgSizer24, 1, wx.EXPAND, 5 )


		bSizer41.Add( fgSizer39, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class wxFB_dialog_clampCustom
###########################################################################

class wxFB_dialog_clampCustom ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Custom Movement Clamp", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		fgSizer72 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer72.AddGrowableCol( 0 )
		fgSizer72.SetFlexibleDirection( wx.BOTH )
		fgSizer72.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.textCtrl_fileSetupInfo = wx.TextCtrl( self, wx.ID_ANY, u"Custom movement of the clamp", wx.DefaultPosition, wx.Size( -1,95 ), wx.TE_MULTILINE|wx.TE_NO_VSCROLL|wx.TE_READONLY|wx.TE_WORDWRAP|wx.NO_BORDER )
		self.textCtrl_fileSetupInfo.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )

		fgSizer72.Add( self.textCtrl_fileSetupInfo, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 20 )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_250.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer72.Add( self.m_bitmap2, 0, wx.ALL, 5 )


		bSizer41.Add( fgSizer72, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 10 )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		fgSizer3 = wx.FlexGridSizer( 6, 3, 10, 20 )
		fgSizer3.AddGrowableCol( 1 )
		fgSizer3.AddGrowableRow( 5 )
		fgSizer3.SetFlexibleDirection( wx.BOTH )
		fgSizer3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText5 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Motion", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		fgSizer3.Add( self.m_staticText5, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		choice_DirectionChoices = [ u"In", u"Out", u"Cycle" ]
		self.choice_Direction = wx.Choice( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_DirectionChoices, 0 )
		self.choice_Direction.SetSelection( 2 )
		fgSizer3.Add( self.choice_Direction, 0, wx.ALL|wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText64 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Cycles", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText64.Wrap( -1 )
		fgSizer3.Add( self.m_staticText64, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.spinCtrl_NumberCycles = wx.SpinCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 1, 100, 10 )
		fgSizer3.Add( self.spinCtrl_NumberCycles, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText76 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"(1-100)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText76.Wrap( -1 )
		fgSizer3.Add( self.m_staticText76, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_staticText6 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Level", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )
		fgSizer3.Add( self.m_staticText6, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		choice_RangeChoices = [ u"Low", u"High" ]
		self.choice_Range = wx.Choice( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_RangeChoices, 0 )
		self.choice_Range.SetSelection( 0 )
		fgSizer3.Add( self.choice_Range, 0, wx.ALL|wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText39 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Speed", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText39.Wrap( -1 )
		fgSizer3.Add( self.m_staticText39, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.spinCtrl_Speed = wx.SpinCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 1, 255, 255 )
		fgSizer3.Add( self.spinCtrl_Speed, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText73 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"(1-255)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText73.Wrap( -1 )
		fgSizer3.Add( self.m_staticText73, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_staticText66 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Time - In (ms)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText66.Wrap( -1 )
		fgSizer3.Add( self.m_staticText66, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.spinCtrl_TimeIn = wx.SpinCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 1, 10000, 5000 )
		fgSizer3.Add( self.spinCtrl_TimeIn, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText74 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"(1-10000)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText74.Wrap( -1 )
		fgSizer3.Add( self.m_staticText74, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_staticText72 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Time - Out (ms)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText72.Wrap( -1 )
		fgSizer3.Add( self.m_staticText72, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.spinCtrl_TimeOut = wx.SpinCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 1, 10000, 2000 )
		fgSizer3.Add( self.spinCtrl_TimeOut, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText75 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"(1-10000)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText75.Wrap( -1 )
		fgSizer3.Add( self.m_staticText75, 0, wx.ALIGN_CENTER|wx.ALL, 5 )


		sbSizer2.Add( fgSizer3, 1, wx.ALL|wx.EXPAND, 10 )

		fgSizer241 = wx.FlexGridSizer( 1, 5, 0, 10 )
		fgSizer241.AddGrowableCol( 0 )
		fgSizer241.AddGrowableCol( 3 )
		fgSizer241.SetFlexibleDirection( wx.BOTH )
		fgSizer241.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer241.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_MoveClamp = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Move Clamp", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer241.Add( self.btn_MoveClamp, 0, wx.ALL, 10 )

		self.btn_SetAndExit = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Set and Exit", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer241.Add( self.btn_SetAndExit, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.btn_Exit = wx.Button( sbSizer2.GetStaticBox(), wx.ID_CANCEL, u"Exit", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer241.Add( self.btn_Exit, 0, wx.ALL, 10 )


		fgSizer241.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		sbSizer2.Add( fgSizer241, 0, wx.EXPAND, 5 )


		bSizer41.Add( sbSizer2, 1, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()
		bSizer41.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.choice_Direction.Bind( wx.EVT_CHOICE, self.OnChoice_Direction )
		self.btn_MoveClamp.Bind( wx.EVT_BUTTON, self.OnButton_MoveClamp )
		self.btn_SetAndExit.Bind( wx.EVT_BUTTON, self.OnButton_SetAndExit )
		self.btn_Exit.Bind( wx.EVT_BUTTON, self.OnButton_Exit )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnChoice_Direction( self, event ):
		event.Skip()

	def OnButton_MoveClamp( self, event ):
		event.Skip()

	def OnButton_SetAndExit( self, event ):
		event.Skip()

	def OnButton_Exit( self, event ):
		event.Skip()


###########################################################################
## Class wxFB_dialog_clampPrompt
###########################################################################

class wxFB_dialog_clampPrompt ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Move clamp?", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		fgSizer46 = wx.FlexGridSizer( 5, 1, 0, 0 )
		fgSizer46.AddGrowableCol( 0 )
		fgSizer46.AddGrowableRow( 3 )
		fgSizer46.SetFlexibleDirection( wx.BOTH )
		fgSizer46.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.textCtrl_fileSetupInfo2 = wx.TextCtrl( self, wx.ID_ANY, u"Checks", wx.DefaultPosition, wx.Size( -1,-1 ), wx.TE_CENTRE|wx.TE_NO_VSCROLL|wx.TE_READONLY|wx.TE_WORDWRAP|wx.NO_BORDER )
		self.textCtrl_fileSetupInfo2.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )
		self.textCtrl_fileSetupInfo2.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )

		fgSizer46.Add( self.textCtrl_fileSetupInfo2, 0, wx.ALL|wx.EXPAND, 5 )

		self.textCtrl_fileSetupInfo = wx.TextCtrl( self, wx.ID_ANY, u"- Is the clamp is in correct position?", wx.DefaultPosition, wx.Size( -1,-1 ), wx.TE_CENTRE|wx.TE_NO_VSCROLL|wx.TE_READONLY|wx.TE_WORDWRAP|wx.NO_BORDER )
		self.textCtrl_fileSetupInfo.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )

		fgSizer46.Add( self.textCtrl_fileSetupInfo, 0, wx.ALL|wx.EXPAND, 5 )

		self.textCtrl_fileSetupInfo1 = wx.TextCtrl( self, wx.ID_ANY, u"- Is the cuvette drained?", wx.DefaultPosition, wx.Size( -1,-1 ), wx.TE_CENTRE|wx.TE_NO_VSCROLL|wx.TE_READONLY|wx.TE_WORDWRAP|wx.NO_BORDER )
		self.textCtrl_fileSetupInfo1.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )

		fgSizer46.Add( self.textCtrl_fileSetupInfo1, 0, wx.ALL|wx.EXPAND, 5 )


		fgSizer46.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		fgSizer241 = wx.FlexGridSizer( 1, 5, 0, 10 )
		fgSizer241.AddGrowableCol( 0 )
		fgSizer241.AddGrowableCol( 3 )
		fgSizer241.SetFlexibleDirection( wx.BOTH )
		fgSizer241.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer241.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_MoveIn = wx.Button( self, wx.ID_ANY, u"Move In", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer241.Add( self.btn_MoveIn, 0, wx.ALL, 10 )

		self.btn_MoveOut = wx.Button( self, wx.ID_ANY, u"Move Out", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer241.Add( self.btn_MoveOut, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.btn_Continue = wx.Button( self, wx.ID_OK, u"Continue", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer241.Add( self.btn_Continue, 0, wx.ALL, 10 )


		fgSizer241.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer46.Add( fgSizer241, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer41.Add( fgSizer46, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()
		bSizer41.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.btn_MoveIn.Bind( wx.EVT_BUTTON, self.OnButton_MoveClampIn )
		self.btn_MoveOut.Bind( wx.EVT_BUTTON, self.OnButton_MoveClampOut )
		self.btn_Continue.Bind( wx.EVT_BUTTON, self.OnButton_Continue )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnButton_MoveClampIn( self, event ):
		event.Skip()

	def OnButton_MoveClampOut( self, event ):
		event.Skip()

	def OnButton_Continue( self, event ):
		event.Skip()


###########################################################################
## Class wxFB_dialog_fileSetup
###########################################################################

class wxFB_dialog_fileSetup ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"File Setup", pos = wx.DefaultPosition, size = wx.Size( 1078,740 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		fgSizer72 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer72.AddGrowableCol( 0 )
		fgSizer72.SetFlexibleDirection( wx.BOTH )
		fgSizer72.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.textCtrl_fileSetupInfo = wx.TextCtrl( self, wx.ID_ANY, u"Enter the file details for the acquisition\n- Name should only contain letters, numbers, spaces, dashes and underscores\n- Tags, users and sample IDs can be used to filter the results database\n- Multiple tags can be selected", wx.DefaultPosition, wx.Size( -1,95 ), wx.TE_MULTILINE|wx.TE_NO_VSCROLL|wx.TE_READONLY|wx.TE_WORDWRAP|wx.NO_BORDER )
		self.textCtrl_fileSetupInfo.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )

		fgSizer72.Add( self.textCtrl_fileSetupInfo, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 20 )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_250.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer72.Add( self.m_bitmap2, 0, wx.ALL, 5 )


		bSizer41.Add( fgSizer72, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 10 )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		bSizer42 = wx.BoxSizer( wx.VERTICAL )

		fgSizer3 = wx.FlexGridSizer( 3, 7, 0, 0 )
		fgSizer3.AddGrowableCol( 1 )
		fgSizer3.AddGrowableCol( 5 )
		fgSizer3.AddGrowableRow( 1 )
		fgSizer3.AddGrowableRow( 2 )
		fgSizer3.SetFlexibleDirection( wx.BOTH )
		fgSizer3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText5 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		fgSizer3.Add( self.m_staticText5, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrl_Name = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		fgSizer3.Add( self.textCtrl_Name, 1, wx.ALL|wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 30, 0), 1, wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText6 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Sample ID", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )
		fgSizer3.Add( self.m_staticText6, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		listBox_SampleIDChoices = []
		self.listBox_SampleID = wx.ListBox( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, listBox_SampleIDChoices, wx.LB_NEEDED_SB )
		fgSizer3.Add( self.listBox_SampleID, 1, wx.ALL|wx.EXPAND, 5 )

		fgSizer73 = wx.FlexGridSizer( 4, 1, 0, 0 )
		fgSizer73.AddGrowableRow( 0 )
		fgSizer73.AddGrowableRow( 3 )
		fgSizer73.SetFlexibleDirection( wx.BOTH )
		fgSizer73.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer73.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_AddSampleID = wx.BitmapButton( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.Bitmap( u"assets/images/Add.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.Size( 24,24 ), wx.BU_AUTODRAW )
		fgSizer73.Add( self.btn_AddSampleID, 0, wx.ALL, 5 )

		self.btn_RemoveSampleID = wx.BitmapButton( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.Bitmap( u"assets/images/Remove.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.Size( 24,24 ), wx.BU_AUTODRAW )
		fgSizer73.Add( self.btn_RemoveSampleID, 0, wx.ALL, 5 )


		fgSizer73.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer3.Add( fgSizer73, 1, wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText7 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Tags", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		fgSizer3.Add( self.m_staticText7, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		listBox_TagsChoices = []
		self.listBox_Tags = wx.ListBox( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), listBox_TagsChoices, wx.LB_MULTIPLE|wx.LB_NEEDED_SB )
		fgSizer3.Add( self.listBox_Tags, 1, wx.ALL|wx.EXPAND, 5 )

		fgSizer732 = wx.FlexGridSizer( 4, 1, 0, 0 )
		fgSizer732.AddGrowableRow( 0 )
		fgSizer732.AddGrowableRow( 3 )
		fgSizer732.SetFlexibleDirection( wx.BOTH )
		fgSizer732.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer732.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_AddTag = wx.BitmapButton( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.Bitmap( u"assets/images/Add.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.Size( 24,24 ), wx.BU_AUTODRAW )
		fgSizer732.Add( self.btn_AddTag, 0, wx.ALL, 5 )

		self.btn_RemoveTag = wx.BitmapButton( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.Bitmap( u"assets/images/Remove.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.Size( 24,24 ), wx.BU_AUTODRAW )
		fgSizer732.Add( self.btn_RemoveTag, 0, wx.ALL, 5 )


		fgSizer732.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer3.Add( fgSizer732, 1, wx.EXPAND, 5 )

		self.m_staticText8 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Users", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText8.Wrap( -1 )
		fgSizer3.Add( self.m_staticText8, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		listBox_UsersChoices = []
		self.listBox_Users = wx.ListBox( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), listBox_UsersChoices, wx.LB_MULTIPLE|wx.LB_NEEDED_SB )
		fgSizer3.Add( self.listBox_Users, 1, wx.ALL|wx.EXPAND, 5 )

		fgSizer731 = wx.FlexGridSizer( 4, 1, 0, 0 )
		fgSizer731.AddGrowableRow( 0 )
		fgSizer731.AddGrowableRow( 3 )
		fgSizer731.SetFlexibleDirection( wx.BOTH )
		fgSizer731.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer731.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_AddUser = wx.BitmapButton( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.Bitmap( u"assets/images/Add.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.Size( 24,24 ), wx.BU_AUTODRAW )
		fgSizer731.Add( self.btn_AddUser, 0, wx.ALL, 5 )

		self.btn_RemoveUser = wx.BitmapButton( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.Bitmap( u"assets/images/Remove.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.Size( 24,24 ), wx.BU_AUTODRAW )
		fgSizer731.Add( self.btn_RemoveUser, 0, wx.ALL, 5 )


		fgSizer731.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer3.Add( fgSizer731, 1, wx.EXPAND, 5 )


		fgSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText9 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Notes", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )
		fgSizer3.Add( self.m_staticText9, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrl_Notes = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( -1,-1 ), wx.TE_MULTILINE )
		fgSizer3.Add( self.textCtrl_Notes, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer42.Add( fgSizer3, 1, wx.ALL|wx.EXPAND, 10 )

		fgSizer24 = wx.FlexGridSizer( 1, 4, 0, 10 )
		fgSizer24.AddGrowableCol( 0 )
		fgSizer24.AddGrowableCol( 3 )
		fgSizer24.SetFlexibleDirection( wx.BOTH )
		fgSizer24.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_OK = wx.Button( sbSizer2.GetStaticBox(), wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_OK, 0, wx.ALL, 10 )

		self.btn_Cancel = wx.Button( sbSizer2.GetStaticBox(), wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_Cancel, 0, wx.ALL, 10 )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		bSizer42.Add( fgSizer24, 0, wx.EXPAND, 5 )


		sbSizer2.Add( bSizer42, 1, wx.EXPAND, 5 )


		bSizer41.Add( sbSizer2, 1, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.btn_AddSampleID.Bind( wx.EVT_BUTTON, self.OnButton_AddSampleID )
		self.btn_RemoveSampleID.Bind( wx.EVT_BUTTON, self.OnButton_RemoveSampleID )
		self.btn_AddTag.Bind( wx.EVT_BUTTON, self.OnButton_AddTag )
		self.btn_RemoveTag.Bind( wx.EVT_BUTTON, self.OnButton_RemoveTag )
		self.btn_AddUser.Bind( wx.EVT_BUTTON, self.OnButton_AddUser )
		self.btn_RemoveUser.Bind( wx.EVT_BUTTON, self.OnButton_RemoveUser )
		self.btn_OK.Bind( wx.EVT_BUTTON, self.OnButton_OK )
		self.btn_Cancel.Bind( wx.EVT_BUTTON, self.OnButton_Cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnButton_AddSampleID( self, event ):
		event.Skip()

	def OnButton_RemoveSampleID( self, event ):
		event.Skip()

	def OnButton_AddTag( self, event ):
		event.Skip()

	def OnButton_RemoveTag( self, event ):
		event.Skip()

	def OnButton_AddUser( self, event ):
		event.Skip()

	def OnButton_RemoveUser( self, event ):
		event.Skip()

	def OnButton_OK( self, event ):
		event.Skip()

	def OnButton_Cancel( self, event ):
		event.Skip()


###########################################################################
## Class wxFB_dialog_flag
###########################################################################

class wxFB_dialog_flag ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Flag Details", pos = wx.DefaultPosition, size = wx.Size( 570,552 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		fgSizer72 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer72.AddGrowableCol( 0 )
		fgSizer72.SetFlexibleDirection( wx.BOTH )
		fgSizer72.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.textCtrl_fileSetupInfo = wx.TextCtrl( self, wx.ID_ANY, u"Enter the details of the flag", wx.DefaultPosition, wx.Size( -1,95 ), wx.TE_MULTILINE|wx.TE_NO_VSCROLL|wx.TE_READONLY|wx.TE_WORDWRAP|wx.NO_BORDER )
		self.textCtrl_fileSetupInfo.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )

		fgSizer72.Add( self.textCtrl_fileSetupInfo, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 20 )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_250.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer72.Add( self.m_bitmap2, 0, wx.ALL, 5 )


		bSizer41.Add( fgSizer72, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 10 )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		bSizer29 = wx.BoxSizer( wx.HORIZONTAL )


		bSizer29.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText71 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Flag Time =   ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText71.Wrap( -1 )
		bSizer29.Add( self.m_staticText71, 0, wx.ALL, 5 )

		self.text_time = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_time.Wrap( -1 )
		bSizer29.Add( self.text_time, 0, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer29.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		sbSizer2.Add( bSizer29, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, 10 )

		fgSizer3 = wx.FlexGridSizer( 3, 2, 10, 20 )
		fgSizer3.AddGrowableCol( 1 )
		fgSizer3.AddGrowableRow( 0 )
		fgSizer3.SetFlexibleDirection( wx.BOTH )
		fgSizer3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText5 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Description", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		fgSizer3.Add( self.m_staticText5, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrl_Description = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		fgSizer3.Add( self.textCtrl_Description, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText6 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Baseline", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )
		fgSizer3.Add( self.m_staticText6, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		choice_BaselineChoices = [ u"True", u"False" ]
		self.choice_Baseline = wx.Choice( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_BaselineChoices, 0 )
		self.choice_Baseline.SetSelection( 1 )
		fgSizer3.Add( self.choice_Baseline, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText39 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Colour", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText39.Wrap( -1 )
		fgSizer3.Add( self.m_staticText39, 0, wx.ALL, 5 )

		choice_ColourChoices = [ u"Red", u"Blue", u"Green", u"Black" ]
		self.choice_Colour = wx.Choice( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_ColourChoices, 0 )
		self.choice_Colour.SetSelection( 0 )
		fgSizer3.Add( self.choice_Colour, 0, wx.ALL|wx.EXPAND, 5 )


		sbSizer2.Add( fgSizer3, 1, wx.ALL|wx.EXPAND, 10 )

		fgSizer24 = wx.FlexGridSizer( 1, 4, 0, 10 )
		fgSizer24.AddGrowableCol( 0 )
		fgSizer24.AddGrowableCol( 3 )
		fgSizer24.SetFlexibleDirection( wx.BOTH )
		fgSizer24.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_OK = wx.Button( sbSizer2.GetStaticBox(), wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_OK, 0, wx.ALL, 10 )

		self.btn_Cancel = wx.Button( sbSizer2.GetStaticBox(), wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_Cancel, 0, wx.ALL, 10 )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		sbSizer2.Add( fgSizer24, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer41.Add( sbSizer2, 1, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.btn_OK.Bind( wx.EVT_BUTTON, self.OnButton_DialogFlag_Ok )
		self.btn_Cancel.Bind( wx.EVT_BUTTON, self.OnButton_DialogFlag_Cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnButton_DialogFlag_Ok( self, event ):
		event.Skip()

	def OnButton_DialogFlag_Cancel( self, event ):
		event.Skip()


###########################################################################
## Class wxFB_dialog_loading
###########################################################################

class wxFB_dialog_loading ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Loading", pos = wx.DefaultPosition, size = wx.Size( 300,150 ), style = wx.CAPTION|wx.STAY_ON_TOP )

		self.SetSizeHintsSz( wx.Size( 300,150 ), wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		bSizer16 = wx.BoxSizer( wx.HORIZONTAL )

		self.message = wx.StaticText( self, wx.ID_ANY, u"Connecting sensor \nPlease Wait", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.message.Wrap( -1 )
		bSizer16.Add( self.message, 0, wx.ALIGN_CENTER, 20 )

		self.m_animCtrl1 = wx.animate.AnimationCtrl( self, wx.ID_ANY, wx.animate.NullAnimation, wx.DefaultPosition, wx.DefaultSize, wx.animate.AC_DEFAULT_STYLE )
		self.m_animCtrl1.LoadFile( u"assets\\gifs\\CS_symbol_loading.gif" )

		self.m_animCtrl1.SetInactiveBitmap( wx.NullBitmap )
		self.m_animCtrl1.Play()
		bSizer16.Add( self.m_animCtrl1, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT, 20 )


		bSizer41.Add( bSizer16, 1, wx.ALIGN_CENTER|wx.ALL, 10 )

		self.progress = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL )
		self.progress.SetValue( 0 )
		bSizer41.Add( self.progress, 0, wx.BOTTOM|wx.EXPAND|wx.LEFT|wx.RIGHT, 10 )


		self.SetSizer( bSizer41 )
		self.Layout()

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class wxFB_dialog_options
###########################################################################

class wxFB_dialog_options ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Options", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		bSizer49 = wx.BoxSizer( wx.VERTICAL )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_250.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer49.Add( self.m_bitmap2, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 10 )


		bSizer41.Add( bSizer49, 0, wx.EXPAND, 5 )

		sbSizer21 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Graph" ), wx.HORIZONTAL )

		self.m_staticText51 = wx.StaticText( sbSizer21.GetStaticBox(), wx.ID_ANY, u"Step size in time (x buffer) =", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText51.Wrap( -1 )
		self.m_staticText51.Enable( False )

		sbSizer21.Add( self.m_staticText51, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 10 )

		self.spinCtrl_XBuffer = wx.SpinCtrl( sbSizer21.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10, 10 )
		self.spinCtrl_XBuffer.Enable( False )

		sbSizer21.Add( self.spinCtrl_XBuffer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 10 )

		self.m_staticText61 = wx.StaticText( sbSizer21.GetStaticBox(), wx.ID_ANY, u"secs", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText61.Wrap( -1 )
		self.m_staticText61.Enable( False )

		sbSizer21.Add( self.m_staticText61, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 10 )


		bSizer41.Add( sbSizer21, 0, wx.ALL|wx.EXPAND, 10 )

		sbSizer211 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Data" ), wx.HORIZONTAL )

		self.m_staticText512 = wx.StaticText( sbSizer211.GetStaticBox(), wx.ID_ANY, u"RPS =", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText512.Wrap( -1 )
		self.m_staticText512.Enable( False )

		sbSizer211.Add( self.m_staticText512, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 10 )

		choice_RPSChoices = [ u"1", u"2", u"4" ]
		self.choice_RPS = wx.Choice( sbSizer211.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_RPSChoices, 0 )
		self.choice_RPS.SetSelection( 0 )
		self.choice_RPS.Enable( False )

		sbSizer211.Add( self.choice_RPS, 0, wx.ALL, 5 )


		bSizer41.Add( sbSizer211, 0, wx.ALL|wx.EXPAND, 10 )

		sbSizer22 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Performance" ), wx.HORIZONTAL )

		self.m_staticText52 = wx.StaticText( sbSizer22.GetStaticBox(), wx.ID_ANY, u"Plot ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText52.Wrap( -1 )
		self.m_staticText52.Enable( False )

		sbSizer22.Add( self.m_staticText52, 0, wx.ALL, 10 )

		choice_PlotRatioChoices = [ u"x1", u"x2", u"x4", u"x8", u"x16" ]
		self.choice_PlotRatio = wx.Choice( sbSizer22.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_PlotRatioChoices, 0 )
		self.choice_PlotRatio.SetSelection( 0 )
		self.choice_PlotRatio.Enable( False )

		sbSizer22.Add( self.choice_PlotRatio, 0, wx.ALL, 5 )

		self.m_staticText54 = wx.StaticText( sbSizer22.GetStaticBox(), wx.ID_ANY, u"points", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText54.Wrap( -1 )
		self.m_staticText54.Enable( False )

		sbSizer22.Add( self.m_staticText54, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )


		bSizer41.Add( sbSizer22, 0, wx.ALL|wx.EXPAND, 10 )


		bSizer41.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		fgSizer24 = wx.FlexGridSizer( 1, 4, 0, 10 )
		fgSizer24.AddGrowableCol( 0 )
		fgSizer24.AddGrowableCol( 3 )
		fgSizer24.SetFlexibleDirection( wx.BOTH )
		fgSizer24.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_OK = wx.Button( self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_OK, 0, wx.ALL, 10 )

		self.btn_Cancel = wx.Button( self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_Cancel, 0, wx.ALL, 10 )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		bSizer41.Add( fgSizer24, 0, wx.EXPAND, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()
		bSizer41.Fit( self )

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class wxFB_dialog_preferences
###########################################################################

class wxFB_dialog_preferences ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Preferences", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		fgSizer39 = wx.FlexGridSizer( 0, 1, 10, 0 )
		fgSizer39.SetFlexibleDirection( wx.BOTH )
		fgSizer39.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_250.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer39.Add( self.m_bitmap2, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 0 )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Save" ), wx.HORIZONTAL )

		self.m_staticText5 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Autosave every", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		sbSizer2.Add( self.m_staticText5, 0, wx.ALL, 10 )

		self.spinCtrl_AutoSave = wx.SpinCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS|wx.SP_WRAP, 1, 60, 1 )
		sbSizer2.Add( self.spinCtrl_AutoSave, 0, wx.ALL, 5 )

		self.m_staticText6 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"mins", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )
		sbSizer2.Add( self.m_staticText6, 0, wx.ALL, 10 )


		fgSizer39.Add( sbSizer2, 0, wx.ALL|wx.EXPAND, 0 )

		sbSizer212 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Timer" ), wx.HORIZONTAL )

		self.m_staticText511 = wx.StaticText( sbSizer212.GetStaticBox(), wx.ID_ANY, u"Timer sound = ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText511.Wrap( -1 )
		sbSizer212.Add( self.m_staticText511, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 10 )

		choice_TimerSoundChoices = [ u"Cash Register", u"Charge", u"Cow", u"Dog", u"Doorbell", u"Exit Cue", u"Fanfare", u"Gong", u"Harp", u"Toilet" ]
		self.choice_TimerSound = wx.Choice( sbSizer212.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_TimerSoundChoices, wx.CB_SORT )
		self.choice_TimerSound.SetSelection( 0 )
		sbSizer212.Add( self.choice_TimerSound, 0, wx.ALL|wx.EXPAND, 5 )


		fgSizer39.Add( sbSizer212, 0, wx.ALL|wx.EXPAND, 0 )

		fgSizer24 = wx.FlexGridSizer( 1, 4, 0, 10 )
		fgSizer24.AddGrowableCol( 0 )
		fgSizer24.AddGrowableCol( 3 )
		fgSizer24.SetFlexibleDirection( wx.BOTH )
		fgSizer24.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_OK = wx.Button( self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_OK, 0, wx.ALL, 10 )

		self.btn_Cancel = wx.Button( self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer24.Add( self.btn_Cancel, 0, wx.ALL, 10 )


		fgSizer24.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer39.Add( fgSizer24, 0, wx.EXPAND, 0 )


		bSizer41.Add( fgSizer39, 1, wx.ALL|wx.EXPAND, 10 )


		self.SetSizer( bSizer41 )
		self.Layout()
		bSizer41.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.choice_TimerSound.Bind( wx.EVT_CHOICE, self.OnTimerSound )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnTimerSound( self, event ):
		event.Skip()


###########################################################################
## Class wxFB_dialog_update
###########################################################################

class wxFB_dialog_update ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Update Available", pos = wx.DefaultPosition, size = wx.Size( 743,240 ), style = wx.CAPTION )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer9 = wx.BoxSizer( wx.VERTICAL )

		fgSizer27 = wx.FlexGridSizer( 6, 1, 5, 0 )
		fgSizer27.AddGrowableCol( 0 )
		fgSizer27.SetFlexibleDirection( wx.BOTH )
		fgSizer27.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText19 = wx.StaticText( self, wx.ID_ANY, u"An update is available for download", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText19.Wrap( -1 )
		fgSizer27.Add( self.m_staticText19, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.TOP, 10 )

		fgSizer29 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer29.AddGrowableCol( 0 )
		fgSizer29.AddGrowableCol( 1 )
		fgSizer29.SetFlexibleDirection( wx.BOTH )
		fgSizer29.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText20 = wx.StaticText( self, wx.ID_ANY, u"Current Version", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText20.Wrap( -1 )
		fgSizer29.Add( self.m_staticText20, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )

		self.m_staticText21 = wx.StaticText( self, wx.ID_ANY, u"1.0", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText21.Wrap( -1 )
		fgSizer29.Add( self.m_staticText21, 0, wx.ALL, 5 )

		self.m_staticText22 = wx.StaticText( self, wx.ID_ANY, u"Latest Version", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText22.Wrap( -1 )
		fgSizer29.Add( self.m_staticText22, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )

		self.m_staticText23 = wx.StaticText( self, wx.ID_ANY, u"1.0", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText23.Wrap( -1 )
		fgSizer29.Add( self.m_staticText23, 0, wx.ALL, 5 )


		fgSizer27.Add( fgSizer29, 1, wx.EXPAND, 5 )

		self.m_gauge1 = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL )
		self.m_gauge1.SetValue( 0 )
		fgSizer27.Add( self.m_gauge1, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND|wx.LEFT|wx.RIGHT, 20 )

		self.m_staticText28 = wx.StaticText( self, wx.ID_ANY, u"Downloading 0 of 136 MB", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.m_staticText28.Wrap( -1 )
		fgSizer27.Add( self.m_staticText28, 0, wx.EXPAND, 5 )

		fgSizer28 = wx.FlexGridSizer( 1, 4, 0, 10 )
		fgSizer28.AddGrowableCol( 0 )
		fgSizer28.AddGrowableCol( 3 )
		fgSizer28.SetFlexibleDirection( wx.BOTH )
		fgSizer28.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer28.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_button20 = wx.Button( self, wx.ID_ANY, u"Download and Update", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer28.Add( self.m_button20, 0, wx.BOTTOM|wx.TOP, 10 )

		self.m_button21 = wx.Button( self, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer28.Add( self.m_button21, 0, wx.BOTTOM|wx.TOP, 10 )


		fgSizer28.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer27.Add( fgSizer28, 1, wx.EXPAND, 5 )


		bSizer9.Add( fgSizer27, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer9 )
		self.Layout()

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class wxFB_dialog_welcome
###########################################################################

class wxFB_dialog_welcome ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Welcome", pos = wx.DefaultPosition, size = wx.Size( 810,445 ), style = wx.CAPTION|wx.STAY_ON_TOP )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		self.m_bitmap2 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_600.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer41.Add( self.m_bitmap2, 1, wx.EXPAND, 5 )

		bSizer16 = wx.BoxSizer( wx.HORIZONTAL )

		self.text_welcome = wx.StaticText( self, wx.ID_ANY, u"Checking for updates", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_welcome.Wrap( -1 )
		bSizer16.Add( self.text_welcome, 0, wx.ALIGN_CENTER|wx.ALL, 20 )

		self.m_animCtrl1 = wx.animate.AnimationCtrl( self, wx.ID_ANY, wx.animate.NullAnimation, wx.DefaultPosition, wx.DefaultSize, wx.animate.AC_DEFAULT_STYLE )
		self.m_animCtrl1.LoadFile( u"assets\\gifs\\CS_symbol_loading.gif" )

		self.m_animCtrl1.Play()
		bSizer16.Add( self.m_animCtrl1, 0, wx.ALIGN_CENTER|wx.ALL, 20 )


		bSizer41.Add( bSizer16, 0, wx.ALIGN_CENTER, 5 )


		self.SetSizer( bSizer41 )
		self.Layout()

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class wxFB_GUI
###########################################################################

class wxFB_GUI ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Causeway Sensors Acquisition Software", pos = wx.DefaultPosition, size = wx.Size( 1099,918 ), style = wx.DEFAULT_FRAME_STYLE|wx.CLIP_CHILDREN|wx.TAB_TRAVERSAL )

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		self.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		self.m_menubar2 = wx.MenuBar( 0 )
		self.m_menubar2.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		self.menu_File = wx.Menu()
		self.menu_FileNew = wx.MenuItem( self.menu_File, wx.ID_ANY, u"New", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_FileNew.SetBitmap( wx.Bitmap( u"assets/images/New document.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_File.AppendItem( self.menu_FileNew )
		self.menu_FileNew.Enable( False )

		self.menu_File.AppendSeparator()

		self.menu_FileExit = wx.MenuItem( self.menu_File, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_FileExit.SetBitmap( wx.Bitmap( u"assets/images/Exit.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_File.AppendItem( self.menu_FileExit )

		self.m_menubar2.Append( self.menu_File, u"File" )

		self.menu_Options = wx.Menu()
		self.menu_OptionsPreferences = wx.MenuItem( self.menu_Options, wx.ID_ANY, u"Preferences", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_OptionsPreferences.SetBitmap( wx.Bitmap( u"assets/images/Toolbox.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_Options.AppendItem( self.menu_OptionsPreferences )

		self.menu_Options.AppendSeparator()

		self.menu_OptionsOptions = wx.MenuItem( self.menu_Options, wx.ID_ANY, u"Options", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_OptionsOptions.SetBitmap( wx.Bitmap( u"assets/images/Application.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_Options.AppendItem( self.menu_OptionsOptions )

		self.m_menubar2.Append( self.menu_Options, u"Options" )

		self.menu_Clamp = wx.Menu()
		self.menu_ClampIn = wx.MenuItem( self.menu_Clamp, wx.ID_ANY, u"Clamp In", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_ClampIn.SetBitmap( wx.Bitmap( u"assets/images/Play.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_Clamp.AppendItem( self.menu_ClampIn )
		self.menu_ClampIn.Enable( False )

		self.menu_ClampOut = wx.MenuItem( self.menu_Clamp, wx.ID_ANY, u"Clamp Out", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_ClampOut.SetBitmap( wx.Bitmap( u"assets/images/Playback.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_Clamp.AppendItem( self.menu_ClampOut )
		self.menu_ClampOut.Enable( False )

		self.menu_Clamp.AppendSeparator()

		self.menu_ClampCustom = wx.MenuItem( self.menu_Clamp, wx.ID_ANY, u"Custom", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_ClampCustom.SetBitmap( wx.Bitmap( u"assets/images/Pinion.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_Clamp.AppendItem( self.menu_ClampCustom )
		self.menu_ClampCustom.Enable( False )

		self.m_menubar2.Append( self.menu_Clamp, u"Clamp" )

		self.menu_Service = wx.Menu()
		self.menu_ServiceService = wx.MenuItem( self.menu_Service, wx.ID_ANY, u"Display Info", wx.EmptyString, wx.ITEM_CHECK )
		self.menu_ServiceService.SetBitmap( wx.Bitmap( u"assets/images/Boss.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_Service.AppendItem( self.menu_ServiceService )

		self.m_menubar2.Append( self.menu_Service, u"Service" )

		self.menu_Help = wx.Menu()
		self.menu_HelpAbout = wx.MenuItem( self.menu_Help, wx.ID_ANY, u"About", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_HelpAbout.SetBitmap( wx.Bitmap( u"assets/images/About.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_Help.AppendItem( self.menu_HelpAbout )

		self.menu_Help.AppendSeparator()

		self.menu_HelpHelp = wx.MenuItem( self.menu_Help, wx.ID_ANY, u"Help", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_HelpHelp.SetBitmap( wx.Bitmap( u"assets/images/Help book.png", wx.BITMAP_TYPE_ANY ) )
		self.menu_Help.AppendItem( self.menu_HelpHelp )

		self.m_menubar2.Append( self.menu_Help, u"Help" )

		self.SetMenuBar( self.m_menubar2 )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )

		self.m_notebook1 = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_notebook1.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		self.hardwarePan = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NO_BORDER )
		self.hardwarePan.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )
		self.hardwarePan.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )

		bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

		fgSizer35 = wx.FlexGridSizer( 4, 1, 20, 20 )
		fgSizer35.AddGrowableCol( 0 )
		fgSizer35.AddGrowableRow( 3 )
		fgSizer35.SetFlexibleDirection( wx.BOTH )
		fgSizer35.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		sbSizer8 = wx.StaticBoxSizer( wx.StaticBox( self.hardwarePan, wx.ID_ANY, u"Machine" ), wx.VERTICAL )

		fgSizer36 = wx.FlexGridSizer( 2, 2, 10, 20 )
		fgSizer36.AddGrowableCol( 1 )
		fgSizer36.SetFlexibleDirection( wx.BOTH )
		fgSizer36.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText23 = wx.StaticText( sbSizer8.GetStaticBox(), wx.ID_ANY, u"Model", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText23.Wrap( -1 )
		self.m_staticText23.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		fgSizer36.Add( self.m_staticText23, 0, wx.ALIGN_CENTER_VERTICAL, 5 )

		self.text_model = wx.StaticText( sbSizer8.GetStaticBox(), wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_model.Wrap( -1 )
		self.text_model.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		fgSizer36.Add( self.text_model, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.m_staticText291 = wx.StaticText( sbSizer8.GetStaticBox(), wx.ID_ANY, u"Firmware", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText291.Wrap( -1 )
		self.m_staticText291.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		fgSizer36.Add( self.m_staticText291, 0, 0, 5 )

		self.text_firmware = wx.StaticText( sbSizer8.GetStaticBox(), wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_firmware.Wrap( -1 )
		self.text_firmware.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		fgSizer36.Add( self.text_firmware, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )


		sbSizer8.Add( fgSizer36, 1, wx.ALL|wx.EXPAND, 10 )

		bSizer14 = wx.BoxSizer( wx.VERTICAL )

		self.btn_connectHardware = wx.Button( sbSizer8.GetStaticBox(), wx.ID_ANY, u"Connect", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_connectHardware.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		bSizer14.Add( self.btn_connectHardware, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )


		sbSizer8.Add( bSizer14, 0, wx.ALL|wx.EXPAND, 10 )


		fgSizer35.Add( sbSizer8, 0, wx.EXPAND, 5 )

		sbSizer9 = wx.StaticBoxSizer( wx.StaticBox( self.hardwarePan, wx.ID_ANY, u"Calibrate detectors" ), wx.VERTICAL )

		fgSizer38 = wx.FlexGridSizer( 3, 2, 10, 20 )
		fgSizer38.AddGrowableCol( 1 )
		fgSizer38.SetFlexibleDirection( wx.BOTH )
		fgSizer38.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText25 = wx.StaticText( sbSizer9.GetStaticBox(), wx.ID_ANY, u"Min", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25.Wrap( -1 )
		fgSizer38.Add( self.m_staticText25, 0, wx.ALIGN_CENTER_VERTICAL, 5 )

		self.spinCtrl_minCounts = wx.SpinCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 150,-1 ), wx.SP_ARROW_KEYS, 1, 65536, 10000 )
		fgSizer38.Add( self.spinCtrl_minCounts, 0, wx.EXPAND, 5 )

		self.m_staticText29 = wx.StaticText( sbSizer9.GetStaticBox(), wx.ID_ANY, u"Max", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText29.Wrap( -1 )
		fgSizer38.Add( self.m_staticText29, 0, 0, 5 )

		self.spinCtrl_maxCounts = wx.SpinCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 150,-1 ), wx.SP_ARROW_KEYS, 1, 65536, 25000 )
		fgSizer38.Add( self.spinCtrl_maxCounts, 0, wx.EXPAND, 5 )

		self.m_staticText311 = wx.StaticText( sbSizer9.GetStaticBox(), wx.ID_ANY, u"Max RPS", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText311.Wrap( -1 )
		fgSizer38.Add( self.m_staticText311, 0, 0, 5 )

		choice_RPSChoices = [ u"1", u"2", u"4" ]
		self.choice_RPS = wx.Choice( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_RPSChoices, 0 )
		self.choice_RPS.SetSelection( 0 )
		fgSizer38.Add( self.choice_RPS, 0, wx.EXPAND, 5 )


		sbSizer9.Add( fgSizer38, 0, wx.ALL|wx.EXPAND, 10 )

		bSizer12 = wx.BoxSizer( wx.VERTICAL )

		self.btn_calibrateDetector = wx.Button( sbSizer9.GetStaticBox(), wx.ID_ANY, u"Calibrate Detector", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_calibrateDetector.Enable( False )

		bSizer12.Add( self.btn_calibrateDetector, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 10 )


		sbSizer9.Add( bSizer12, 1, wx.ALL|wx.EXPAND, 10 )


		fgSizer35.Add( sbSizer9, 0, wx.EXPAND, 5 )

		sbSizer91 = wx.StaticBoxSizer( wx.StaticBox( self.hardwarePan, wx.ID_ANY, u"Calibrate chip" ), wx.VERTICAL )

		self.btn_calibrateChip = wx.Button( sbSizer91.GetStaticBox(), wx.ID_ANY, u"Calibrate Chip", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_calibrateChip.Enable( False )

		sbSizer91.Add( self.btn_calibrateChip, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 10 )


		fgSizer35.Add( sbSizer91, 1, wx.EXPAND, 5 )


		fgSizer35.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		bSizer6.Add( fgSizer35, 0, wx.ALL|wx.EXPAND, 10 )

		self.m_bitmap9 = wx.StaticBitmap( self.hardwarePan, wx.ID_ANY, wx.Bitmap( u"assets/images/causeway_graphic1.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer6.Add( self.m_bitmap9, 0, wx.ALL, 5 )


		self.hardwarePan.SetSizer( bSizer6 )
		self.hardwarePan.Layout()
		bSizer6.Fit( self.hardwarePan )
		self.m_notebook1.AddPage( self.hardwarePan, u"Hardware", False )
		self.acquisitionPan = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NO_BORDER )
		bSizer31 = wx.BoxSizer( wx.VERTICAL )

		fgSizer45 = wx.FlexGridSizer( 1, 2, 0, 10 )
		fgSizer45.AddGrowableCol( 0 )
		fgSizer45.AddGrowableRow( 0 )
		fgSizer45.SetFlexibleDirection( wx.BOTH )
		fgSizer45.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_panel11 = wx.Panel( self.acquisitionPan, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer15 = wx.BoxSizer( wx.VERTICAL )

		self.m_splitter2 = wx.SplitterWindow( self.m_panel11, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D|wx.SP_LIVE_UPDATE )
		self.m_splitter2.SetSashGravity( 0 )
		self.m_splitter2.Bind( wx.EVT_IDLE, self.m_splitter2OnIdle )
		self.m_splitter2.SetMinimumPaneSize( 100 )

		self.m_panel201 = wx.Panel( self.m_splitter2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer27 = wx.BoxSizer( wx.VERTICAL )

		self.panelAcquisitionGraph = panel_AcquisitionGraph( self.m_panel201, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.panelAcquisitionGraph.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		self.panelAcquisitionGraph.SetMinSize( wx.Size( -1,300 ) )

		bSizer27.Add( self.panelAcquisitionGraph, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel201.SetSizer( bSizer27 )
		self.m_panel201.Layout()
		bSizer27.Fit( self.m_panel201 )
		self.m_panel21 = wx.Panel( self.m_splitter2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer28 = wx.BoxSizer( wx.VERTICAL )

		self.panelAcquisitionLog = panel_AcquisitionLog( self.m_panel21, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.panelAcquisitionLog.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		self.panelAcquisitionLog.SetMinSize( wx.Size( -1,100 ) )

		bSizer28.Add( self.panelAcquisitionLog, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel21.SetSizer( bSizer28 )
		self.m_panel21.Layout()
		bSizer28.Fit( self.m_panel21 )
		self.m_splitter2.SplitHorizontally( self.m_panel201, self.m_panel21, 10000 )
		bSizer15.Add( self.m_splitter2, 1, wx.EXPAND, 10 )


		self.m_panel11.SetSizer( bSizer15 )
		self.m_panel11.Layout()
		bSizer15.Fit( self.m_panel11 )
		fgSizer45.Add( self.m_panel11, 1, wx.EXPAND |wx.ALL, 5 )

		fgSizer41 = wx.FlexGridSizer( 5, 1, 5, 0 )
		fgSizer41.AddGrowableCol( 0 )
		fgSizer41.AddGrowableRow( 4 )
		fgSizer41.SetFlexibleDirection( wx.BOTH )
		fgSizer41.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_bitmap22 = wx.StaticBitmap( self.acquisitionPan, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_200.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer41.Add( self.m_bitmap22, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL|wx.EXPAND, 5 )

		sbSizer13 = wx.StaticBoxSizer( wx.StaticBox( self.acquisitionPan, wx.ID_ANY, u"Data" ), wx.VERTICAL )

		self.btn_dataStartStop = wx.Button( sbSizer13.GetStaticBox(), wx.ID_ANY, u"Start", wx.DefaultPosition, wx.DefaultSize, wx.NO_BORDER )
		self.btn_dataStartStop.SetBackgroundColour( wx.Colour( 87, 193, 234 ) )
		self.btn_dataStartStop.Enable( False )

		sbSizer13.Add( self.btn_dataStartStop, 0, wx.ALL|wx.EXPAND, 5 )

		self.btn_dataClear = wx.Button( sbSizer13.GetStaticBox(), wx.ID_ANY, u"Clear", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_dataClear.Enable( False )

		sbSizer13.Add( self.btn_dataClear, 0, wx.ALL|wx.EXPAND, 5 )

		choice_displayTypeChoices = [ u"Sensor 1 only", u"Sensor 2 only", u"Both sensors", u"Subtracted (2-1)", u"Raw counts" ]
		self.choice_displayType = wx.Choice( sbSizer13.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_displayTypeChoices, 0 )
		self.choice_displayType.SetSelection( 2 )
		sbSizer13.Add( self.choice_displayType, 0, wx.ALL|wx.EXPAND, 5 )


		fgSizer41.Add( sbSizer13, 1, wx.BOTTOM|wx.EXPAND|wx.TOP, 5 )

		sbSizer211 = wx.StaticBoxSizer( wx.StaticBox( self.acquisitionPan, wx.ID_ANY, u"Reference" ), wx.VERTICAL )

		self.btn_flag = wx.Button( sbSizer211.GetStaticBox(), wx.ID_ANY, u"Flag", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_flag.Enable( False )

		sbSizer211.Add( self.btn_flag, 0, wx.ALL|wx.EXPAND, 5 )

		self.crosshairPan = wx.Panel( sbSizer211.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		crosshairTextSizer = wx.FlexGridSizer( 4, 4, 5, 10 )
		crosshairTextSizer.AddGrowableCol( 1 )
		crosshairTextSizer.AddGrowableCol( 3 )
		crosshairTextSizer.SetFlexibleDirection( wx.BOTH )
		crosshairTextSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText44 = wx.StaticText( self.crosshairPan, wx.ID_ANY, u"Time :", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText44.Wrap( -1 )
		crosshairTextSizer.Add( self.m_staticText44, 0, wx.ALIGN_RIGHT|wx.LEFT, 10 )


		crosshairTextSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.text_crosshairTime = wx.StaticText( self.crosshairPan, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_crosshairTime.Wrap( -1 )
		crosshairTextSizer.Add( self.text_crosshairTime, 0, wx.ALIGN_CENTER, 5 )


		crosshairTextSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText50 = wx.StaticText( self.crosshairPan, wx.ID_ANY, u"y1 :", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText50.Wrap( -1 )
		crosshairTextSizer.Add( self.m_staticText50, 0, wx.ALIGN_RIGHT|wx.LEFT, 10 )


		crosshairTextSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.text_crosshairY1 = wx.StaticText( self.crosshairPan, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_crosshairY1.Wrap( -1 )
		crosshairTextSizer.Add( self.text_crosshairY1, 0, wx.ALIGN_CENTER, 5 )


		crosshairTextSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText53 = wx.StaticText( self.crosshairPan, wx.ID_ANY, u"y2 :", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText53.Wrap( -1 )
		crosshairTextSizer.Add( self.m_staticText53, 0, wx.ALIGN_LEFT|wx.ALIGN_RIGHT, 10 )


		crosshairTextSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.text_crosshairY2 = wx.StaticText( self.crosshairPan, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_crosshairY2.Wrap( -1 )
		crosshairTextSizer.Add( self.text_crosshairY2, 0, wx.ALIGN_CENTER, 5 )


		crosshairTextSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText33 = wx.StaticText( self.crosshairPan, wx.ID_ANY, u"y2-y1 :", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText33.Wrap( -1 )
		crosshairTextSizer.Add( self.m_staticText33, 0, wx.ALIGN_RIGHT|wx.LEFT, 10 )


		crosshairTextSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.text_crosshairY2Y1 = wx.StaticText( self.crosshairPan, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.text_crosshairY2Y1.Wrap( -1 )
		crosshairTextSizer.Add( self.text_crosshairY2Y1, 0, wx.ALIGN_CENTER, 5 )


		crosshairTextSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		self.crosshairPan.SetSizer( crosshairTextSizer )
		self.crosshairPan.Layout()
		crosshairTextSizer.Fit( self.crosshairPan )
		sbSizer211.Add( self.crosshairPan, 1, wx.EXPAND |wx.ALL, 10 )


		fgSizer41.Add( sbSizer211, 0, wx.BOTTOM|wx.EXPAND|wx.TOP, 5 )

		sbSizer15 = wx.StaticBoxSizer( wx.StaticBox( self.acquisitionPan, wx.ID_ANY, u"Countdown From Crosshair" ), wx.VERTICAL )

		bSizer20 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_bitmap13 = wx.StaticBitmap( sbSizer15.GetStaticBox(), wx.ID_ANY, wx.Bitmap( u"assets/images/stopclock_100.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer20.Add( self.m_bitmap13, 0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		fgSizer26 = wx.FlexGridSizer( 5, 1, 10, 0 )
		fgSizer26.AddGrowableCol( 0 )
		fgSizer26.AddGrowableRow( 0 )
		fgSizer26.AddGrowableRow( 4 )
		fgSizer26.SetFlexibleDirection( wx.BOTH )
		fgSizer26.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer26.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.textCtrl_timer = wx.TextCtrl( sbSizer15.GetStaticBox(), wx.ID_ANY, u"0.0", wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE )
		fgSizer26.Add( self.textCtrl_timer, 0, wx.EXPAND, 40 )

		self.btn_timerStartStop = wx.Button( sbSizer15.GetStaticBox(), wx.ID_ANY, u"Start", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_timerStartStop.SetFont( wx.Font( 9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Swiss 721" ) )

		fgSizer26.Add( self.btn_timerStartStop, 0, wx.EXPAND, 40 )

		fgSizer29 = wx.FlexGridSizer( 0, 4, 0, 0 )
		fgSizer29.AddGrowableCol( 0 )
		fgSizer29.AddGrowableCol( 3 )
		fgSizer29.SetFlexibleDirection( wx.BOTH )
		fgSizer29.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer29.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_staticText501 = wx.StaticText( sbSizer15.GetStaticBox(), wx.ID_ANY, u"Auto flag", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText501.Wrap( -1 )
		fgSizer29.Add( self.m_staticText501, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )

		self.check_AutoFlag = wx.CheckBox( sbSizer15.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer29.Add( self.check_AutoFlag, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT, 5 )


		fgSizer29.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer26.Add( fgSizer29, 1, wx.EXPAND, 5 )


		fgSizer26.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		bSizer20.Add( fgSizer26, 1, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.LEFT, 5 )


		sbSizer15.Add( bSizer20, 0, wx.ALL|wx.EXPAND, 5 )


		fgSizer41.Add( sbSizer15, 1, wx.EXPAND, 5 )


		fgSizer41.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		fgSizer45.Add( fgSizer41, 1, wx.EXPAND, 5 )


		bSizer31.Add( fgSizer45, 1, wx.ALL|wx.EXPAND, 10 )


		self.acquisitionPan.SetSizer( bSizer31 )
		self.acquisitionPan.Layout()
		bSizer31.Fit( self.acquisitionPan )
		self.m_notebook1.AddPage( self.acquisitionPan, u"Acquisition", True )
		self.viewerPanel = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NO_BORDER )
		bSizer26 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_panel18 = wx.Panel( self.viewerPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer25 = wx.BoxSizer( wx.VERTICAL )

		self.m_splitter5 = wx.SplitterWindow( self.m_panel18, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D|wx.SP_LIVE_UPDATE )
		self.m_splitter5.SetSashGravity( 0.5 )
		self.m_splitter5.Bind( wx.EVT_IDLE, self.m_splitter5OnIdle )
		self.m_splitter5.SetMinimumPaneSize( 200 )

		self.m_panel20 = wx.Panel( self.m_splitter5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel20.SetMinSize( wx.Size( -1,200 ) )

		bSizer261 = wx.BoxSizer( wx.VERTICAL )

		self.m_splitter3 = wx.SplitterWindow( self.m_panel20, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D|wx.SP_LIVE_UPDATE )
		self.m_splitter3.SetSashGravity( 0.5 )
		self.m_splitter3.Bind( wx.EVT_IDLE, self.m_splitter3OnIdle )
		self.m_splitter3.SetMinimumPaneSize( 100 )

		self.m_panel19 = wx.Panel( self.m_splitter3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel19.SetMinSize( wx.Size( -1,100 ) )

		fgSizer301 = wx.FlexGridSizer( 2, 4, 0, 0 )
		fgSizer301.AddGrowableCol( 1 )
		fgSizer301.AddGrowableCol( 2 )
		fgSizer301.AddGrowableCol( 3 )
		fgSizer301.AddGrowableRow( 1 )
		fgSizer301.SetFlexibleDirection( wx.BOTH )
		fgSizer301.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText52 = wx.StaticText( self.m_panel19, wx.ID_ANY, u"Date Created", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText52.Wrap( -1 )
		fgSizer301.Add( self.m_staticText52, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_staticText531 = wx.StaticText( self.m_panel19, wx.ID_ANY, u"Sample ID", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText531.Wrap( -1 )
		fgSizer301.Add( self.m_staticText531, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_staticText54 = wx.StaticText( self.m_panel19, wx.ID_ANY, u"Tags", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText54.Wrap( -1 )
		fgSizer301.Add( self.m_staticText54, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_staticText55 = wx.StaticText( self.m_panel19, wx.ID_ANY, u"Users", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText55.Wrap( -1 )
		fgSizer301.Add( self.m_staticText55, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		fgSizer31 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer31.SetFlexibleDirection( wx.BOTH )
		fgSizer31.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText57 = wx.StaticText( self.m_panel19, wx.ID_ANY, u"From", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText57.Wrap( -1 )
		fgSizer31.Add( self.m_staticText57, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.datePicker_From = wx.DatePickerCtrl( self.m_panel19, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		fgSizer31.Add( self.datePicker_From, 0, wx.ALL, 5 )

		self.m_staticText58 = wx.StaticText( self.m_panel19, wx.ID_ANY, u"To", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText58.Wrap( -1 )
		fgSizer31.Add( self.m_staticText58, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.datePicker_To = wx.DatePickerCtrl( self.m_panel19, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DROPDOWN )
		fgSizer31.Add( self.datePicker_To, 0, wx.ALL, 5 )


		fgSizer301.Add( fgSizer31, 1, wx.EXPAND, 5 )

		listBox_SampleIDChoices = []
		self.listBox_SampleID = wx.ListBox( self.m_panel19, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, listBox_SampleIDChoices, wx.LB_ALWAYS_SB|wx.LB_SINGLE )
		fgSizer301.Add( self.listBox_SampleID, 0, wx.ALL|wx.EXPAND, 5 )

		listBox_TagsChoices = []
		self.listBox_Tags = wx.ListBox( self.m_panel19, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, listBox_TagsChoices, wx.LB_ALWAYS_SB|wx.LB_SINGLE )
		fgSizer301.Add( self.listBox_Tags, 0, wx.ALL|wx.EXPAND, 5 )

		listBox_UsersChoices = []
		self.listBox_Users = wx.ListBox( self.m_panel19, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, listBox_UsersChoices, wx.LB_ALWAYS_SB|wx.LB_SINGLE )
		fgSizer301.Add( self.listBox_Users, 0, wx.ALL|wx.EXPAND, 5 )


		self.m_panel19.SetSizer( fgSizer301 )
		self.m_panel19.Layout()
		fgSizer301.Fit( self.m_panel19 )
		self.m_panel15 = wx.Panel( self.m_splitter3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer251 = wx.BoxSizer( wx.VERTICAL )

		self.panel_ViewerList = panel_ViewerList( self.m_panel15, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.panel_ViewerList.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		self.panel_ViewerList.SetMinSize( wx.Size( -1,100 ) )

		bSizer251.Add( self.panel_ViewerList, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel15.SetSizer( bSizer251 )
		self.m_panel15.Layout()
		bSizer251.Fit( self.m_panel15 )
		self.m_splitter3.SplitHorizontally( self.m_panel19, self.m_panel15, 0 )
		bSizer261.Add( self.m_splitter3, 1, wx.EXPAND, 5 )


		self.m_panel20.SetSizer( bSizer261 )
		self.m_panel20.Layout()
		bSizer261.Fit( self.m_panel20 )
		self.m_panel17 = wx.Panel( self.m_splitter5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer262 = wx.BoxSizer( wx.VERTICAL )

		self.panel_ViewerPlot = panel_ViewerPlot( self.m_panel17, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.panel_ViewerPlot.SetMinSize( wx.Size( -1,200 ) )

		bSizer262.Add( self.panel_ViewerPlot, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel17.SetSizer( bSizer262 )
		self.m_panel17.Layout()
		bSizer262.Fit( self.m_panel17 )
		self.m_splitter5.SplitHorizontally( self.m_panel20, self.m_panel17, 0 )
		bSizer25.Add( self.m_splitter5, 1, wx.EXPAND, 5 )


		self.m_panel18.SetSizer( bSizer25 )
		self.m_panel18.Layout()
		bSizer25.Fit( self.m_panel18 )
		bSizer26.Add( self.m_panel18, 1, wx.EXPAND |wx.ALL, 5 )

		fgSizer33 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer33.AddGrowableRow( 2 )
		fgSizer33.SetFlexibleDirection( wx.BOTH )
		fgSizer33.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_bitmap221 = wx.StaticBitmap( self.viewerPanel, wx.ID_ANY, wx.Bitmap( u"assets/images/cs_logo_200.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer33.Add( self.m_bitmap221, 0, wx.ALL, 5 )

		sbSizer14 = wx.StaticBoxSizer( wx.StaticBox( self.viewerPanel, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		self.btn_LoadDatabase = wx.Button( sbSizer14.GetStaticBox(), wx.ID_ANY, u"Load Database", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer14.Add( self.btn_LoadDatabase, 0, wx.ALL|wx.EXPAND, 5 )

		self.btn_ExportToTxt = wx.Button( sbSizer14.GetStaticBox(), wx.ID_ANY, u"Export to .txt", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_ExportToTxt.Enable( False )

		sbSizer14.Add( self.btn_ExportToTxt, 0, wx.ALL|wx.EXPAND, 5 )

		self.btn_DeleteFile = wx.Button( sbSizer14.GetStaticBox(), wx.ID_ANY, u"Delete File", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_DeleteFile.Enable( False )

		sbSizer14.Add( self.btn_DeleteFile, 0, wx.ALL|wx.EXPAND, 5 )


		fgSizer33.Add( sbSizer14, 1, wx.ALL|wx.EXPAND, 5 )


		fgSizer33.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )


		bSizer26.Add( fgSizer33, 0, wx.EXPAND, 5 )


		self.viewerPanel.SetSizer( bSizer26 )
		self.viewerPanel.Layout()
		bSizer26.Fit( self.viewerPanel )
		self.m_notebook1.AddPage( self.viewerPanel, u"Viewer", False )

		bSizer4.Add( self.m_notebook1, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer4 )
		self.Layout()
		self.guiTimer = wx.Timer()
		self.guiTimer.SetOwner( self, wx.ID_ANY )
		self.guiTimer.Start( 50, True )


		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_MENU, self.OnMenu_FileNew, id = self.menu_FileNew.GetId() )
		self.Bind( wx.EVT_MENU, self.OnExit, id = self.menu_FileExit.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenu_OptionsPreferences, id = self.menu_OptionsPreferences.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenu_OptionsOptions, id = self.menu_OptionsOptions.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenu_ClampIn, id = self.menu_ClampIn.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenu_ClampOut, id = self.menu_ClampOut.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenu_ClampCustom, id = self.menu_ClampCustom.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenu_ServiceService, id = self.menu_ServiceService.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenu_HelpAbout, id = self.menu_HelpAbout.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenu_HelpHelp, id = self.menu_HelpHelp.GetId() )
		self.btn_connectHardware.Bind( wx.EVT_BUTTON, self.OnButton_ConnectHardware )
		self.btn_calibrateDetector.Bind( wx.EVT_BUTTON, self.OnButton_CalibrateDetector )
		self.btn_calibrateChip.Bind( wx.EVT_BUTTON, self.OnButton_CalibrateChip )
		self.m_splitter2.Bind( wx.EVT_SPLITTER_SASH_POS_CHANGED, self.OnAcquisitionSashPosChanged )
		self.m_splitter2.Bind( wx.EVT_SPLITTER_SASH_POS_CHANGING, self.OnAcquisitionSashPosChanging )
		self.btn_dataStartStop.Bind( wx.EVT_BUTTON, self.OnButton_DataStartStop )
		self.btn_dataClear.Bind( wx.EVT_BUTTON, self.OnButton_DataClear )
		self.choice_displayType.Bind( wx.EVT_CHOICE, self.OnChoice_DisplayType )
		self.btn_flag.Bind( wx.EVT_BUTTON, self.OnButton_Flag )
		self.btn_timerStartStop.Bind( wx.EVT_BUTTON, self.OnButton_TimerStartStop )
		self.m_splitter5.Bind( wx.EVT_SPLITTER_SASH_POS_CHANGED, self.OnViewerSashPosChanged )
		self.m_splitter5.Bind( wx.EVT_SPLITTER_SASH_POS_CHANGING, self.OnViewerSashPosChanging )
		self.datePicker_From.Bind( wx.EVT_DATE_CHANGED, self.OnDatePicker_From )
		self.datePicker_To.Bind( wx.EVT_DATE_CHANGED, self.OnDatePicker_To )
		self.listBox_SampleID.Bind( wx.EVT_LISTBOX, self.OnListBox_SampleID )
		self.listBox_Tags.Bind( wx.EVT_LISTBOX, self.OnListBox_Tags )
		self.listBox_Users.Bind( wx.EVT_LISTBOX, self.OnListBox_Users )
		self.btn_LoadDatabase.Bind( wx.EVT_BUTTON, self.OnButton_LoadDatabase )
		self.btn_ExportToTxt.Bind( wx.EVT_BUTTON, self.OnButton_ExportToTxt )
		self.btn_DeleteFile.Bind( wx.EVT_BUTTON, self.OnButton_DeleteFile )
		self.Bind( wx.EVT_TIMER, self.OnGuiTimer, id=wx.ID_ANY )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnMenu_FileNew( self, event ):
		event.Skip()

	def OnExit( self, event ):
		event.Skip()

	def OnMenu_OptionsPreferences( self, event ):
		event.Skip()

	def OnMenu_OptionsOptions( self, event ):
		event.Skip()

	def OnMenu_ClampIn( self, event ):
		event.Skip()

	def OnMenu_ClampOut( self, event ):
		event.Skip()

	def OnMenu_ClampCustom( self, event ):
		event.Skip()

	def OnMenu_ServiceService( self, event ):
		event.Skip()

	def OnMenu_HelpAbout( self, event ):
		event.Skip()

	def OnMenu_HelpHelp( self, event ):
		event.Skip()

	def OnButton_ConnectHardware( self, event ):
		event.Skip()

	def OnButton_CalibrateDetector( self, event ):
		event.Skip()

	def OnButton_CalibrateChip( self, event ):
		event.Skip()

	def OnAcquisitionSashPosChanged( self, event ):
		event.Skip()

	def OnAcquisitionSashPosChanging( self, event ):
		event.Skip()

	def OnButton_DataStartStop( self, event ):
		event.Skip()

	def OnButton_DataClear( self, event ):
		event.Skip()

	def OnChoice_DisplayType( self, event ):
		event.Skip()

	def OnButton_Flag( self, event ):
		event.Skip()

	def OnButton_TimerStartStop( self, event ):
		event.Skip()

	def OnViewerSashPosChanged( self, event ):
		event.Skip()

	def OnViewerSashPosChanging( self, event ):
		event.Skip()

	def OnDatePicker_From( self, event ):
		event.Skip()

	def OnDatePicker_To( self, event ):
		event.Skip()

	def OnListBox_SampleID( self, event ):
		event.Skip()

	def OnListBox_Tags( self, event ):
		event.Skip()

	def OnListBox_Users( self, event ):
		event.Skip()

	def OnButton_LoadDatabase( self, event ):
		event.Skip()

	def OnButton_ExportToTxt( self, event ):
		event.Skip()

	def OnButton_DeleteFile( self, event ):
		event.Skip()

	def OnGuiTimer( self, event ):
		event.Skip()

	def m_splitter2OnIdle( self, event ):
		self.m_splitter2.SetSashPosition( 10000 )
		self.m_splitter2.Unbind( wx.EVT_IDLE )

	def m_splitter5OnIdle( self, event ):
		self.m_splitter5.SetSashPosition( 0 )
		self.m_splitter5.Unbind( wx.EVT_IDLE )

	def m_splitter3OnIdle( self, event ):
		self.m_splitter3.SetSashPosition( 0 )
		self.m_splitter3.Unbind( wx.EVT_IDLE )
