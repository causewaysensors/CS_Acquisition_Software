#!/usr/bin/env python2
'''Log for displaying information during acquisition from the sensor'''
from time import sleep

import wx
from wx.lib.mixins.listctrl import ListCtrlAutoWidthMixin
from wx.lib.newevent import NewCommandEvent


DISPLAY_TYPES = (
    "Both sensors",
    "Sensor 1 only",
    "Sensor 2 only",
    "Subtracted (2-1)",
    "Raw counts",
    )


class AutoWidthListCtrl(wx.ListCtrl, ListCtrlAutoWidthMixin):
    """Class that automatically sizes the columns of a list ctrl"""
    def __init__(self, parent, style=wx.LC_REPORT):
        wx.ListCtrl.__init__(self, parent, -1, style=style)
        ListCtrlAutoWidthMixin.__init__(self)


class panel_AcquisitionLog(wx.Panel):
    """This is the log"""
    def __init__(
            self,
            parent,
            id=wx.ID_ANY,
            position=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=0,
            name="",
            ):

        wx.Panel.__init__(self, parent, id, position, size, style, name)

        self.parent = parent

        # Variables that control the log
        self.dict_flags = {}
        self.displaytype = "Both sensors"

        # Sizers and objects
        self.vbox = wx.BoxSizer(wx.VERTICAL)

        self.log = AutoWidthListCtrl(self)

        self.log.InsertColumn(0, "Time", width=50)
        self.log.InsertColumn(1, "Sensor", width=100)
        self.log.InsertColumn(2, "Response", width=100)
        self.log.InsertColumn(3, "Baseline", width=100)
        self.log.InsertColumn(4, "Description")

        self.vbox.Add(self.log, 1, wx.EXPAND)
        self.SetSizer(self.vbox)

        # Bind internal events
        self.Bind(
            wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnRightClickFlag, self.log)

        # Events that are passed up the parent
        self.Event_FlagDeleted, self.EVT_FLAGDELETED = NewCommandEvent()
        self.Event_FlagEdited, self.EVT_FLAGEDITED = NewCommandEvent()

    def Clear(self):
        """Reset everything when the graph is cleared"""
        self.displaytype = "Both sensors"
        self.log.DeleteAllItems()
        self.dict_flags = {}

    def ChangeDisplayType(self, newtype):
        """Change the log panel to match the graph"""
        if newtype in DISPLAY_TYPES:
            self.displaytype = newtype
            self.Redraw()

    def Redraw(self):
        """Totally clear, recalculate and redraw

        Log panel changes very infrequently
        """

        # Clear the whole
        self.logrow = 0
        self.log.DeleteAllItems()

        # Add all the flags again in order
        for time in sorted(self.dict_flags.keys()):

            counts1 = float(self.dict_flags[time]['sensor1_counts'])
            counts2 = float(self.dict_flags[time]['sensor2_counts'])

            # If it is a baseline (otherwise use previous baseline)
            if self.dict_flags[time]['baseline']:
                baseline1_counts = counts1
                baseline2_counts = counts2

            # Calculate the percentages
            def percentage(a, b):
                return (a - b) / b * 100.0
            percentage1 = percentage(counts1, baseline1_counts)
            percentage2 = percentage(counts2, baseline2_counts)
            self.dict_flags[time]['sensor1_percent'] = percentage1
            self.dict_flags[time]['sensor2_percent'] = percentage2
            self.dict_flags[time]['subtracted_percent'] = (
                percentage2 - percentage1)

            def update(value, sensor='1', format_="%0.3f%%"):
                index = self.log.InsertStringItem(self.logrow, str(time))
                self.log.SetStringItem(index, 1, sensor)
                self.log.SetStringItem(index, 2, format_ % value)
                baseline = self.dict_flags[time]['baseline']
                self.log.SetStringItem(index, 3, str(baseline))
                description = self.dict_flags[time]['description']
                self.log.SetStringItem(index, 4, description)
                self.logrow += 1

            if self.displaytype == "Both sensors":
                update(percentage1)
                update(percentage2, sensor='2')
            elif self.displaytype == "Sensor 1 only":
                update(percentage1)
            elif self.displaytype == "Sensor 2 only":
                update(percentage2, '2')
            elif self.displaytype == "Subtracted (2-1)":
                update(percentage2 - percentage1, sensor='2-1')
            elif self.displaytype == "Raw counts":
                update(counts1, sensor='1', format_="%0.1f")
                update(counts2, sensor='2', format_="%0.1f")

        self.Refresh()

    def AddFlag(
            self,
            time,
            sensor1_counts=0.0,
            sensor2_counts=0.0,
            description="",
            baseline=False,
            colour="Red",
            ):
        """Add a flag to the logpanel

        Only one flag per time so can overwrite if required (exception is 0)
        """
        self.dict_flags[time] = {
                    'sensor1_counts': sensor1_counts,
                    'sensor2_counts': sensor2_counts,
                    'description': description,
                    'baseline': baseline,
                    'colour': colour,
                    }
        self.Redraw()

    def OnRightClickFlag(self, event):
        """Right click on any row to delete/edit it"""

        # Get time that has been right-clicked on
        time = float(event.GetText())

        # Can't delete the initial flag
        if time == 0:
            return

        # Create menu and bind events
        menu = wx.Menu()

        delete = menu.Append(-1, "Delete")
        self.Bind(
            wx.EVT_MENU, lambda event: self.OnDeleteFlag(event, time), delete)
        edit = menu.Append(-1, "Edit")
        self.Bind(
            wx.EVT_MENU, lambda event: self.OnEditFlag(event, time), edit)

        self.PopupMenu(menu, event.GetPoint())

        menu.Destroy()

    def OnDeleteFlag(self, event, time):
        """Delete the flag from array and redraw"""
        self.dict_flags.pop(time)
        self.Redraw()

        evt = self.Event_FlagDeleted(self.GetId(), time=time)
        wx.PostEvent(self, evt)

    def OnEditFlag(self, event, time):
        """Create dialog to edit flags"""
        wx.PostEvent(self, self.Event_FlagEdited(
            self.GetId(),
            time=time,
            description=self.dict_flags[time]['description'],
            baseline=self.dict_flags[time]['baseline'],
            colour=self.dict_flags[time]['colour'],
            ))


class panel_AcquisitionLog_test(wx.App):
    '''Class for testing panel_AcquisitionLog'''
    def __init__(self, auto_close=True):
        """Place the panel in a frame and test opening, updating and closing"""
        super(panel_AcquisitionLog_test, self).__init__()
        # Setup the UI elements
        self.frame = wx.Frame(None)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.logpan = panel_AcquisitionLog(self.frame)
        self.sizer.Add(self.logpan, 1, wx.EXPAND | wx.ALL, 20)
        self.frame.Show()
        self.frame.SetSizerAndFit(self.sizer)
        self.frame.Layout()
        self.frame.Maximize()

        # Run through a few tests
        tests = {
            3: (self.logpan.ChangeDisplayType, ("Sensor 1 only", )),
            5: (self.logpan.AddFlag, (2, 25002, 25004, "New baseline", True,)),
            6: (self.logpan.ChangeDisplayType, ("Sensor 2 only", )),
            9: (self.logpan.ChangeDisplayType, ("Subtracted (2-1)", )),
            12: (self.logpan.ChangeDisplayType, ("Raw counts", )),
            15: (self.logpan.ChangeDisplayType, ("Both sensors", )),
        }
        for i in range(max(i for i in tests)):
            sleep(0 if auto_close else 1)
            try:
                tests[i][0](*tests[i][1])
            except KeyError:
                pass

        if auto_close:
            self.TopWindow.Destroy()


if __name__ == '__main__':
    panel_AcquisitionLog_test(False).MainLoop()
