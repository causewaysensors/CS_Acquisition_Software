from wx import App

from wxfb.layout import wxFB_dialog_calibrateChipResults


class dialogCalibrateChipResults(wxFB_dialog_calibrateChipResults):
    def __init__(
            self,
            parent,
            data_sensor1,
            data_sensor2,
            slope,
            yIntercept,
            r2,
            ):

        # Initialize parent class
        wxFB_dialog_calibrateChipResults.__init__(self, parent)

        self.text_Slope.SetLabelText("%0.3E" % slope)
        self.text_YIntercept.SetLabelText("%0.3E" % yIntercept)
        self.text_R2.SetLabelText("%0.3f" % r2)

        self.graphpan.PlotData(data_sensor1, data_sensor2, slope, yIntercept)


class dialogCalibrateChipResults_test(App):
    '''Class to test dialogCalibrateChipResults.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close after tests run'''
        super(dialogCalibrateChipResults_test, self).__init__()

        # Create dialog with sample data
        dialog = dialogCalibrateChipResults(
            parent=None,
            data_sensor1=[1, 2, 3, 4],
            data_sensor2=[2, 4, 6, 8],
            slope=2,
            yIntercept=0,
            r2=1,
            )

        # Depending on auto_close, dialog needs opened differently
        if auto_close:
            dialog.Show()
        else:
            dialog.ShowModal()
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogCalibrateChipResults_test(False).MainLoop()
