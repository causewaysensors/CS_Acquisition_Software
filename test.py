from sqlite3 import connect
from subprocess import call
from unittest import TestCase, main

from lib.db import names
from lib.db.models import CREATE_STATEMENT, INSERT_STATEMENT


class TestPortability(TestCase):
    '''Check that this repository is portable across machines'''
    def test_absence(self):
        '''Check that specific strings are absent from this repository'''
        for i in (
                'Google Driv'+'e',  # Hardcoded path from 2.0 development
                'SELECT '+'*',  # Bad practice
                'select '+'*',  # "
                ):
            result = call(('git', 'grep', '-F', i))
            self.assertNotEqual(0, result)


class TestDatabase(TestCase):
    '''Test the new style methods for accessing the database'''
    def test_names(self):
        connection = connect(':memory:')
        cursor = connection.cursor()
        cursor.execute(CREATE_STATEMENT)
        for i in 'abc':
            cursor.execute(INSERT_STATEMENT, (
                i,  # Name
                'v0.0.0',  # Version
                '02_02_2018',  # Date
                '',  # SampleID
                '',  # Users
                '',  # Tags
                '',  # Notes
                '[0.0, 1.0, 2.0]',  # Array_Time
                '[10000, 10000, 10000]',  # Array_Y1_Counts
                '[10000, 10000, 10000]',  # Array_Y2_Counts
                '[0.0, 0.0, 0.0]',  # Array_Y1_Percent
                '[0.0, 0.0, 0.0]',  # Array_Y2_Percent TEXT
                '[0.0]',  # Array_Baseline_Times
                '{}',  # invalid! Dict_Flags
                ))
        self.assertEqual(names(cursor), list('abc'))


if __name__ == '__main__':
    main()
