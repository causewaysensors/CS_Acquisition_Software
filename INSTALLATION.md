Installation instructions
=========================

*Following these instructions on a new Windows 10 PC should take one to two hours.*

This document starts with a set of explanations of why various libraries are required, then includes detailed instructions how to install and run the software. Finally there is a question and answer section

Explanations
------------

-	`configparser`: The [`ConfigParser`](https://docs.python.org/2/library/configparser.html) module has been renamed and updated to `configparser` in Python 3. This repository uses a [back-port](https://pypi.python.org/pypi/configparser) to Python 2.
-	`matplotlib`: Main plotting library, see also [`pylab`](#pylab)
-	`SciPy`: Only used for one linear regression [function](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html) (`scipy.stats.linregress`).

### `wxPython`

Main GUI library, installed separately and not managed by [`pipenv`](#pipenv) . This repository requires the classic `3.0.2.0` release from 28 November 2014.

This repository is not compatible with the [Project Phoenix `wxPython`](https://pypi.python.org/pypi/wxPython/4.0.0b2) releases, from version 4.0 onwards. The migration [guide](https://docs.wxpython.org/MigrationGuide.html) explains some of the incompatibilities.

Binaries for classic `wxPython` are not available through [the Python Package Index](https://pypi.python.org). `wxPython` is not managed through [`pipenv`](#pipenv)\.

Python 3 binaries for classic `wxPython` are [not available](https://sourceforge.net/projects/wxpython/files/wxPython/3.0.2.0/) online. Python 3 binaries for phoenix `wxPython` releases are available via `pip`, but would require changes to this repository during a migration. The [downloads page](https://www.wxpython.org/pages/downloads/) describes `wxPython` as "a very large and complex package, with several dependencies, and in many cases is not very easy to build".

### wxFormBuilder

Design software for creating `wxPython` layouts. Installers available from [wxFormBuilder](https://github.com/wxFormBuilder/wxFormBuilder).

Must use version 3.6.0 as later version are designed for Phoenix (see above for issues and migration requirements) and earlier versions are not compatible.

N.B. wxFormBuilder has a known issue with wx.Timer, where the flag for `oneshot` is set to `true` instead of `True`. This needs manually changed when the script is generated.

### Python 3

This software runs in Python 2.7. Python 2.7 was released on 3 July 2010. See [`wxPython` (above)](#wxpython) for details of some challenges running in Python 3.

Python 3.6 is installed to provide the [launcher](https://docs.python.org/3/using/windows.html#launcher). [PEP 397](https://www.python.org/dev/peps/pep-0397/) and Python 3 introduced `py.exe` which simplifies using Python under Windows. The launcher aids in locating different Python versions.

### `Pipenv`

Since roughly August 2017 `Pipenv` has been the official recommended Python dependency management tool. This is not used to manage [`wxPython`](#wxpython)\.

### `pylab`

`pylab` is a convenience module for working with `matplotlib` and `numpy`, a numerical Python library. The `matplotlib` usage [FAQ](https://matplotlib.org/faq/usage_faq.html#matplotlib-pyplot-and-pylab-how-are-they-related) explains that using `pylab` is not recommended.

Installation instructions
-------------------------

If you experience problems copying and pasting blocks of code, for example the lines pasting in reverse order, beware the known issues with `PowerShell` ([1](https://github.com/lzybkr/PSReadLine/issues/496#issuecomment-338515707), [2](https://github.com/lzybkr/PSReadLine/issues/579)). The window responds to pasting with `CTRL+V` differently to a right-click or pasting via the menu, especially if the pasted text does not include carriage returns. A suggested workaround is to run `Remove-PSReadlineKeyHandler Ctrl+Enter`.

### Pre-requisites

1.	Check operating system: Windows 10 Pro or Home, 64 bit (Start → About your PC).
2.	Add a new local admin user for example "acquisition" (Home: Start → Settings → Family & other people → Add someone else…; Pro: Start → Add, edit or remove other people → Add someone else…). Choose options that don't need a Microsoft Account or email address.
3.	Change the account type to Administrator.
4.	Log off and log on as the new user, following the example above "acquisition".
5.	Clean up the Desktop, the Task Bar and the Status Tray.
6.	Check the "Region & language" settings are set to "English (United Kingdom)".

### Download source code with Git

1.	Install 64 bit [Git for Windows](https://git-scm.com/download/win) using all of the default options, then check this succeeded with `PowerShell`:

	```powershell
	git version 2.15.1.windows.2
	```

2.	Clone this repository onto the desktop, so at the `PowerShell` prompt:

	```powershell
	cd $Env:USERPROFILE\Desktop
	git clone https://gitlab.com/causewaysensors/acquisition.git/
	cd acquisition
	```

3.	Install the 64 bit [Atom](https://atom.io) text editor (*optional*).

4.	Clean up the Desktop again (*optional*).

### Install Python

1.	Remove any existing Python installations.

2.	Try to install the 64 bit [Visual C++ Re-distributable for Visual Studio 2015](https://www.microsoft.com/en-us/download/details.aspx?id=48145) via `vc_redist.x64.exe`, ignoring any error that reads "Another version of this product is already installed…"

3.	Install 64 bit versions of the following. Use all of the default options:

	1.	Python 3.6.4 [download](https://www.python.org/ftp/python/3.6.4/python-3.6.4-amd64.exe), [release](https://www.python.org/downloads/release/python-364/)

	2.	Python 2.7.14 [download](https://www.python.org/ftp/python/2.7.14/python-2.7.14.amd64.msi), [release](https://www.python.org/downloads/release/python-2714/)

	3.	`wxPython` 3.0.2.0 [download](https://sourceforge.net/projects/wxpython/files/wxPython/3.0.2.0/wxPython3.0-win64-3.0.2.0-py27.exe/download), [release](https://sourceforge.net/projects/wxpython/files/wxPython/3.0.2.0/)

	Test that all three installed correctly by running the following at a `PowerShell` prompt:

	```powershell
	py --version
	py -2 --version
	py -2 -c "from wx import version; print(version())"
	```

### Install `pipenv`

Install `pipenv` for Python 2 and then check that the install succeeded by running the two commands below in a `PowerShell` Window:

```powershell
py -2 -m pip install pipenv
py -2 -m pipenv --version
```

The output should finish with a version number like `pipenv, version 9.0.3`.

*`pipenv` is installed for Python 2 so that only one set of folders has to be added to `PATH` in the next step.*

### Install dependencies and run

*All of the commands in this section should be run in the same `PowerShell` window.*

1.	Start in the directory containing the clone of the git repository that was downloaded earlier:

	```powershell
	cd $Env:USERPROFILE\Desktop\acquisition\
	```

2.	Change the `PATH` so that it includes `pew` (see [Why change the path?](#why-change-the-path) for an explanation below) and `python`:

	```powershell
	$Env:PATH = ("{0};{1}" -f $Env:PATH, (py -2 -c "
	from sys import executable
	from os.path import dirname, join
	print(';'.join((join(dirname(executable), 'Scripts'), dirname(executable))))
	"));
	```

	Then check that the change to `PATH` succeeded, and that all of the required programs are available:

	```powershell
	python --version
	pew version
	pipenv --version
	```

	Which should display two version numbers like:

	```
	Python 2.7.14
	1.1.2
	pipenv, version 9.0.3
	```

3.	Turn off the colours which don't work with the default `PowerShell` colour scheme:

	```powershell
	$Env:PIPENV_COLORBLIND=1
	```

4.	Install the dependencies:

	```powershell
	$Env:PIP_IGNORE_INSTALLED = 1
	pipenv --site-packages install --dev
	Remove-Item Env:PIP_IGNORE_INSTALLED
	```

5.	Run the software:

	```powershell
	pipenv run python .\main.py
	```

Run previously installed software
---------------------------------

The `PowerShell` commands below are the minimum to run a version of the software that has been previously installed in line with the instructions above.

```powershell
cd $Env:USERPROFILE\Desktop\acquisition
$Env:PATH = $(py path.py)
pipenv run python .\main.py
```

Question and answers
--------------------

### Why change the path?

A lot of modules work with the `py.exe` launcher, but not all.

[pew](https://pypi.org/project/pew/) is the "Python Environment Wrapper", and cannot be directly executed as a module:

```
…>py -m pew
…\Python36\python.exe: No module named pew.__main__; 'pew' is a package and cannot be directly executed
```

`Pipenv` also requires that `python.exe` is on the `PATH`.

### How to remove everything?

After uninstalling Python through "Add or Remove Programs", `$Env:USERPROFILE\AppData\Local\Programs\Python` or `C:\Python27` will still contain scripts installed with `pip`.

`virtualenvs` may also need to be removed from `$Env:USERPROFILE\.virtualenvs`

### Why do I get an error message?

If the last line of the message starts with `ImportError:` it may be for one of the two reasons below:

1.	Perhaps because you have not followed these instructions exactly. These instructions have been tested multiple times on the exact versions of the software specified; please check that you are following the steps precisely.

2.	Perhaps because `python` is missing from the `pipenv` command for example:

	```powershell
	pipenv run .\main.py  # causes an ImportError
	pipenv run python .\main.py  # doesn't
	```
