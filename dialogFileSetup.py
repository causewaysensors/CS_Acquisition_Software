from datetime import date
from json import dumps, loads

import wx

from dialogClampPrompt import dialogClampPrompt

from lib.db import names
from lib.db.models import INSERT_STATEMENT
from lib.settings import VALID, VERSION, read_config, write_config

from wxfb.layout import wxFB_dialog_fileSetup


class dialogFileSetup(wxFB_dialog_fileSetup):
    '''Dialog to enter name, ids, users, tags and notes'''
    def __init__(self, parent, config):

        # Initialize parent class
        wxFB_dialog_fileSetup.__init__(self, parent)

        # References
        self.configParser = config
        self.parent = parent

        # CONFIGURATION FILE
        if 'DialogFileSetup' not in self.configParser:
            self.configParser['DialogFileSetup'] = {}

        if 'SampleID' in self.configParser['DialogFileSetup']:
            listvalues = loads(
                self.configParser.get('DialogFileSetup', 'SampleID'))
            listvalues.sort()
            self.listBox_SampleID.InsertItems(listvalues, 0)

        if 'Tags' in self.configParser['DialogFileSetup']:
            listvalues = loads(
                self.configParser.get('DialogFileSetup', 'Tags'))
            listvalues.sort()
            self.listBox_Tags.InsertItems(listvalues, 0)

        if 'Users' in self.configParser['DialogFileSetup']:
            listvalues = loads(
                self.configParser.get('DialogFileSetup', 'Users'))
            listvalues.sort()
            self.listBox_Users.InsertItems(listvalues, 0)

        # Want everything disabled
        if parent:
            self.parent.Enable(False)

    def AddValueConfig(self, section, key, value):
        '''Add a value and save the config'''
        if section not in self.configParser:
            self.configParser[section] = {}

        if key in self.configParser[section]:
            oldlist = loads(self.configParser.get(section, key))
            oldlist.append(value)
            self.configParser[section][key] = dumps(sorted(oldlist))
        else:
            self.configParser[section][key] = dumps([value, ])
        write_config(self.configParser)

    def AddToListBox(self, listBox, name):
        '''Prompt for a new value and add to sorted list box'''
        prompt = wx.TextEntryDialog(
            self, 'Enter new value', 'Add value', defaultValue='')
        if prompt.ShowModal() == wx.ID_OK:
            new = prompt.GetValue()
            if new == "":
                return
            values = sorted(listBox.GetStrings() + [new, ])
            listBox.Clear()
            listBox.InsertItems(values, 0)
            self.AddValueConfig('DialogFileSetup', name, new)
            self.Refresh()
        return

    def RemoveFromListBox(self, listBox, name):
        '''Remove selected values from the list box'''
        ids = listBox.GetSelections()
        if not ids:
            return
        for i in reversed(ids):
            listBox.Delete(i)
        to_save = set(loads(self.configParser.get("DialogFileSetup", name)))
        to_save -= set(listBox.GetString(i) for i in ids)
        self.configParser["DialogFileSetup"][name] = to_save
        write_config(self.configParser)
        self.Refresh()
        return

    def OnButton_AddSampleID(self, event):
        '''Pressed button to add to Sample ID list'''
        self.AddToListBox(self.listBox_SampleID, 'SampleID')
        event.Skip()

    def OnButton_RemoveSampleID(self, event):
        '''Pressed button to remove from Sample ID list'''
        self.RemoveFromListBox(self.listBox_SampleID, 'SampleID')
        event.Skip()

    def OnButton_AddTag(self, event):
        '''Pressed button to add to Tags list'''
        self.AddToListBox(self.listBox_Tags, 'Tags')
        event.Skip()

    def OnButton_RemoveTag(self, event):
        '''Pressed button to remove from Tags list'''
        self.RemoveFromListBox(self.listBox_Tags, 'Tags')
        event.Skip()

    def OnButton_AddUser(self, event):
        '''Pressed button to add to Users list'''
        self.AddToListBox(self.listBox_Users, 'Users')
        event.Skip()

    def OnButton_RemoveUser(self, event):
        '''Pressed button to remove from Users list'''
        self.RemoveFromListBox(self.listBox_Users, 'Users')
        event.Skip()

    def OnButton_OK(self, event):
        '''Pressed OK so check input and save to the database'''
        # Read everything from the dialog
        self.parent.fileName = str(self.textCtrl_Name.GetValue())
        self.parent.file_sampleid = str(
            self.listBox_SampleID.GetStringSelection())
        self.parent.tags = [
            str(self.listBox_Tags.GetString(index))
            for index in self.listBox_Tags.GetSelections()]
        self.parent.users = [
            str(self.listBox_Users.GetString(index))
            for index in self.listBox_Users.GetSelections()]
        self.parent.file_notes = str(self.textCtrl_Notes.GetValue())

        # Add todays date to user name to create full record name
        todaysdate = date.today().strftime('%d_%m_%Y')
        self.parent.fileName = "%s-%s" % (self.parent.fileName, todaysdate)

        # Get all the names from the database and make sure no duplicates
        databasefileNames = names(self.parent.cur)

        if not self.parent.fileName:
            wx.MessageBox(
                (
                    'Blank fileName\n'
                    'Enter a fileName containing only letters,'
                    'numbers and underscores'
                ),
                'Error',
                wx.OK | wx.ICON_ERROR,
                )
            return
        elif not VALID.issuperset(self.parent.fileName):
            wx.MessageBox(
                (
                    'Invalid fileName\n'
                    'Enter a fileName containing only letters,'
                    'numbers and underscores'
                ),
                'Error',
                wx.OK | wx.ICON_ERROR
                )
            return
        elif self.parent.fileName in databasefileNames:
            '''Tell user about invalid file name'''
            wx.MessageBox(
                (
                    'File with same name created today\n'
                    'Please rename or delete previous file'
                ),
                'Error',
                wx.OK | wx.ICON_ERROR,
                )
            return

        # Enable/disable everything
        self.parent.dataStreaming = True
        self.parent.EnableDisable()

        # Set up database entry
        graph = self.parent.panelAcquisitionGraph
        self.parent.cur.execute(
            INSERT_STATEMENT,
            (
                self.parent.fileName,
                VERSION,
                todaysdate,
                self.parent.file_sampleid,
                dumps(self.parent.users),
                dumps(self.parent.tags),
                self.parent.file_notes,
                dumps(graph.array_time_1),
                dumps(graph.array_y1_counts_1),
                dumps(graph.array_y2_counts_1),
                dumps(graph.array_y1_1),
                dumps(graph.array_y2_1),
                dumps(graph.array_baseline_times),
                dumps(self.parent.panelAcquisitionLog.dict_flags)
                )
            )

        self.parent.db.commit()

        # Check if clamp needs moved
        dialog = dialogClampPrompt(self.parent)
        dialog.ShowModal()

        # Tell the arduino to begin data
        self.parent.WriteSerial("newData\n")

        # Delete the dialog
        self.Destroy()
        self.parent.Enable()
        self.parent.SetFocus()

        event.Skip()
        return

    def OnButton_Cancel(self, event):
        '''Cancel so move focus back to parent'''
        if self.parent:
            self.parent.Enable(True)
        self.Destroy()
        event.Skip()


class dialogFileSetup_test(wx.App):
    '''Test dialogFileSetup opens and then close in a frame'''
    def __init__(self, auto_close=True):
        '''Setup and then close'''
        super(dialogFileSetup_test, self).__init__()
        dialog = dialogFileSetup(
                parent=None,
                config=read_config(test=True),
                )
        dialog.Show()
        if auto_close:
            button = dialog.btn_Cancel
            event = wx.PyCommandEvent(wx.EVT_BUTTON.typeId, button.GetId())
            wx.PostEvent(button, event)


if __name__ == '__main__':
    dialogFileSetup_test(False).MainLoop()
