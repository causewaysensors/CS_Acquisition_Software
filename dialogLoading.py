from time import sleep

from wx import App

from wxfb.layout import wxFB_dialog_loading


class dialogLoading(wxFB_dialog_loading):
    def __init__(self, parent, message):

        # Initialize parent class
        wxFB_dialog_loading.__init__(self, parent)

        # Update the message to whatever you need
        self.message.SetLabelText(message)

    def ChangeMessage(self, newmessage):

        self.message.SetLabelText(newmessage)
        self.Layout()
        self.Refresh()

    def UpdateProgress(self, percent):

        self.progress.SetValue(percent)
        self.Refresh()


class dialogLoading_test(App):
    '''Class to test dialogLoading.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, run tests and close'''
        super(dialogLoading_test, self).__init__()

        # Create dialog with sample
        dialog = dialogLoading(parent=None, message="Inital message")
        dialog.Show()

        # List of functions to run [function, arguments]
        listTests = [
            [dialog.ChangeMessage, ["New message"]],
            [dialog.UpdateProgress, [25]],
            [dialog.UpdateProgress, [50]],
            [dialog.UpdateProgress, [75]],
            [dialog.UpdateProgress, [100]],
            ]
        for function, arguments in listTests:
            sleep(0 if auto_close else 1)
            function(*arguments)

        # Loading dialog has no close so automatically destroy
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogLoading_test(False).MainLoop()
