def names(cursor):
    '''Retrieve a list of names from the database'''
    cursor.execute('SELECT Name FROM CAUSEWAY_DATA;')
    return [str(row[0]) for row in cursor.fetchall()]
