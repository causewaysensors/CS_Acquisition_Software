from textwrap import dedent


CREATE_STATEMENT = dedent('''
    CREATE TABLE IF NOT EXISTS CAUSEWAY_DATA(
         Name TEXT UNIQUE
        ,Version TEXT
        ,Date TEXT
        ,SampleID TEXT
        ,Users TEXT
        ,Tags TEXT
        ,Notes TEXT
        ,Array_Time TEXT
        ,Array_Y1_Counts TEXT
        ,Array_Y2_Counts TEXT
        ,Array_Y1_Percent TEXT
        ,Array_Y2_Percent TEXT
        ,Array_Baseline_Times
        ,Dict_Flags
    );''')
DELETE_STATEMENT = 'DELETE FROM CAUSEWAY_DATA WHERE Name=?;'
INSERT_STATEMENT = 'INSERT INTO CAUSEWAY_DATA VALUES({});'.format(
    ', '.join(['?', ] * 14)
    )
SELECT_STATEMENT = dedent('''
    SELECT
         Name
        ,Version
        ,Date
        ,SampleID
        ,Users TEXT
        ,Tags TEXT
        ,Notes TEXT
        ,Array_Time TEXT
        ,Array_Y1_Counts TEXT
        ,Array_Y2_Counts TEXT
        ,Array_Y1_Percent TEXT
        ,Array_Y2_Percent TEXT
        ,Array_Baseline_Times
        ,Dict_Flags
    FROM CAUSEWAY_DATA
    WHERE Name=?
    ;''')
