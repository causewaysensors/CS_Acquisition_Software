'''Default settings file

Both Python 2 and 3 should be able to import from this file.

Can be imported from with only the standard library installed.
'''
from os.path import expanduser, join
from pprint import pprint
from string import ascii_letters, digits
from subprocess import check_output

# Python 2 uses a backported version from PyPI
try:
    from configparser import ConfigParser  # noqa: I100 I201 I202
except ImportError:
    pass

DEFAULT_FOLDERNAME = expanduser(join('~', 'Documents', 'CausewaySensors'))
VALID = set(''.join((ascii_letters, '_- ', digits)))
VERSION = "v2.0.0"
try:
    VERSION = check_output(('git', 'describe')).rstrip().decode()
except Exception:  # different errors on Linux/Windows and Python 2/3
    pass  # stick with default above, expect exception on distributed software


def read_config(test=False):
    '''Read and return either the real config or a test version'''
    path = join('test' if test else DEFAULT_FOLDERNAME, 'config.ini')
    result = ConfigParser()
    result.read(path)
    return result


def write_config(config):
    '''Write the config file'''
    path = join(DEFAULT_FOLDERNAME, 'config.ini')
    with open(path, 'w') as file_:
        config.write(file_)


if __name__ == "__main__":
    ignore = ('VALID', )
    everything = dict(globals())  # copy to avoid changes in size during iter
    pprint({
        k: everything[k]
        for k in everything if k.isupper() and k not in ignore})
