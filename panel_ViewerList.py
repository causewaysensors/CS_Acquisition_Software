"""Log for listing acquisition records loaded from database."""
from datetime import datetime
from time import sleep

import wx

from panel_AcquisitionLog import AutoWidthListCtrl


class panel_ViewerList(wx.Panel):
    """Panel for displaying all the records currently in database

    In the viewer tab of the acquisition software, users can load in all legacy
    data from the database and inspect each record. These records can be sorted
    by date, user or sampleID to facilitate finding particular records.
    """
    def __init__(
            self,
            parent,
            id=wx.ID_ANY,
            position=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=0,
            name=""
            ):

        wx.Panel.__init__(self, parent, id, position, size, style, name)

        # Sizers and objects
        self.vbox = wx.BoxSizer(wx.VERTICAL)

        self.log = AutoWidthListCtrl(
            self, style=wx.LC_REPORT | wx.LC_SINGLE_SEL)

        self.log.InsertColumn(0, "Name", width=150)
        self.log.InsertColumn(1, "Date", width=150)
        self.log.InsertColumn(2, "SampleID", width=150)
        self.log.InsertColumn(3, "Users", width=150)
        self.log.InsertColumn(4, "Tags", width=150)
        self.log.InsertColumn(5, "Notes")

        self.logrow = 0

        self.vbox.Add(self.log, 1, wx.EXPAND)
        self.SetSizer(self.vbox)

        # Bind internal events
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColSelected, self.log)

        self.Clear()
        self.sortType = "Date_Descending"

    def OnColSelected(self, event):
        """Sort records according to name of column in either direction."""
        columnSelected = event.m_col
        col = self.log.GetColumn(columnSelected)
        columnSelected = col.GetText()

        # Only sort Name, Date and SampleID (others make no sense)
        if columnSelected not in ['Name', 'Date', 'SampleID']:
            return

        tempstring_Descending = "%s_%s" % (columnSelected, "Descending")
        tempstring_Ascending = "%s_%s" % (columnSelected, "Ascending")

        # If it is already the sort type
        if tempstring_Descending == self.sortType:

            self.sortType = tempstring_Ascending

        elif tempstring_Ascending == self.sortType:

            # Switch to descending
            self.sortType = tempstring_Descending

        else:

            # Name and Sample are ascending, date is descending
            if columnSelected == "Name" or columnSelected == "SampleID":
                self.sortType = tempstring_Ascending
            elif columnSelected == "Date":
                self.sortType = tempstring_Descending

        self.SortItems()

        # Sort based on column
        event.Skip()

    def SortItems(self):
        """Sort all records according to current rules."""
        # If not results, don't sort
        if len(self.column_Name) == 0:
            return

        # Create sorting array
        temparray = []
        i = 0

        if self.sortType == 'Name_Ascending':
            # Create the temp array to sort
            for elem in self.column_Name:
                temparray.append([i, elem])
                i = i + 1
            temparray.sort(key=lambda x: (x[1] == "", x[1].lower()))

        elif self.sortType == 'Name_Descending':
            # Create the temp array to sort
            for elem in self.column_Name:
                temparray.append([i, elem])
                i = i + 1
            temparray.sort(
                key=lambda x: (x[1] == "", x[1].lower()), reverse=True)

        elif self.sortType == 'Date_Ascending':
            for elem in self.column_Date:
                temparray.append([i, elem])
                i = i + 1
            temparray.sort(
                key=lambda x: datetime.strptime(x[1], '%d_%m_%Y'))

        elif self.sortType == 'Date_Descending':
            for elem in self.column_Date:
                temparray.append([i, elem])
                i = i + 1
            temparray.sort(
                key=lambda x: datetime.strptime(x[1], '%d_%m_%Y'),
                reverse=True,
                )

        elif self.sortType == 'SampleID_Ascending':
            for elem in self.column_SampleID:
                temparray.append([i, elem])
                i = i + 1
            temparray.sort(key=lambda x: (x[1] == "", x[1].lower()))

        elif self.sortType == 'SampleID_Ascending':
            for elem in self.column_SampleID:
                temparray.append([i, elem])
                i = i + 1
            temparray.sort(
                key=lambda x: (x[1] == "", x[1].lower()),
                reverse=True,
                )

        old_column_Name = self.column_Name
        old_column_Date = self.column_Date
        old_column_SampleID = self.column_SampleID
        old_column_Users = self.column_Users
        old_column_Tags = self.column_Tags
        old_column_Notes = self.column_Notes

        array_indices = [elem[0] for elem in temparray]

        self.Clear()

        # Reorder all the other arrays
        for index in array_indices:
            self.column_Name.append(old_column_Name[index])
            self.column_Date.append(old_column_Date[index])
            self.column_SampleID.append(old_column_SampleID[index])
            self.column_Users.append(old_column_Users[index])
            self.column_Tags.append(old_column_Tags[index])
            self.column_Notes.append(old_column_Notes[index])

        self.Redraw()

    def AddLines(
            self,
            array_Name,
            array_Date,
            array_SampleID,
            array_Users,
            array_Tags,
            array_Notes,
            ):
        """Add records from database to log and sort."""
        self.column_Name = array_Name
        self.column_Date = array_Date
        self.column_SampleID = array_SampleID
        self.column_Users = array_Users
        self.column_Tags = array_Tags
        self.column_Notes = array_Notes

        self.SortItems()

        self.Redraw()

    def Clear(self):
        """Clear the log and clear all arrays."""
        # Clear everything and reset dictionary
        self.logrow = 0
        self.log.DeleteAllItems()

        # Variables that control the log
        self.column_Name = []
        self.column_Date = []
        self.column_SampleID = []
        self.column_Users = []
        self.column_Tags = []
        self.column_Notes = []

    def Redraw(self):
        """Redraw the after sort, delete data or load new data."""
        # Clear everything and reset dictionary
        self.logrow = 0
        self.log.DeleteAllItems()

        for row in range(len(self.column_Name)):

            index = self.log.InsertStringItem(
                self.logrow, self.column_Name[row])
            self.log.SetStringItem(index, 1, self.column_Date[row])
            self.log.SetStringItem(index, 2, self.column_SampleID[row])

            self.log.SetStringItem(index, 3, ", ".join(self.column_Users[row]))

            self.log.SetStringItem(index, 4, ", ".join(self.column_Tags[row]))

            self.log.SetStringItem(index, 5, self.column_Notes[row])

            self.logrow += 1


class panel_ViewerList_test(wx.App):
    """Class for testing panel_ViewerList."""
    def __init__(self, auto_close=True):
        """Place panel in frame and test opening, listing data and closing."""
        super(panel_ViewerList_test, self).__init__()

        # Setup frame and sizers
        frame = wx.Frame(None)
        sizer = wx.BoxSizer(wx.VERTICAL)
        search_pan = panel_ViewerList(frame)
        sizer.Add(search_pan, 1, wx.EXPAND | wx.ALL, 20)
        frame.Show()
        frame.SetSizerAndFit(sizer)
        frame.Maximize()
        frame.Layout()

        # Create sample data
        array_Name = [
            'Experiment-30_01_2018',
            'Experiment_2-30_01_2018']
        array_Date = ['30_01_2018', '30_01_2018']
        array_SampleID = ['ID 1', 'ID 2']
        array_Users = [['User 1', 'User 2'], ['User 1']]
        array_Tags = [['Tag 1', 'Tag 2'], ['Tag 1']]
        array_Notes = ['Notes 1', 'Notes 2']

        # List of functions to run (func, args)
        listTests = [
            [search_pan.AddLines, (
                array_Name,
                array_Date,
                array_SampleID,
                array_Users,
                array_Tags,
                array_Notes,
                )],
            [search_pan.Redraw, ()],
            [search_pan.Clear, ()],
            [search_pan.AddLines, (
                array_Name,
                array_Date,
                array_SampleID,
                array_Users,
                array_Tags,
                array_Notes,
                )],
            ]
        for function, arguments in listTests:
            sleep(0 if auto_close else 1)
            function(*arguments)

        if auto_close:
            self.TopWindow.Destroy()


if __name__ == '__main__':
    panel_ViewerList_test(False).MainLoop()
