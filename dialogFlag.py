from wx import App

from wxfb.layout import wxFB_dialog_flag


class dialogFlag(wxFB_dialog_flag):
    def __init__(
            self,
            parent,
            flagTime,
            description="",
            colour="Red",
            baseline=False
            ):

        # References
        self.parent = parent
        self.flagTime = flagTime

        # Initialize parent class
        wxFB_dialog_flag.__init__(self, parent)

        # Set initial values
        self.text_time.SetLabelText(str(flagTime))
        self.textCtrl_Description.SetLabelText(description)
        self.choice_Colour.SetStringSelection(colour)
        self.choice_Baseline.SetStringSelection(str(baseline))

    def OnButton_DialogFlag_Ok(self, event):

        # Post the event so the GUI redraws the text
        description = self.textCtrl_Description.GetValue()
        colour = self.choice_Colour.GetStringSelection()
        baseline = (self.choice_Baseline.GetStringSelection() == "True")

        # Draw on graph
        graph = self.parent.panelAcquisitionGraph
        graph.AddFlag(flagTime=self.flagTime, baseline=baseline, colour=colour)
        self.parent.panelAcquisitionLog.AddFlag(
            time=self.flagTime,
            sensor1_counts=graph.crosshair_y1_counts,
            sensor2_counts=graph.crosshair_y2_counts,
            description=description,
            baseline=baseline,
            colour=colour,
            )

        # Re-enable GUI and destroy dialog
        self.parent.Enable(True)
        self.Destroy()

        event.Skip()

    def OnButton_DialogFlag_Cancel(self, event):

        # Re-enable GUI and destroy dialog
        if self.parent:
            self.parent.Enable(True)
        self.Destroy()

        event.Skip()


class dialogFlag_test(App):
    '''Class to test dialogFlag.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close after tests run'''
        super(dialogFlag_test, self).__init__()

        # Create dialog with sample
        dialog = dialogFlag(parent=None, flagTime=10)

        # Depending on auto_close, dialog needs opened differently
        if auto_close:
            dialog.Show()
        else:
            dialog.ShowModal()
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogFlag_test(False).MainLoop()
