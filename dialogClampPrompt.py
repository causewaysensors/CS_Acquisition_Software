from wx import App

from wxfb.layout import wxFB_dialog_clampPrompt


class dialogClampPrompt(wxFB_dialog_clampPrompt):
    def __init__(self, parent):

        # Initialize parent class
        wxFB_dialog_clampPrompt.__init__(self, parent)

        # Use the parent methods
        self.parent = parent

    def OnButton_MoveClampIn(self, event):

        self.parent.MoveClamp("In")

        event.Skip()

    def OnButton_MoveClampOut(self, event):

        self.parent.MoveClamp("Out")

        event.Skip()

    def OnButton_Continue(self, event):

        self.Destroy()
        event.Skip()


class dialogClampPrompt_test(App):
    '''Class to test dialogClampPrompt.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close after tests run'''
        super(dialogClampPrompt_test, self).__init__()

        # Depending on auto_close, dialog needs opened differently
        if auto_close:
            dialogClampPrompt(None).Show()
        else:
            dialogClampPrompt(None).ShowModal()
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogClampPrompt_test(False).MainLoop()
