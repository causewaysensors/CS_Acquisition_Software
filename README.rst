++++++
README
++++++

For installation instructions see `INSTALLATION.md <./INSTALLATION.md>`__.

Calibration data specification
==============================

All readings come back from the `Arduino` and are saved directly without
converting types —  passed through directly not interpreted. All numbers are
recorded as strings in the output `JSON`.


The actual frequency of measurements by the `Arduino` is either 8 hertz, 4
hertz, or 2 hertz, the plotting is likely different. The corresponding
idealised integration time is either 125, 250 or 500 milliseconds. The
limitations of the hardware mean that some dead time is required. The actual
integration time is 10 to 12.7 milliseconds lower than the idealised
integration time.

The integration time is specified as a number of ``cycles`` in effect this is
a limit on the time resolution of the sensor. This resolution is 2.7
milliseconds plus or minus six percent (calculated from the data sheet). For
each sensor the duration of a large number of these cycles is measured and
then an accurate duration for a single cycle calculated returned in
``increments``.

The actual integration time can be calculated from ``increments`` multiplied
by ``cycles`` for the right ``frequency``.

The sensor is capable of a maximum integration time of :math:`(2.7 + 6%) x 255
= 729` milliseconds. 255 is the maximum number that can be represented in
eight bits. Because of this limitation if a one hertz frequency or a one
second idealised integration time is required, we simply integrate for an
idealised integration time of half a second and multiply the result by two.

In future it maybe helpful to calculate another set of counts for the
second sensor and record these, even if the `Arduino` only uses one set of
counts to select a single gain and frequency. We cannot anticipate ever using
two sensors with two separate gains and frequencies in the acquisition
software.

:``cycles``:
    The number of increments that each sensor would integrate for given
    each frequency.

    One set per sensor and each set has one number per frequency (``'2'``,
    ``'4'`` or ``'8'``). Valid numbers of cycles are 0 to 255.

:``counts``:
    Twelve numbers - counts read from sensor one for each frequencies
    times and gain as explained below.

    Each count can be between 0 and 65535. 65535 is the maximum number that
    can be represented in a sixteen bit integer.

    The order is as follows; starting with gain ``x1`` one number for each
    frequency ``'8'``, ``'4'`` and ``'2'``. Then three more numbers for
    ``x8``, three for ``x16`` and finally three for ``x111``, all in the same
    order.

:``frequency``:
    The frequency selected by the `Arduino`; one of ``'2'``, ``'4'`` or
    ``'8'``. Units are hertz.

:``gain``:
    Selected gain: whichever one of ``'x1'``, ``'x8'``, ``'x16'`` or ``'x111'``
    that was chosen.

:``increments``:
    The resolution of each sensor. Units are milliseconds.

    A dictionary including the measured minimum integration time for each
    sensor. The valid range is 2.7 milliseconds plus or minus 6% (calculated
    from data sheet).

Example data file:

.. formatted with: jq --indent 4 -S . file.json

.. code:: javascript

    {
        "counts": [
            "123",
            "234",
            "345",
            "456",
            "567",
            "678",
            "789",
            "8901",
            "9012",
            "12345",
            "23456",
            "34567"
        ],
        "cycles": {
            "1": {
                "2": "160",
                "4": "79",
                "8": "40"
            },
            "2": {
                "2": "161",
                "4": "81",
                "8": "41"
            }
        },
        "frequency": "2",
        "gain": "x8",
        "increments": {
            "1": "2.72",
            "2": "2.71"
        }
    }

Coding conventions and style
============================

-   In the absence of a strong reason otherwise, stick to tool defaults;
    prefer default configuration over custom
-   It is better to list `flake8` errors or warnings explicitly at the most
    specific level; for example prefer two lines ``E241`` and ``E242`` to a
    single line ``E24``
-   Prefer conventions codified in ``pycodestyle`` or ``flake8`` over
    third party tutorials; for example ``1 | 2`` to avoid ``E227`` rather than
    ``1|2`` to match the `wxPython` tutorials
-   Where possible code to the documented behaviour rather than the
    implemented, for example use the ignore list from the `flake8` docs_
    rather than the source code_
-   Errors should result in a message printed to the console, unless:

    -   more that ten errors of that type are expected per experiment or
    -   the error's cause is obvious when looking at the GUI window

-   ``print`` statements should only be committed in ``try/except`` blocks
-   Where a list of imports fits easily on one line, list each imported
    name explicitly for example prefer the style demonstrated on the second
    line to the first line below:

    .. code::

        import wx  # no
        from wx import App, Frame  # yes
-   SQL statements should be terminated with a semi-colon ``;``

.. _docs: http://pycodestyle.pycqa.org/en/latest/intro.html#error-codes
.. _code: https://github.com/PyCQA/flake8/blob/master/src/flake8/
    defaults.py#L15

flake8
======

This repository includes configuration for ``flake8`` to check for a subset of
errors. [#]_

Line-wrapping
=============

Avoid ``\`` where possible.

.. code:: python

    # if this line needs wrapped:
    if locals['serial'] == "SETTING" and locals['description'] == \
    "Calibration":
        print()

    # prefer:
    if (locals['serial'] == "SETTING"
            and locals['description'] == "Calibration"):
        print()

    # to:
    if (locals['serial'] == "SETTING" and
            locals['description'] == "Calibration"):
        print()

    # if this line needs wrapped:
    dialog = print('self', 'one', 'two', 'three', 'four', 'five', 'six', \
    'seven')

    dialog = print(
        'self', 'one', 'two', 'three', 'four', 'five', 'six', 'seven')

    # if this line needs wrapped:
    dialog = print('self', 'one', 'two', 'three', 'four', 'five', 'six', \
    'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', \
    'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen', \
    'twenty')

    # prefer
    dialog = print(
        'self',
        'one',
        'two',
        'three',
        'four',
        'five',
        'six',
        'seven',
        'eight',
        'nine',
        'ten',
        'eleven',
        'twelve',
        'thirteen',
        'fourteen',
        'fifteen',
        'sixteen',
        'seventeen',
        'eighteen',
        'nineteen',
        'twenty',
        )

    # try to avoid spending time lining up arguments further into a line
    dialog = dialogCalibrateSensorsResults('self',
                                        'calibration_counts',
                                        'status',
                                        'ms_1',
                                        'ms_2',
                                        'gain_1',
                                        'gain_2')

.. [#] All of the names of local Python files that may be imported are
    listed in ``.flake8`` because this code is not setup as a package, so
    relative imports are impossible.

.. vim: ft=rst
