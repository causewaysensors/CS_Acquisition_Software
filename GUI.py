from csv import writer
from datetime import date, datetime
from json import dump, dumps, loads
try:
    from os import startfile
except ImportError:
    def startfile(_ignore): pass  # only used to launch .chm help file
from os.path import dirname, join
from sqlite3 import Error
from textwrap import dedent
from threading import Lock, Thread
from time import sleep

from scipy.stats import linregress

from serial import Serial, SerialException

import wx

from dialogCalibrateChip import dialogCalibrateChip

from dialogCalibrateChipResults import dialogCalibrateChipResults

from dialogCalibrateSensorsResults import dialogCalibrateSensorsResults

from dialogClampCustom import dialogClampCustom

from dialogClampPrompt import dialogClampPrompt

from dialogFileSetup import dialogFileSetup

from dialogFlag import dialogFlag

from dialogLoading import dialogLoading

from dialogOptions import dialogOptions

from dialogPreferences import dialogPreferences

from lib.db.models import DELETE_STATEMENT, SELECT_STATEMENT
from lib.settings import DEFAULT_FOLDERNAME, VERSION, write_config

from wxfb.layout import wxFB_GUI


class GUI(wxFB_GUI):
    def __init__(self, parent, foldername=DEFAULT_FOLDERNAME):

        # Initialize parent class
        wxFB_GUI.__init__(self, parent)

        # Set initial page
        self.m_notebook1.ChangeSelection(0)

        # GUI timer refresh
        self.timerMS = 50

        # Serial Variables
        self.comport = None

        self.serialType = None
        self.serialDescription = None
        self.serialValues = []
        # Using threads in dialogs so need to be safe4
        self.serialLock = Lock()

        # Timer variables
        self.timerRunning = False
        # Time the timer was started (crosshair location when start pressed)
        self.timerTime = 0
        self.timerEnd = 0

        # Display variables
        self.fileName = None
        self.foldername = foldername

        self.dataStreaming = False
        self.firstValue = True

        self.xBuffer = 10
        self.RPS = 1
        self.plotRatio = "x1"

        # Bind events from sub panels
        self.Bind(
            self.panelAcquisitionGraph.EVT_CROSSHAIRCHANGE,
            self.OnCrosshairChange,
            )
        self.Bind(self.panelAcquisitionLog.EVT_FLAGDELETED, self.OnFlagDeleted)
        self.Bind(self.panelAcquisitionLog.EVT_FLAGEDITED, self.OnFlagEdited)
        self.Bind(
            wx.EVT_LIST_ITEM_SELECTED,
            self.OnViewerListSelected,
            self.panel_ViewerList.log,
            )

        # Control the close
        self.Bind(wx.EVT_CLOSE, self.OnExit)

# ENABLE/DISABLE ##############################################################

    def EnableDisable(self):

        # ---------------------------------------------------------------------
        # MENUS
        # ---------------------------------------------------------------------

        # FILE/NEW - Only available if data stopped and data exists
        if self.firstValue or self.dataStreaming:
            self.menu_FileNew.Enable(False)
        else:
            self.menu_FileNew.Enable(True)

        # OPTIONS/OPTIONS - Only available if data is not running
        if self.dataStreaming:
            self.menu_OptionsOptions.Enable(False)
        else:
            self.menu_OptionsOptions.Enable(True)

        # CLAMP/IN-OUT - Only available if comport connected and data not
        # running
        if self.comport is None:
            self.menu_ClampIn.Enable(False)
            self.menu_ClampOut.Enable(False)
        elif self.dataStreaming:
            self.menu_ClampIn.Enable(False)
            self.menu_ClampOut.Enable(False)
        else:
            self.menu_ClampIn.Enable(True)
            self.menu_ClampOut.Enable(True)

        # CLAMP/CUSTOM - Only available in service mode with comport
        if self.menu_ServiceService.IsChecked() and self.comport is not None:
            self.menu_ClampCustom.Enable(True)
        else:
            self.menu_ClampCustom.Enable(False)

        # ---------------------------------------------------------------------
        # BUTTONS - HARDWARE
        # ---------------------------------------------------------------------

        # Disable whole panel if data is streaming
        if self.dataStreaming:
            self.hardwarePan.Enable(False)
        else:
            self.hardwarePan.Enable(True)

        # CALIBRATE DETECTOR - Only available if comport available and data not
        # streaming
        if self.comport is not None and not self.dataStreaming:
            self.btn_calibrateDetector.Enable(True)
        else:
            self.btn_calibrateDetector.Enable(False)

        # CALIBRATE CHIP - Only available if comport is available and data is
        # not streaming
        if self.comport is not None and not self.dataStreaming:
            self.btn_calibrateChip.Enable(True)
        else:
            self.btn_calibrateChip.Enable(False)

        # ---------------------------------------------------------------------
        # BUTTONS - ACQUISITION
        # ---------------------------------------------------------------------

        # DATA/START-STOP - Comport
        if self.comport is not None:
            self.btn_dataStartStop.Enable(True)
        else:
            self.btn_dataStartStop.Enable(False)

        # Colour and text depends on datarunning + first value
        if (not self.dataStreaming) and self.firstValue:
            self.btn_dataStartStop.SetLabelText("Start")
            self.btn_dataStartStop.SetBackgroundColour('#66CCFF')
        elif self.dataStreaming:
            self.btn_dataStartStop.SetLabelText("Stop")
            self.btn_dataStartStop.SetBackgroundColour('Red')
        elif not (self.dataStreaming or self.firstValue):
            self.btn_dataStartStop.SetLabelText("Append")
            self.btn_dataStartStop.SetBackgroundColour('#66CCFF')

        # DATA/CLEAR - First value and not data streaming
        if self.firstValue or self.dataStreaming:
            self.btn_dataClear.Enable(False)
        else:
            self.btn_dataClear.Enable(True)

        # FLAG - Filename
        if self.fileName is not None:
            self.btn_flag.Enable(True)
        else:
            self.btn_flag.Enable(False)

        # ---------------------------------------------------------------------
        # BUTTONS - VIEWER
        # ---------------------------------------------------------------------
        if self.panel_ViewerList.log.GetSelectedItemCount() > 0:
            self.btn_ExportToTxt.Enable(True)
            self.btn_DeleteFile.Enable(True)
        else:
            self.btn_ExportToTxt.Enable(False)
            self.btn_DeleteFile.Enable(False)

        # Refresh everything
        self.Refresh()
        self.Update()

# EVENTS_MENU #################################################################

    def OnMenu_FileNew(self, event):
        """
        Keep old file and open new one
        """

        # Reset everything
        self.Reset()

        # File Name
        self.fileName = None

        # Calibration has been carried out, check if it is ok
        if self.panelAcquisitionGraph.multiplier_2 != 1.0:

            dialog = wx.MessageBox(
                'Keep calibration?', 'Calibration', wx.YES_NO)

            if dialog == wx.NO:
                self.panelAcquisitionGraph.multiplier_2 = 1.0

        event.Skip()

    def Reset(self):
        """
        Resets everything except the fileName
        """

        # Right hand side
        self.btn_dataStartStop.SetLabel("Start")
        self.btn_dataStartStop.SetBackgroundColour('#66CCFF')

        self.choice_displayType.SetStringSelection("Both Sensors")

        # Crosshair text
        self.text_crosshairTime.SetLabelText("-")
        self.text_crosshairY1.SetLabelText("-")
        self.text_crosshairY2.SetLabelText("-")
        self.text_crosshairY2Y1.SetLabelText("-")
        self.crosshairPan.Layout()

        # Reset the graph and log panels
        self.panelAcquisitionGraph.Reset()
        self.panelAcquisitionLog.Clear()

        # First value for baseline
        self.firstValue = True
        self.dataStreaming = False

        # Menus & Buttons
        self.EnableDisable()

    def OnExit(self, event):

        # Try and clean up any busy dialogs (C++ error causes them to remain)
        if hasattr(self, 'bi'):
            del self.bi

        self.Destroy()
        event.Skip()

    def OnMenu_OptionsPreferences(self, event):
        '''Finish preferences from the options menu: sound and interval'''
        dialog = dialogPreferences(
            self, self.autosaveInterval, self.timerSound)
        if dialog.ShowModal() == wx.ID_OK:
            self.autosaveInterval = dialog.spinCtrl_AutoSave.GetValue() * 60
            self.timerSound = dialog.choice_TimerSound.GetStringSelection()
            self.configParser.set(
                'Data',
                'autosaveInterval',
                str(self.autosaveInterval),
                )
            self.configParser.set('GUI', 'timerSound', str(self.timerSound))
            write_config(self.configParser)

    def OnMenu_OptionsOptions(self, event):
        dialog = dialogOptions(self, self.xBuffer, self.RPS, self.plotRatio)
        dialog.ShowModal()
        event.Skip()

    def OnMenu_ClampIn(self, event):
        self.MoveClamp("In")
        event.Skip()

    def OnMenu_ClampOut(self, event):

        dialog = wx.MessageDialog(
            self,
            'Ensure the cuvette has been drained',
            'Move clamp',
            wx.OK | wx.CANCEL | wx.STAY_ON_TOP | wx.ICON_QUESTION,
            )

        if dialog.ShowModal() == wx.ID_OK:
            self.MoveClamp("Out")

        event.Skip()

    def OnMenu_ClampCustom(self, event):

        dialog = dialogClampCustom(self)
        dialog.ShowModal()

        event.Skip()

    def MoveClamp(self, direction):

        # Disable everything during movement
        self.Enable(False)

        # Write to arduino
        if direction == "In":
            self.WriteSerial('clamp|%s|%s|%d|%d\n' % (
                "In", self.clampRange, self.clampSpeed, self.clampTimeIn))
        else:
            self.WriteSerial('clamp|%s|%s|%d|%d\n' % (
                "Out", self.clampRange, self.clampSpeed, self.clampTimeOut))

        # Create dialog
        self.bi = wx.BusyInfo("Moving clamp %s, please wait" % (
            direction.lower()), self)

        # Wait for the clamping to stop
        while True:
            self.ReadSerial_Blocking(lock=True)

            # Wait for the calibration results
            if (self.serialType == "INFO" and
               self.serialDescription == "clamp" and
               self.serialValues[0] == "finished"):
                break

        # Destroy dialog
        del self.bi

        # Re-enable everything
        self.Enable(True)
        self.SetFocus()

    def OnMenu_ServiceService(self, event):

        if self.menu_ServiceService.IsChecked():

            dlg = wx.PasswordEntryDialog(self, "Enter password")
            dlg.ShowModal()
            result = dlg.GetValue()

            if result == "nanofreaks":

                wx.MessageBox('Service mode enabled', 'Service Mode', wx.OK)
                self.menu_ServiceService.Check(True)

            else:
                self.menu_ServiceService.Check(False)

        else:
            wx.MessageBox('Service mode disabled', 'Service Mode', wx.OK)
            self.menu_ServiceService.Check(False)

        # Enable/disable everything
        self.EnableDisable()

        event.Skip()

    def OnMenu_HelpAbout(self, event):
        '''Display the about dialog, including the version number'''
        info = wx.AboutDialogInfo()
        info.Name = "Causeway Sensors Acquisition Software"
        info.Version = VERSION
        info.Description = "Acquisition software for the Causeway Affinity"
        info.WebSite = ("http://www.causewaysensors.com", "Website")
        info.Developers = ["Breandan Hill"]
        wx.AboutBox(info)

    def OnMenu_HelpHelp(self, event):
        startfile(join(
            dirname(__file__),
            'assets',
            'help',
            'Causeway Sensors Help.chm',
            ))


# EVENTS_HARDWARE #############################################################

    def OnButton_ConnectHardware(self, event):

        # Disable everything at start
        # Don't re-enable until thread is finished
        self.Enable(False)

        # Show the dialog
        self.dialog = dialogLoading(self, "Connecting hardware\nPlease wait")
        self.dialog.Show()

        # Start the thread
        self.thread = Thread(target=self.Thread_ConnectHardware, args=(2,))
        self.thread.start()

    def Thread_ConnectHardware(self, additionalwait=0):

        # Variable which is changed if sensor found
        validBiosensor = False

        # Get the serial lock - KEEP IT THE WHOLE TIME
        self.serialLock.acquire()

        # Test all comports to see which ones are even open
        listports = []

        for i in range(256):

            portname = "COM%s" % (i+1)

            # Try and close any previously opened ports
            # Don't print error due to multiple expected failures
            if hasattr(self, 'ser'):
                try:
                    self.ser.close()
                except SerialException:
                    pass

            # Open and close the serial port to see if it even exists
            # Don't print error due to multiple expected failures
            try:
                self.ser = Serial(portname)
                self.ser.close()
                listports.append(portname)
            except SerialException:
                pass

        self.dialog.UpdateProgress(50)

        # For each available port, try and detect "connected"
        for port in listports:

            self.comport = port

            # Try and close any previously opened ports
            # Don't print error due to multiple expected failures
            if hasattr(self, 'ser'):
                try:
                    self.ser.close()
                except SerialException:
                    pass

            # Try to send and receive from sensors
            # Don't print error due to multiple expected failures
            try:

                # Open the serial at each address
                self.ser = Serial(str(port), 9600, timeout=1)

                # Write to the biosensor to go into standby mode (it may
                # already be)
                self.WriteSerial("stopData\n")

                # Send connect through the serial
                self.WriteSerial("connect\n")

                # May need to loop several times
                for i in range(5):

                    self.ReadSerial_Blocking()

                    # Valid biosensor - Keep port open
                    if (self.serialType == "INFO"
                            and self.serialDescription == "modelFirmware"):

                        validBiosensor = True

                        self.model = str(self.serialValues[0])
                        self.firmware = str(self.serialValues[1])

                        break

            except SerialException:
                pass

        self.dialog.UpdateProgress(100)

        # BIOSENSOR NOT FOUND
        if not validBiosensor:

            # Reset com port and try to close
            # Don't print error due to multiple expected failures
            self.comport = None

            if hasattr(self, 'ser'):
                try:
                    self.ser.close()
                except SerialException:
                    pass

            # Small additional wait if required
            sleep(additionalwait)

            # Label everything
            self.text_model.SetLabel('-')
            self.text_firmware.SetLabel('-')

            # Enable the buttons/menus
            self.EnableDisable()

            # Destroy the old
            self.dialog.Destroy()

            # Create new warning dialog
            self.dialog = wx.MessageBox(
                (
                    'No sensor found! Ensure the sensor is connected and '
                    'not in use by another program'
                    ),
                'Error',
                wx.OK | wx.ICON_ERROR,
                )

        # BIOSENSOR FOUND
        else:

            # Change the message on the loading
            self.dialog.ChangeMessage(
                "Causeway %s found at %s\nConnecting" % (
                    self.model, self.comport)
                )
            self.Refresh()

            # Small additional wait if required
            sleep(additionalwait)

            # Label everything
            self.text_model.SetLabel(self.model)
            self.text_firmware.SetLabel(self.firmware)

            # Enable the buttons/menus
            self.EnableDisable()

            # Destroy dialog
            self.dialog.Destroy()

        # Release the lock
        self.serialLock.release()

        # Re-enable everything
        self.Enable(True)
        self.SetFocus()

        return True

    def OnButton_CalibrateDetector(self, event):

        # Disable the GUI so the user can do nothing stupid
        # Don't re-enable until the thread is finished
        self.Enable(False)

        dialog = dialogClampPrompt(self)
        dialog.ShowModal()

        # Show the loading dialog
        self.dialog = dialogLoading(
            self, "Preparing to calibrate\nplease wait")
        self.dialog.Show()

        # Start the thread
        self.thread = Thread(target=self.Thread_CalibrateDetector, args=(1,))
        self.thread.start()

        event.Skip()

    def Thread_CalibrateDetector(self, additionalwait=0):

        # Get the serial lock - KEEP IT THE WHOLE TIME
        self.serialLock.acquire()

        # Short wait
        sleep(additionalwait)

        self.ser.write(
            "setMinCounts|%d\n" % self.spinCtrl_minCounts.GetValue())
        self.ser.write(
            "setMaxCounts|%d\n" % self.spinCtrl_maxCounts.GetValue())
        self.ser.write(
            "setRPS|%s\n" % str(self.choice_RPS.GetStringSelection()))

        self.dialog.ChangeMessage("Calibrating sensors\nPlease wait")
        self.ser.write("calibrate\n")

        status = "Failed"
        progress = 0

        for i in range(20):

            progress = progress + 5

            self.dialog.UpdateProgress(progress)

            self.ReadSerial_Blocking()

            # Wait for the calibration results
            read = (self.serialType, self.serialDescription)
            if read == ("SETTING", "Calibration"):

                # 0) Success or failure of calbration
                status = str(self.serialValues[0])

                # 1) Length increment in ms (2dp)
                increment_1, increment_2 = self.serialValues[1].split(',')

                # 2) Number of cycles in each acquisition frequency for each
                # sensor
                [cycles_low_1, cycles_low_2, cycles_mid_1, cycles_mid_2,
                    cycles_high_1, cycles_high_2] = self.serialValues[
                    2].split(',')

                # 3) Selected values for acquisition freq and gain
                acquisition_freq, sel_gain = self.serialValues[3].split(',')

                # 4) List of counts for all gains and 8Hz, 4Hz, 2Hz acqusiition
                calibration_counts = self.serialValues[4].split(',')

                calibration_dict = {
                    "counts": calibration_counts,
                    "cycles": {
                        "1": {
                            "8": cycles_low_1,
                            "4": cycles_mid_1,
                            "2": cycles_high_1,
                            },
                        "2": {
                            "8": cycles_low_2,
                            "4": cycles_mid_2,
                            "2": cycles_high_2,
                            },
                            },
                    "gain": sel_gain,
                    "increments": {
                        "1": increment_1,
                        "2": increment_2,
                        },
                    "frequency": acquisition_freq,
                    }

                # Print this calibration
                destination = join(self.foldername, 'calibrationData.txt')
                with open(destination, 'w') as outfile:
                    dump(calibration_dict, outfile)

                break

        # Release the lock
        self.serialLock.release()

        # Short wait if required
        sleep(additionalwait)

        # Destroy the old dialog
        self.dialog.Destroy()

        # Display results (different depending on rights)
        if status == "Failed":
            dialog = wx.MessageBox(
                'No calibration occured\nCheck everything', 'Error', wx.OK)

        elif self.menu_ServiceService.IsChecked():
            dialog = dialogCalibrateSensorsResults(
                self,
                results=calibration_counts,
                status=status,
                ms_1=float(increment_1) * float(calibration_dict[
                    "cycles"]["1"][acquisition_freq]),
                ms_2=float(increment_2) * float(calibration_dict[
                    "cycles"]["2"][acquisition_freq]),
                gain=sel_gain)

            dialog.ShowModal()

        else:

            if status == "Success":
                dialog = wx.MessageBox(
                    'Sensors calibrated', 'Success', wx.OK)
            elif status == "Failed Low":
                dialog = wx.MessageBox(
                    'Not enough light to calibrate\nCheck LED is powered',
                    'Error',
                    wx.OK,
                    )

            elif status == "Failed High":
                dialog = wx.MessageBox(
                    'Too much light to calibrate\nCheck sample',
                    'Error',
                    wx.OK,
                    )

        # Re-enable the GUI
        self.Enable(True)
        self.SetFocus()

        return 0

    def OnButton_CalibrateChip(self, event):

        # Disable the GUI until the dialog is closed
        self.Enable(False)

        # Get flags
        dict_flags = self.panelAcquisitionLog.dict_flags

        # Create dialog
        dialog = dialogCalibrateChip(self, dict_flags)

        if dialog.ShowModal() == wx.ID_OK:

            # Check if enough times have been received
            if dialog.listPan.log.GetSelectedItemCount() == 0:
                wx.MessageBox('Nothing selected', 'Error', wx.OK)
                return

            # Get all selections
            selection = []
            index = dialog.listPan.log.GetFirstSelected()
            selection.append(index)
            while len(selection) < dialog.listPan.log.GetSelectedItemCount():
                index = dialog.listPan.log.GetNextSelected(index)
                selection.append(index)

            # Convert to times and get the set
            timeselection = [
                float(dialog.listPan.log.GetItemText(i)) for i in selection]
            timeselection = list(set(timeselection))

            # Check if enough times have been received
            if len(timeselection) < 2:
                wx.MessageBox(
                    'Too few calibration times selected', 'Error', wx.OK)
                return

            # Get the Y values
            array_y1 = []
            array_y2 = []

            for i in timeselection:
                array_y1.append(dict_flags[i]['sensor1_percent'])
                array_y2.append(dict_flags[i]['sensor2_percent'])

            # This is how much the second one need corrected compared to the
            # first
            result = linregress(array_y1, array_y2)
            slope = result[0]
            self.panelAcquisitionGraph.multiplier_2 = 1.0/float(slope)
            if self.menu_ServiceService.IsChecked():
                dialog = dialogCalibrateChipResults(*result)
                dialog.ShowModal()
            else:
                wx.MessageBox('Chip calibrated', 'Calibration result', wx.OK)

        # Re-enable the GUI
        self.Enable(True)
        self.SetFocus()

        event.Skip()
        return

# EVENTS_ACQUISITION ##########################################################

    def OnAcquisitionSashPosChanging(self, event):

        event.Skip()

    def OnAcquisitionSashPosChanged(self, event):

        event.Skip()

    def OnButton_TimerStartStop(self, event):
        self.Enable(False)  # Disable the GUI to avoid double clicks

        if self.btn_timerStartStop.GetLabelText() == "Start":
            try:  # Get the countdown and check it is valid
                self.timerTime = float(self.textCtrl_timer.GetValue())
            except ValueError:
                return

            self.timerRunning = True
            self.timerEnd = (  # Get time from crosshair
                self.panelAcquisitionGraph.crosshair_time + self.timerTime)
            self.btn_timerStartStop.SetLabel("Stop")

        elif self.btn_timerStartStop.GetLabelText() == "Stop":
            self.timerRunning = False
            self.btn_timerStartStop.SetLabel("Start")

        # Re-enable the GUI
        self.Enable(True)

        event.Skip()
        return

    def OnChoice_DisplayType(self, event):

        # Get the new type of graph
        newtype = self.choice_displayType.GetString(
            self.choice_displayType.GetSelection())

        # Change the panelAcquisitionLog and the panelAcquisitionGraph
        self.panelAcquisitionGraph.ChangeGraphType(newtype)
        self.panelAcquisitionLog.ChangeDisplayType(newtype)

        # Update everything
        self.Refresh()
        event.Skip()

    def OnCrosshairChange(self, event):

        # Generate the text
        if self.panelAcquisitionGraph.crosshair_y1_counts == 0:
            self.text_crosshairTime.SetLabelText("-")
            self.text_crosshairY1.SetLabelText("-")
            self.text_crosshairY2.SetLabelText("-")
            self.text_crosshairY2Y1.SetLabelText("-")
        else:
            graph = self.panelAcquisitionGraph
            self.text_crosshairTime.SetLabelText("%0.2f s" % (
                graph.crosshair_time))
            self.text_crosshairY1.SetLabelText("%0.3f %% (%0.1f)" % (
                graph.crosshair_y1, graph.crosshair_y1_counts))
            self.text_crosshairY2.SetLabelText("%0.3f %% (%0.1f)" % (
                graph.crosshair_y2, graph.crosshair_y2_counts))
            self.text_crosshairY2Y1.SetLabelText("%0.3f %%" % (
                graph.crosshair_y2y1))

        self.crosshairPan.Layout()

        self.Refresh()

    def OnButton_Flag(self, event):

        # Disable the GUI to avoid double clicks
        self.Enable(False)

        if self.panelAcquisitionGraph.crosshair_time != 0:
            self.OnFlag(self.panelAcquisitionGraph.crosshair_time)
        else:
            wx.MessageBox(
                (
                    'Unable to add flag at time = 0. '
                    'Please move crosshair and try again.'
                    ),
                'Error',
                wx.OK | wx.ICON_ERROR,
                )

        # Re-enable the GUI
        self.Enable(True)
        self.SetFocus()

    def OnFlag(self, flagTime, description="", baseline=False, colour="Red"):
        self.dialog = dialogFlag(self, flagTime, description, colour, baseline)
        self.dialog.ShowModal()

    def OnFlagDeleted(self, event):
        self.panelAcquisitionGraph.RemoveFlag(event.time)

    def OnFlagEdited(self, event):
        self.OnFlag(
            event.time, event.description, event.baseline, event.colour)

    def OnButton_DataStartStop(self, event):
        # Disable the GUI to avoid double clicks
        self.Enable(False)

        # Determine the state of the button - Start/Stop/Append
        # -------------------------------------------------------------------

        # START
        if self.btn_dataStartStop.GetLabel() == 'Start':

            # If there is no open file
            if self.fileName is None:

                # Open the file setup dialog
                dialog = dialogFileSetup(
                    parent=self,
                    config=self.configParser,
                    )
                dialog.ShowModal()

            else:
                # Enable/disable everything
                self.dataStreaming = True
                self.EnableDisable()

                # Check if clamp needs moved
                dialog = dialogClampPrompt(self)
                dialog.ShowModal()

                # Tell the arduino to begin data
                self.WriteSerial("newData\n")

        # STOP
        elif self.btn_dataStartStop.GetLabel() == 'Stop':

            # Enable/disable everything
            self.dataStreaming = False
            self.EnableDisable()

            # Tell the arduino to begin data
            self.WriteSerial("stopData\n")
            self.dataStreaming = False

            # Update the database
            self.UpdateDatabase()

        # APPEND
        elif self.btn_dataStartStop.GetLabel() == 'Append':

            # Enable/disable everything
            self.dataStreaming = True
            self.EnableDisable()

            # Check if clamp needs moved
            dialog = dialogClampPrompt(self)
            dialog.ShowModal()

            # Tell the arduino to begin data
            self.WriteSerial("appendData\n")

        # Re-enable the GUI
        self.Enable(True)
        self.SetFocus()

        event.Skip()

    def OnButton_DataClear(self, event):

        # Disable the GUI to avoid double clicks
        self.Enable(False)

        # Clear the graph
        self.panelAcquisitionGraph.Reset()

        # Change the data buttons
        self.btn_dataStartStop.SetLabelText("Start")
        self.btn_dataStartStop.SetBackgroundColour("#66CCFF")

        self.btn_dataClear.Enable(False)

        # Clear the initial data points
        self.firstValue = True

        self.Refresh()

        # Re-enable the GUI
        self.Enable(True)

        event.Skip()

    def OnChoice_DisplayRatio(self, event):
        event.Skip()

# EVENTS_VIEWER ###############################################################

    def OnViewerSashPosChanging(self, event):

        try:
            self.panel_ViewerPlot.figure.tight_layout()
        except ValueError:
            pass

        event.Skip()

    def OnViewerSashPosChanged(self, event):

        try:
            self.panel_ViewerPlot.figure.tight_layout()
        except ValueError:
            pass

        event.Skip()

    def OnDatePicker_From(self, event):
        self.ParseData()
        event.Skip()

    def OnDatePicker_To(self, event):
        self.ParseData()
        event.Skip()

    def OnListBox_SampleID(self, event):
        self.ParseData()
        event.Skip()

    def OnListBox_Tags(self, event):
        self.ParseData()
        event.Skip()

    def OnListBox_Users(self, event):
        self.ParseData()
        event.Skip()

    def OnButton_LoadDatabase(self, event):

        # Disable the GUI to avoid double clicks
        self.Enable(False)

        self.LoadDatabase()

        # Re-enable the GUI
        self.Enable(True)

        event.Skip()

    def OnButton_ExportToTxt(self, event):

        # Disable the GUI to avoid double clicks
        self.Enable(False)

        # Check something is selected
        noItems = self.panel_ViewerList.log.GetSelectedItemCount()

        if noItems == 1:

            # Open a file dialog
            dialog = wx.FileDialog(
                self,
                message="Export to .txt",
                wildcard='.txt',
                style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT,
                )

            if dialog.ShowModal() == wx.ID_OK:
                # Create the arrays
                saveArray = [self.displayed_time,
                             self.displayed_y1_percent,
                             self.displayed_y2_percent,
                             self.displayed_y1_counts,
                             self.displayed_y2_counts]

                # Transpose the matrix
                saveArray = zip(*saveArray)

                # Get the file path
                filepath = dialog.GetPath()

                with open(filepath, 'wb') as csvfile:
                    csvwriter = writer(csvfile, delimiter=',')
                    csvwriter.writerow(
                        ['baseline_times', self.displayed_baseline_times])
                    csvwriter.writerow(
                        ['flag_dict', dumps(self.displayed_flag_dict)])
                    csvwriter.writerow(
                        ['Time', 'y1 %', 'y2 %', 'y1 counts', 'y2 counts'])
                    csvwriter.writerows(saveArray)

        elif noItems > 1:
            wx.MessageBox(
                'Can only export one file at a time',
                'Error',
                wx.OK | wx.ICON_ERROR,
                )
        else:
            wx.MessageBox('No file selected', 'Error', wx.OK | wx.ICON_ERROR)

        # Re-enable the GUI
        self.Enable(True)

        event.Skip()

    def OnButton_DeleteFile(self, event):
        '''Remove an entry from the database'''
        self.Enable(False)

        # Exit if nothing selected
        log = self.panel_ViewerList.log
        if not log.GetSelectedItemCount():
            wx.MessageBox('Nothing selected', 'Error', wx.OK)
            self.Enable()
            return

        # Exit if the user doesn't confirm
        dialog = wx.MessageDialog(
            None,
            'Really delete results from database?',
            'Delete',
            wx.YES_NO | wx.ICON_EXCLAMATION,
            )
        if dialog.ShowModal() == wx.ID_NO:
            self.Enable()
            return

        # Confirm & delete entry, if needed stop acquiring or reset calibration
        selection = log.GetItemText(log.GetFirstSelected())
        if selection != self.fileName:  # not the file we're acquiring into
            self.cur.execute(DELETE_STATEMENT, (selection,))
            self.db.commit()
        else:
            dialog = wx.MessageDialog(
                None,
                'Stop and discard current acquisition?',
                'Delete',
                wx.YES_NO | wx.ICON_EXCLAMATION,
                )
            if dialog.ShowModal() == wx.ID_YES:
                self.WriteSerial("stopData\n")
                self.Reset()
                self.cur.execute(DELETE_STATEMENT, (self.fileName,))
                self.db.commit()

                # If there is any current calibration
                if self.panelAcquisitionGraph.multiplier_2 != 1.0:
                    dialog = wx.MessageDialog(
                        None,
                        'Keep the current calibration?',
                        'Calibration',
                        wx.YES_NO | wx.ICON_EXCLAMATION,
                        )
                    if dialog.ShowModal() == wx.ID_YES:
                        self.panelAcquisitionGraph.multiplier_2 = 1.0

        self.LoadDatabase()  # Reload incase things have changed
        self.Enable()
        event.Skip()
        return

    def OnViewerListSelected(self, event):
        # Get the data for the clicked Name
        index = self.panel_ViewerList.log.GetFirstSelected()
        self.cur.execute(
            SELECT_STATEMENT,
            (self.panel_ViewerList.log.GetItemText(index),),
            )
        rows = self.cur.fetchone()

        # Some values need converted to floats
        self.displayed_time = loads(rows[7])
        self.displayed_y1_percent = loads(rows[10])
        self.displayed_y2_percent = loads(rows[11])
        self.displayed_y1_counts = loads(rows[8])
        self.displayed_y2_counts = loads(rows[9])
        self.displayed_flag_dict = loads(rows[13])
        self.displayed_baseline_times = loads(rows[12])

        self.panel_ViewerPlot.SetData(self.displayed_time,
                                      self.displayed_y1_percent,
                                      self.displayed_y2_percent,
                                      self.displayed_y1_counts,
                                      self.displayed_y2_counts,
                                      self.displayed_flag_dict,
                                      self.displayed_baseline_times)

        self.EnableDisable()

    def ParseData(self):

        # Get limits of data
        min_date = self.datePicker_From.GetValue()
        min_date = self.wxdate2pydate(min_date)

        max_date = self.datePicker_To.GetValue()
        max_date = self.wxdate2pydate(max_date)

        sampleID_choice = self.listBox_SampleID.GetStringSelection()
        user_choice = self.listBox_Users.GetStringSelection()
        tag_choice = self.listBox_Tags.GetStringSelection()

        # Clear all data from the search panel
        self.panel_ViewerList.Clear()

        namesToAdd = []

        # Check against each criteria
        for index in range(len(self.array_Name)):
            # SAMPLE ID
            if (sampleID_choice == "All"
                    or self.array_SampleID[index] == sampleID_choice):
                # USERS
                if (user_choice == "All"
                        or user_choice in self.array_Users[index]):
                    # TAGS
                    if (tag_choice == "All"
                            or tag_choice in self.array_Tags[index]):

                        # Create 3 datetime objects
                        d, m, y = self.array_Date[index].split('_')
                        filedate = date(int(y), int(m), int(d))

                        # DATE
                        if filedate >= min_date and filedate <= max_date:
                            namesToAdd.append(self.array_Name[index])

        # Get the indices of these
        array_Indices = [self.array_Name.index(name) for name in namesToAdd]

        self.panel_ViewerList.AddLines(
            [self.array_Name[index] for index in array_Indices],
            [self.array_Date[index] for index in array_Indices],
            [self.array_SampleID[index] for index in array_Indices],
            [self.array_Users[index] for index in array_Indices],
            [self.array_Tags[index] for index in array_Indices],
            [self.array_Notes[index] for index in array_Indices],
            )

    def LoadDatabase(self):

        # Name
        self.cur.execute(dedent(
            '''SELECT Name, Version, Date, SampleID, Users, Tags, Notes
            FROM CAUSEWAY_DATA;'''))
        rows = self.cur.fetchall()

        # NAMES
        self.array_Name = [str(row[0]) for row in rows]

        # VERSION
        self.array_Version = [str(row[1]) for row in rows]

        # DATES
        self.array_Date = [row[2] for row in rows]
        self.set_Date = self.UniqueValuesFromListOfLists(self.array_Date)
        self.set_Date = sorted(
            self.set_Date,
            key=lambda x: datetime.strptime(x, '%d_%m_%Y'),
            )

        # SAMPLEID
        self.array_SampleID = [str(row[3]) for row in rows]
        self.set_SampleID = self.UniqueValuesFromListOfLists(
            self.array_SampleID)
        self.set_SampleID.sort(reverse=True)

        # USERS
        self.array_Users = [loads(row[4]) for row in rows]
        self.array_Users = [[str(y) for y in x] for x in self.array_Users]

        self.set_Users = self.UniqueValuesFromListOfLists(self.array_Users)
        self.set_Users.sort(reverse=True)

        # TAGS
        self.array_Tags = [loads(row[5]) for row in rows]
        self.array_Tags = [[str(y) for y in x] for x in self.array_Tags]

        self.set_Tags = self.UniqueValuesFromListOfLists(self.array_Tags)
        self.set_Tags.sort(reverse=True)

        # NOTES
        self.array_Notes = [str(row[6]) for row in rows]

        # Put the sets in the listboxes
        self.listBox_SampleID.Clear()
        self.listBox_Tags.Clear()
        self.listBox_Users.Clear()

        for elem in self.set_SampleID:
            self.listBox_SampleID.Insert(elem, 0)

        for elem in self.set_Users:
            self.listBox_Users.Insert(elem, 0)

        for elem in self.set_Tags:
            self.listBox_Tags.Insert(elem, 0)

        # Add "All" options manually
        self.listBox_SampleID.Insert("All", 0)
        self.listBox_SampleID.SetSelection(0)
        self.listBox_Users.Insert("All", 0)
        self.listBox_Users.SetSelection(0)
        self.listBox_Tags.Insert("All", 0)
        self.listBox_Tags.SetSelection(0)

        # Set dates
        if len(self.set_Date) > 0:
            day, month, year = self.set_Date[0].split('_')
            self.datePicker_From.SetValue(self.pydate2wxdate(
                date(int(year), int(month), int(day))))

            day, month, year = self.set_Date[-1].split('_')
            self.datePicker_To.SetValue(self.pydate2wxdate(
                date(int(year), int(month), int(day))))

        self.panel_ViewerList.AddLines(self.array_Name,
                                       self.array_Date,
                                       self.array_SampleID,
                                       self.array_Users,
                                       self.array_Tags,
                                       self.array_Notes)

        self.panel_ViewerPlot.Clear()

        # Enable/disable buttons
        self.EnableDisable()

    def UniqueValuesFromListOfLists(self, outerElements):

        uniqueValues = []

        for innerElements in outerElements:

            if type(innerElements) is list:

                for elem in innerElements:
                    if elem not in uniqueValues:
                        if elem != "":
                            uniqueValues.append(str(elem))

            else:

                if innerElements not in uniqueValues:
                    if innerElements != "":
                        uniqueValues.append(str(innerElements))

        return uniqueValues

    def pydate2wxdate(self, date_):
        assert isinstance(date_, (datetime, date))
        tt = date_.timetuple()
        dmy = (tt[2], tt[1] - 1, tt[0])
        return wx.DateTimeFromDMY(*dmy)

    def wxdate2pydate(sef, date_):
        assert isinstance(date_, wx.DateTime)
        if date_.IsValid():
            ymd = map(int, date_.FormatISODate().split('-'))
            return date(*ymd)
        else:
            return None

# SECTION_TIMER ###############################################################

    def OnGuiTimer(self, event):

        # If no serial has been initialised, skip this
        if self.comport is None:
            self.guiTimer.Start(self.timerMS, oneShot=True)
            return

        # Check the serial
        self.ReadSerial_NonBlocking(lock=True)

        # DATA
        if self.serialType == "DATA":

            # Send data to graph
            self.panelAcquisitionGraph.ReceiveTwoValues(
                time=float(self.serialValues[0]),
                y1=float(self.serialValues[1]),
                y2=float(self.serialValues[2]),
                )

            # Add flag for first value
            if self.firstValue:
                self.panelAcquisitionLog.AddFlag(
                    time=float(self.serialValues[0]),
                    sensor1_counts=float(self.serialValues[1]),
                    sensor2_counts=float(self.serialValues[2]),
                    description="Initial",
                    baseline=True,
                    colour="Red",
                    )
                self.panelAcquisitionGraph.AddFlag(
                    flagTime=float(self.serialValues[0]),
                    baseline=True,
                    colour="Red",
                    )
                self.firstValue = False

            # Timer is based on data time
            if self.timerRunning:

                dataTime = float(self.serialValues[0])
                self.timerTime = self.timerEnd - dataTime

                if self.timerTime < 0:
                    self.timerTime = 0.0

                    fileName = join(
                        dirname(__file__),
                        'assets/sounds/%s.wav' % self.timerSound,
                        )

                    self.sound = wx.Sound(fileName)
                    if self.sound.IsOk():
                        self.sound.Play(wx.SOUND_ASYNC)

                    # Flag if the flag is checked
                    if self.check_AutoFlag.IsChecked():
                        graph = self.panelAcquisitionGraph
                        graph.crosshair_time = self.timerEnd
                        graph.MoveCrosshair()
                        self.OnFlag(graph.crosshair_time)

                    self.btn_timerStartStop.SetLabel('Start')
                    self.timerRunning = False

                self.textCtrl_timer.SetLabel(str(self.timerTime))

            # Save to database if required
            if float(self.serialValues[0]) % self.autosaveInterval == 0:
                self.UpdateDatabase()

        # Restart the timer and skip the event
        self.guiTimer.Start(self.timerMS, oneShot=True)
        event.Skip()
        return

    def UpdateDatabase(self):
        sql = ''' UPDATE CAUSEWAY_DATA
                    SET Array_Time = ? ,
                        Array_Y1_Counts = ?,
                        Array_Y2_Counts = ?,
                        Array_Y1_Percent = ?,
                        Array_Y2_Percent = ?,
                        Array_Baseline_Times = ?,
                        Dict_Flags = ?
                    WHERE name = ?'''
        values = (
            dumps(self.panelAcquisitionGraph.array_time_1),
            dumps(self.panelAcquisitionGraph.array_y1_counts_1),
            dumps(self.panelAcquisitionGraph.array_y2_counts_1),
            dumps(self.panelAcquisitionGraph.array_y1_1),
            dumps(self.panelAcquisitionGraph.array_y2_1),
            dumps(self.panelAcquisitionGraph.array_baseline_times),
            dumps(self.panelAcquisitionLog.dict_flags),
            self.fileName,
            )
        try:
            self.cur.execute(sql, values)
            self.db.commit()
        except Error as e:
            print('sqlite.Error: {}'.format(e.message))
            return False
        else:
            return True

    def CheckSerial(self, lock=False):
        """
        Check if there is anything in the serial
        """

        # Acquire the lock in case the serial is in use
        if lock:
            self.serialLock.acquire()

        if self.ser.in_waiting() > 0:
            if lock:
                self.serialLock.release()
            return True
        else:
            if lock:
                self.serialLock.release()
            return False

    def ClearSerial(self, lock=False):
        """
        Check if there is anything in the serial
        """

        # Acquire the lock in case the serial is in use
        if lock:
            self.serialLock.acquire()

        while self.ser.in_waiting > 0:
            self.ser.readline()

        # Release lock
        if lock:
            self.serialLock.release()

    def WriteSerial(self, command, lock=False):
        """
        Read the serial port immediately and if nothing is there, continue
        """

        # Acquire the lock in case the serial is in use
        if lock:
            self.serialLock.acquire()

        self.ser.write(command)

        # Release lock
        if lock:
            self.serialLock.release()

    def _parse(self, input_):
        '''Parses a line from the Arduino'''
        if input_:
            fields = input_.split('|')
            type_ = fields[0]
            description = fields[1]
            values = fields[2:]
            values = [i.rstrip() for i in values]
            return type_, description, values
        else:
            return "", "", []

    def ReadSerial_NonBlocking(self, lock=False):
        """
        Read the serial port immediately and if nothing is there, continue

        :lock: this function call requires a lock
        """

        # Non-blocking so if the lock is not available, move on
        if lock and not self.serialLock.acquire(False):
            self.serialType, self.serialDescription, \
                self.serialValues = self._parse('')
        else:
            if (self.ser.in_waiting > 0):
                self.serialType, self.serialDescription, \
                    self.serialValues = self._parse(
                        self.ser.readline())
            else:
                self.serialType, self.serialDescription, \
                    self.serialValues = self._parse('')
            self.serialLock.release()

    def ReadSerial_Blocking(self, lock=False):
        """
        Wait for something to come through the serial port
        Stopped by timeout

        :lock: this function call requires a lock
        """
        # Acquire the lock in case the serial is in use
        if lock:
            self.serialLock.acquire()  # Waits indefinitley for lock

        # Blocking so may time out
        # No print as serial exception will happen frequently
        try:
            serialInput = self.ser.readline()
        except SerialException:  # timed out
            self.serialType, self.serialDescription, \
                self.serialValues = self._parse('')
            return

        self.serialType, self.serialDescription, \
            self.serialValues = self._parse(serialInput)

        if lock:
            self.serialLock.release()
        return


class GUI_test(wx.App):
    '''Testing the GUI class'''
    def __init__(self, auto_close=True):
        '''Open up the GUI, and if required close it down'''
        super(GUI_test, self).__init__()
        GUI(None).Show()
        if auto_close:
            self.TopWindow.Destroy()


if __name__ == "__main__":
    GUI_test(False).MainLoop()
