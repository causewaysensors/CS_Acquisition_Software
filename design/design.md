# Acquisition Software - Design document

<img src="AcquisitionDesign.png" width="500">

* Future command-line interface for "Logical Device" module
* Database(s) can be accessed by other programs

## main.py

Entry point for the acquisition software.
* Loads the static configuration for the software including:
 * Version
 * Initial values
 * Auto-save interval
 * Database tags, users and samples
* Initialises the database so the viewer can be used without an Arduino/sensor connected
* Sets up the display, frames etc.

## GUI

<img src="AcquisitionLayout.png" width="500">

User interface for the acquisition software and central node for data handling, arrays etc.

User interface is designed in wxFormBuilder v3.6.1 and sub-classed to overwrite methods and add functionality.

### display_methods

Methods and classes to break out functionality of various elements of the display. Will be implemented after the refactor or as a last step.

### lib/db/statements

Module containing SQL statements to create, insert, select and delete from the database

### lib/db/interface

Module to handle low-level read and write operations to the database using the statements defined above.

### lib/db/utils

Module to handle conversion of database data to csv format and vice versa. Currently will only handle export from database but may include import in future so data can be shared between laptops.

### lib/settings

Module to handle low-level read and write operations to configuration file. Also handles version of acquisition software.

### lib/calibration

Module to handle low-level read and write operations on a calibration file. Currently we only write to the calibration file but we may possibly wish to read in data in future.

## Logical Device Module (Control)

This module is a wrapper around the serial methods, which will handle the logical connections between the Arduino and the GUI. Created when the Arduino is connected in the GUI.

* **\__init__()**  
  Initialise and set up the serial handle
* **configure()**
  Send configuration data to the Arduino to set up the sensor e.g. integration time, gain, RPS. Currently default values are set on Arduino initialisation but could set them here.

  ```
  setMinCounts|<value>
  setMaxCounts|<value>
  setRPS|<value>
  setOverhead|<value>
  setIntegrationMS_1|<value>
  setIntegrationMS_2|<value>
  setGain_1|<value>
  setGain_2|<value>
  printSettings
  ```
* **calibrate()**   
  Calibrate the sensors depending on user specified:
  1. Min counts (1-65535)  
  1. Max counts (1-65535)
  1. Desired RPS (1, 2, 4, 8)

  ```
  setMinCounts|<value>
  setMaxCounts|<value>
  setRPS|<value>
  calibrate
  ```
* **start_capture(reset=False)**  
  Start the Arduino acquiring data. "Reset" argument is whether time is reset to zero before acquisition i.e. new data or append

  ```
  newData or
  appendData
  ```
* **_reset_timer()**  
  Reset the data time to zero i.e. new data. Currently handled by ```newData``` command but may be changed in future Arduino iterations.
* **stop_capture()**    
  Write to the Arduino to stop acquiring data

  ```
  stopData
  ```
* **read()**    
  Read from the Arduino. Blocking until timeout as will be called from "Poll Serial" thread.
* **clamp()**    
  Send a command to the Arduino to move the clamp. Serial command is of the form

  ```
  clamp|In|Low|255|5000
  ```
* **stirrer()**    
  For possible future iterations when a stirrer or agitator may be added to the biosensor

## Device Interface Module

This module will contain the low level functions to interface
with pySerial. An instance will be created by the logical device modules when the Arduino is connected in the GUI.

* **\__init__()**  
  Will set up the serial handle and lock for serial  
* **self.\_serial_handle**  
  Handle of the serial connection
* **self.\_lock**  
  Threading lock to stop simultaneous serial connections. Use context manager to handle lock.
* **\_write()**  
  Private method to simply write to Arduino. This will also
  handle locking.
* **\_readline()**  
  Private method to simply read line from Arduino. This will
  also handle locking.
* **_parse()**  
  Parse a readline() from the Arduino into type, description
  and values
* **clear()**  
  Clear all data from the serial port
* **write()**  
  Write a command to the Arduino
* **write_many()**  
  May need to write multiple commands to the Arduino. e.g. when
  we calibrate the sensor we set max/min counts, RPS and
  then call the calibrate function.
* **readline_block()**  
  Wait for a response from the Arduino and then read a line.

## Poll Serial - Thread

This module polls the Arduino for serial responses and raises an event in the display. If the serial response is "standby", the module decreases the polling frequency.

Will need to set appropriate timeout on the blocking serial read so errors caused by disconnecting the Arduino can be handled.

There are only limited valid responses, so may set up error checking if other values are received.

* **\__init__(self, received_evt, disconnect_evt)**   
  Read in the handles for the display events that are raised when a serial response is received or when the serial has been disconnected.
* **received_evt**  
  Handle to event in the display which is raised when something other that "standby" is received from the Arduino.
* **disconnect_evt**  
  Handle to event in display that is raised when the Arduino is disconnected.
* **_run_gen()**  
  Generator which polls the Arduino for serial responses. If no data is received inside timeout, Arduino is disconnected so raise "disconnect_evt".

  ```
  while True
    try:
      data_ = self.dev.read()
    except:
      # Handle timeout (disconnected Arduino)
      self.disconnect_evt()
      break
    yield data_
  ```
* **run()**  
  For each item received, parse and check if the type is "standby". If not, raise the event in GUI with the serial response as a value.

  ```
  for data_ in self._run_gen():
    type, description, values = self.dev._parse(data_)
    if type != "standby":    
      self.received_evt(type, description, values)
  ```
