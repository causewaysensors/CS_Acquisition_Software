from GUI import GUI_test

from dialogCalibrateChip import dialogCalibrateChip_test

from dialogCalibrateChipResults import dialogCalibrateChipResults_test

from dialogCalibrateSensorsResults import dialogCalibrateSensorsResults_test

from dialogClampCustom import dialogClampCustom_test

from dialogClampPrompt import dialogClampPrompt_test

from dialogFileSetup import dialogFileSetup_test

from dialogFlag import dialogFlag_test

from dialogLoading import dialogLoading_test

from dialogOptions import dialogOptions_test

from dialogPreferences import dialogPreferences_test

from dialogWelcome import dialogWelcome_test

from doubleslider import doubleslider_test

from panel_AcquisitionGraph import panel_AcquisitionGraph_test

from panel_AcquisitionLog import panel_AcquisitionLog_test

from panel_CalibrateGraph import panel_CalibrateGraph_test

from panel_CalibrateLog import panel_CalibrateLog_test

from panel_ViewerList import panel_ViewerList_test

from panel_ViewerPlot import panel_ViewerPlot_test

if __name__ == '__main__':
    GUI_test().MainLoop()
    # main - main GUI script
    dialogCalibrateChip_test().MainLoop()
    dialogCalibrateChipResults_test().MainLoop()
    dialogCalibrateSensorsResults_test().MainLoop()
    dialogClampCustom_test().MainLoop()
    dialogClampPrompt_test().MainLoop()
    dialogFileSetup_test().MainLoop()
    dialogFlag_test().MainLoop()
    dialogLoading_test().MainLoop()
    dialogOptions_test().MainLoop()
    dialogPreferences_test().MainLoop()
    dialogWelcome_test().MainLoop()
    doubleslider_test().MainLoop()
    panel_AcquisitionGraph_test().MainLoop()
    panel_AcquisitionLog_test().MainLoop()
    panel_CalibrateGraph_test().MainLoop()
    panel_CalibrateLog_test().MainLoop()
    panel_ViewerList_test().MainLoop()
    panel_ViewerPlot_test().MainLoop()
    # setup - metadata configuration
    # test - test script
    # test_gui - test script
