"""Graph for plotting calibration data."""
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigCanvas
from matplotlib.figure import Figure

from scipy.stats import linregress

import wx


class panel_CalibrateGraph(wx.Panel):
    """Panel for plotting data after chip calibration

    This panel contains the canvas and figure for plotting the graph and linear
    regression as well as the methods for resizing.
    """
    def __init__(
            self,
            parent,
            id=wx.ID_ANY,
            position=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=0,
            name="",
            ):

        wx.Panel.__init__(self, parent, id, position, size, style, name)

        # Set up MPL stuff
        self.figure = Figure(None)

        # Create canvas and graphpan
        self.canvas = FigCanvas(self, wx.ID_ANY, self.figure)
        self.canvas.SetMinSize([-1, 200])

        self.axes = self.figure.add_subplot(111)
        self.axes.set_xlabel('Sensor 1 Response')
        self.axes.set_ylabel('Sensor 2 Response')
        self.axes.grid(True, color='Gray')

        # Use sizer to arrange things
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.sizer.Add(self.canvas, 1, wx.EXPAND)

        self.SetSizerAndFit(self.sizer)

        # Bind resize event
        self.Bind(wx.EVT_SIZE, self.OnSize)

    def OnSize(self, event):
        """Adjust layout of mpl figure on resize."""
        try:
            self.figure.tight_layout()
        except ValueError:
            pass

        # Let the event propagate
        event.Skip()

    def PlotData(self, data_sensor1, data_sensor2, fit_slope, fit_yintercept):
        """Plot data and linear fit as calculated in calibration."""

        # Calculate the linear fit
        linearFitData = [
            data * fit_slope + fit_yintercept
            for data in data_sensor1]

        # Plot the data points
        self.axes.plot(data_sensor1, data_sensor2, 'x', color='Black')
        self.axes.plot(data_sensor1, linearFitData, linewidth=1, color='Red')

        self.figure.tight_layout()


class panel_CalibrateGraph_test(wx.App):
    """Class for testing panel_CalibrateGraph."""
    def __init__(self, auto_close=True):
        """Place panel in frame and test opening, plotting data and closing."""
        super(panel_CalibrateGraph_test, self).__init__()

        # Setup frame and sizers
        frame = wx.Frame(None)
        sizer = wx.BoxSizer(wx.VERTICAL)
        graph_pan = panel_CalibrateGraph(frame, wx.ID_ANY)
        sizer.Add(graph_pan, 1, wx.EXPAND | wx.ALL, 20)
        frame.Show()
        frame.SetSizerAndFit(sizer)
        frame.Maximize()
        frame.Layout()  # Display mpl figure correctly

        # Generate sample data, calculate regression and plot
        data_sensor1 = [10, 20, 30, 40, 50]
        data_sensor2 = [11, 22, 33, 44, 55]
        slope, yintercept, r2, pvalue, stderr = linregress(
            data_sensor1, data_sensor2)
        graph_pan.PlotData(
            data_sensor1, data_sensor2, slope, yintercept)

        if auto_close:
            self.TopWindow.Destroy()


if __name__ == '__main__':
    panel_CalibrateGraph_test(False).MainLoop()
