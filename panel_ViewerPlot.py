from time import sleep

from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigCanvas
from matplotlib.figure import Figure

import wx


class panel_ViewerPlot(wx.Panel):

    def __init__(
            self,
            parent,
            id=wx.ID_ANY,
            position=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=0,
            name="",
            ):

        wx.Panel.__init__(self, parent, id, position, size, style, name)

        # Access to parent
        self.parent = parent

        # Set background colour
        self.SetBackgroundColour("White")

        # Set min size
        self.SetMinSize([-1, -1])

        # Data arrays
        self.array_time = []
        self.array_y1_counts = []
        self.array_y2_counts = []
        self.array_y1_percent = []
        self.array_y2_percent = []
        self.baseline_times = []
        self.dict_flags = {}

        # Crosshair
        self.crosshair_time = None
        self.crosshair_vert = None

        # Set background colour
        self.SetBackgroundColour("White")
        # Set min size
        self.SetMinSize([-1, -1])

        # Set up MPL stuff
        self.figure = Figure(None)
        self.axes = self.figure.add_subplot(111)

        # Create canvas and graphpan
        self.canvas = FigCanvas(self, wx.ID_ANY, self.figure)

        # Nav bar
        self.navBar = NavigationToolbar2Wx(self.canvas)

        self.navBar.DeleteToolByPos(8)
        self.navBar.DeleteToolByPos(6)

        # Choice of counts or percent
        self.choice_displayType = wx.Choice(
            self, wx.ID_ANY, choices=['Counts', 'Percentage'])
        self.choice_displayType.SetSelection(1)

        # Status text
        self.crosshair_text = wx.StaticText(
            self,
            wx.ID_ANY,
            label="Time = ? s     Y1 = ? % (? counts)     Y2 = ? % (? counts)",
            )

        # Use sizer to arrange things
        self.bsizer = wx.BoxSizer(wx.VERTICAL)

        self.bsizer.Add(self.canvas, 1, wx.ALL | wx.EXPAND, 5)

        self.flexsizer1 = wx.FlexGridSizer(1, 3, 0, 0)
        self.flexsizer1.AddGrowableRow(0)
        self.flexsizer1.AddGrowableCol(0)
        self.flexsizer1.AddGrowableCol(2)

        self.flexsizer1.Add(
            self.choice_displayType,
            0,
            wx.ALIGN_RIGHT | wx.RIGHT | wx.ALIGN_CENTER_VERTICAL,
            20,
            )
        self.flexsizer1.Add(
            self.navBar,
            0,
            wx.LEFT | wx.RIGHT | wx.ALIGN_CENTER_VERTICAL,
            20,
            )
        self.flexsizer1.Add(
            self.crosshair_text,
            0,
            wx.LEFT | wx.ALIGN_CENTER_VERTICAL,
            20,
            )

        self.bsizer.Add(self.flexsizer1, 0, wx.EXPAND | wx.ALL, 10)

        self.SetSizerAndFit(self.bsizer)

        self.Bind(wx.EVT_CHOICE, self.OnChoice, self.choice_displayType)

        # Detect clicks, movement and releases
        self.canvas.mpl_connect('button_press_event', self.OnCanvasButtonPress)
        self.canvas.mpl_connect('motion_notify_event', self.OnCanvasMouseMove)
        self.canvas.mpl_connect(
            'button_release_event', self.OnCanvasButtonRelease)

        self.rightpress = None

    def Clear(self):

        self.axes.cla()

        # Data arrays
        self.array_time = []
        self.array_y1_counts = []
        self.array_y2_counts = []
        self.array_y1_percent = []
        self.array_y2_percent = []
        self.baseline_times = []
        self.dict_flags = {}

        # Crosshair
        self.crosshair_time = None
        self.crosshair_vert = None

        self.canvas.draw()

    def OnChoice(self, event):

        self._plotData()

    def OnCanvasButtonPress(self, event):
        """
        Check for both left and right clicks. Only deal with right atm
        """

        if event.inaxes:

            if event.button == 3:
                x0 = event.xdata
                y0 = event.ydata
                self.rightpress = x0, y0

    def OnCanvasMouseMove(self, event):
        # Right mouse held down inside axes
        if self.rightpress is not None and self.crosshair_time is not None:

            if event.inaxes:
                # Hide the previous of the
                self.crosshairvert.set_visible(False)

                # capture mouse motion
                x = event.xdata

                # Find the time that is closest to the right click
                self.crosshair_time = min(
                    self.array_time, key=lambda y: abs(y - x))
                self.crosshairvert = self.axes.axvline(
                    self.crosshair_time, linewidth=1, color="Green")

                # Find y values
                index = self.array_time.index(self.crosshair_time)

                self.crosshair_text.SetLabel(
                    (
                        "Time = %0.2f s     Y1 = %0.3f %% (%0.1f counts)"
                        "Y2 = %0.3f %% (%0.1f counts)"
                    ) % (
                       self.crosshair_time,
                       self.array_y1_percent[index],
                       self.array_y1_counts[index],
                       self.array_y2_percent[index],
                       self.array_y2_counts[index]),
                    )

                self.canvas.draw()

    def OnCanvasButtonRelease(self, event):

        # Reset mouse clicks
        self.leftpress = None
        self.rightpress = None

    def SetData(self, array_time, array_y1_percent, array_y2_percent,
                array_y1_counts, array_y2_counts, flag_times, baseline_times):

        # Set all the arrays
        self.array_time = array_time
        self.array_y1_percent = array_y1_percent
        self.array_y2_percent = array_y2_percent
        self.array_y1_counts = array_y1_counts
        self.array_y2_counts = array_y2_counts
        self.flag_times = flag_times
        self.baseline_times = baseline_times

        self._plotData()

    def _plotData(self):

        # Clear the old data
        self.axes.cla()

        # Label the x-axis
        self.axes.set_xlabel("Times (s)")

        # Display percentages or counts
        string = self.choice_displayType.GetString(
            self.choice_displayType.GetCurrentSelection())
        if string == "Percentage":
            self.axes.set_ylabel("%")
            self.axes.plot(
                self.array_time,
                self.array_y1_percent,
                'r',
                self.array_time,
                self.array_y2_percent,
                'b',
                )
        else:
            self.axes.set_ylabel("Counts")
            self.axes.plot(
                self.array_time,
                self.array_y1_counts,
                'r',
                self.array_time,
                self.array_y2_counts,
                'b',
                )

        # Plot flags
        for time in self.dict_flags.keys():
            time = float(time)
            if time not in self.baseline_times:
                self.axes.axvline(time, linewidth=1, color="Black")

        # Plot baselines
        for time in self.baseline_times:
            self.axes.axvline(time, linewidth=1, color="Red")

        # Add Crosshair
        if len(self.array_time) > 0:
            self.crosshair_time = self.array_time[-1]
            self.crosshairvert = self.axes.axvline(
                self.crosshair_time, linewidth=1, color="Green")

            self.crosshair_text.SetLabel(
                (
                    "Time = %0.2f s     Y1 = %0.3f %% (%0.1f counts)     "
                    "Y2 = %0.3f %% (%0.1f counts)"
                ) % (
                    self.array_time[-1],
                    self.array_y1_percent[-1],
                    self.array_y1_counts[-1],
                    self.array_y2_percent[-1],
                    self.array_y2_counts[-1],
                    )
                )

        # Draw everything
        self.canvas.draw()

        # Layout and draw
        try:
            self.figure.tight_layout()
        except ValueError:
            pass


class panel_ViewerPlot_test(wx.App):
    '''Class to test panel_ViewerPlot.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close after tests run'''
        super(panel_ViewerPlot_test, self).__init__()

        # Setup frame and sizers
        frame = wx.Frame(None)
        sizer = wx.BoxSizer(wx.VERTICAL)
        plot_pan = panel_ViewerPlot(frame, wx.ID_ANY)
        sizer.Add(plot_pan, 1, wx.EXPAND | wx.ALL, 20)
        frame.Show()
        frame.SetSizerAndFit(sizer)
        frame.Maximize()
        frame.Layout()  # Required to display figure correctly

        # Sample data
        sample_arguments = (
            [1, 2, 3, 4],  # Time
            [1, 2, 0, 1],  # Percent sensor 1
            [-1, -2, 0, -1],  # Percent sensor 2
            [101, 102, 103, 104],  # Counts sensor 1
            [99, 98, 97, 96],  # Counts sensor 1
            [1, 3],  # Flag times
            [3],  # Baseline times
            )

        # List of functions to run [function, arguments]
        list_tests = [
            [plot_pan.SetData, sample_arguments],  # Plot data
            [plot_pan.Clear, []],  # Clear graph (e.g. reload database)
            [plot_pan.SetData, sample_arguments],
            ]
        for function, arguments in list_tests:
            sleep(0 if auto_close else 1)
            function(*arguments)

        # Destroy top window rather than using close
        if auto_close:
            self.TopWindow.Destroy()


if __name__ == '__main__':
    panel_ViewerPlot_test(False).MainLoop()
