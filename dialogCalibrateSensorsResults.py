from wx import App

from wxfb.layout import wxFB_dialog_calibrateSensorsResults


class dialogCalibrateSensorsResults(wxFB_dialog_calibrateSensorsResults):
    def __init__(self, parent, results, status, ms_1, ms_2, gain):

        # Initialize parent class
        wxFB_dialog_calibrateSensorsResults.__init__(self, parent)

        self.parent = parent

        # Set Strings
        self.text_Status.SetLabelText(status)
        self.text_Integration1.SetLabelText("{0}".format(ms_1))
        self.text_Integration2.SetLabelText("{0}".format(ms_2))
        self.text_Gain.SetLabelText(gain)

        # Set the values from parent
        index = 0
        for i in range(0, 4):

            for j in range(0, 3):

                self.grid_CalibrateSensors.SetCellValue(i, j, results[index])
                index = index + 1


class dialogCalibrateSensorsResults_test(App):
    '''Class to test dialogCalibrateSensorsResults.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close automatically'''
        super(dialogCalibrateSensorsResults_test, self).__init__()

        # Create dialog with sample
        dialog = dialogCalibrateSensorsResults(
            parent=None,
            results=[str(i + 1) for i in range(12)],
            status="Success",
            ms_1="125ms",
            ms_2="125ms",
            gain="x8",
            )

        # Depending on auto_close, dialog needs opened differently
        if auto_close:
            dialog.Show()
        else:
            dialog.ShowModal()
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogCalibrateSensorsResults_test(False).MainLoop()
