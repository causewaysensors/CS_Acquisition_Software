import wx

from wxfb.layout import wxFB_dialog_calibrateChip


class dialogCalibrateChip(wxFB_dialog_calibrateChip):
    def __init__(self, parent, dict_flags):

        # Initialize parent class
        wxFB_dialog_calibrateChip.__init__(self, parent)

        self.listpan.Draw(dict_flags)


class dialogCalibrateChip_test(wx.App):
    '''Class to test dialogCalibrateChip.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close automatically'''
        super(dialogCalibrateChip_test, self).__init__()

        # Sample data
        dict_flags = {
            0: {'sensor1_counts': 1.5,
                'sensor2_counts': 2.5,
                'baseline': True,
                'description': "Initial baseline",
                },
            2: {'sensor1_counts': 2.5,
                'sensor2_counts': 3.5,
                'baseline': False,
                'description': "Example flag",
                },
            5: {'sensor1_counts': 3.5,
                'sensor2_counts': 4.5,
                'baseline': True,
                'description': "Example baseline",
                },
               }

        # Depending on auto_close, dialog needs opened differently
        dialog = dialogCalibrateChip(parent=None, dict_flags=dict_flags)
        if auto_close:
            dialog.Show()
        else:
            dialog.ShowModal()
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogCalibrateChip_test(False).MainLoop()
