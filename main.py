from os import makedirs, remove
from os.path import exists, join
from sqlite3 import Error, connect
from threading import Thread
from time import sleep

# Python 2 uses a backported version from PyPI
from configparser import ConfigParser  # noqa: I100 I201 I202

from wx import App, WakeUpMainThread

from GUI import GUI

from dialogWelcome import dialogWelcome

from lib.db.models import CREATE_STATEMENT
from lib.settings import DEFAULT_FOLDERNAME, VERSION


class MyApp(App):
    """
    Simple App Class
    Can add password protection and updates here
    """

    def OnInit(self, foldername=DEFAULT_FOLDERNAME):
        """
        Link the frame and app
        """

        # Create the mainWindow.
        # Contain design, guiTimer and events
        self.GUI = GUI(None)

        # Bring up welcome dialog
        self.dialog = dialogWelcome(None, "Checking for updates")
        self.dialog.Show()

        # Database must be connected in same thread but close in threaded
        # dialog
        self.databaseConnected = False

        # Create the CS folder for results and config if it doesn't exist
        # Put in user folder so there can be one for each user if required
        if not exists(foldername):
            makedirs(foldername)

        # Create results database (if required)
        try:  # Create/read the database
            self.GUI.db = connect(join(foldername, 'results.db'))
            self.GUI.cur = self.GUI.db.cursor()
            self.GUI.cur.execute(CREATE_STATEMENT)
            self.GUI.db.commit()
            self.GUI.cur.execute("SELECT 1 FROM CAUSEWAY_DATA LIMIT 1;")
            self.databaseConnected = True
        except Error as e:
            print('sqlite.Error: {}'.format(e.message))

        self.GUI.configParser = ConfigParser()

        # Check if a new config file is required
        self.GUI.configFilename = join(foldername, 'config.ini')
        createConfig = False
        if exists(self.GUI.configFilename):
            self.GUI.configParser.read(self.GUI.configFilename)
            version = str(self.GUI.configParser['About']['version'])
            if version != VERSION:
                createConfig = True  # it exists but is the wrong version
                remove(self.GUI.configFilename)
        else:
            createConfig = True

        # Create the new config file if required
        if createConfig:
            # Create the minimum config data structure
            self.GUI.configParser['About'] = {'version': VERSION}
            self.GUI.configParser['Data'] = {'autosaveInterval': 60}
            self.GUI.configParser['GUI'] = {'timerSound': 'Exit Cue'}
            self.GUI.configParser['Clamp'] = {'range': 'Low',
                                              'speed': 255,
                                              'timeIn': 5000,
                                              'timeOut': 2000}

            # Write the data structure to disk
            with open(self.GUI.configFilename, 'w') as configfile:
                self.GUI.configParser.write(configfile)

        # Read the config file
        self.GUI.configParser.read(self.GUI.configFilename)
        stored = self.GUI.configParser
        self.GUI.autosaveInterval = int(stored['Data']['autosaveInterval'])
        self.GUI.timerSound = str(stored['GUI']['timerSound'])
        self.GUI.clampRange = str(stored['Clamp']['range'])
        self.GUI.clampSpeed = int(stored['Clamp']['speed'])
        self.GUI.clampTimeIn = int(stored['Clamp']['timeIn'])
        self.GUI.clampTimeOut = int(stored['Clamp']['timeOut'])

        # Start a thread behind the welcome dialog that will check for updates
        # etc.
        self.dialogThread = Thread(target=self.Thread_CheckUpdates, args=(1,))
        self.dialogThread.start()

        return True

    def Thread_CheckUpdates(self, additionalwait=0):

        # Simulate checking everything
        # This will handle updating, passwords etc.
        sleep(additionalwait)

        self.dialog.ChangeMessage("Software up to date")

        sleep(additionalwait)

        self.dialog.ChangeMessage("Loading database")

        sleep(additionalwait)

        # Fake load database
        if self.databaseConnected:
            self.dialog.ChangeMessage("Database connected")
        else:
            self.dialog.ChangeMessage(
                "Error connecting to database\nClosing Program")

            sleep(additionalwait)

            self.dialog.Destroy()
            self.ExitMainLoop()
            WakeUpMainThread()

            return 0

        sleep(additionalwait)

        # Destroy the loading dialog
        self.dialog.Destroy()

        # Show the mainwindow
        self.GUI.Show()
        self.GUI.Maximize()
        self.GUI.Raise()

        return 0


if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()
