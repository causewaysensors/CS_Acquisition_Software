"""Log for choosing which flags will be used to calibrate chip."""
import wx
import wx.lib.newevent

from panel_AcquisitionLog import AutoWidthListCtrl


class panel_CalibrateLog(wx.Panel):
    """Panel for listing flags in current data to be used for calibration

    Simply display flag values and allow them to be selected. Do not allow
    logs to be added/deleted/edited as in panel_AcquisitionLog.
    """
    def __init__(
            self,
            parent,
            id=wx.ID_ANY,
            position=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=0,
            name="",
            ):

        wx.Panel.__init__(self, parent, id, position, size, style, name)

        self.parent = parent

        # Variables that control the log
        self.logrow = 0

        # Sizers and objects
        self.vbox = wx.BoxSizer(wx.VERTICAL)

        self.log = AutoWidthListCtrl(self)

        self.log.InsertColumn(0, "Time", width=50)
        self.log.InsertColumn(1, "Sensor", width=100)
        self.log.InsertColumn(2, "Response", width=100)
        self.log.InsertColumn(3, "Baseline", width=100)
        self.log.InsertColumn(4, "Description")

        self.vbox.Add(self.log, 1, wx.EXPAND)
        self.SetSizer(self.vbox)

    def Draw(self, dict_flags):
        """Populate list with flag values."""
        # Add all the flags in order
        for time in sorted(dict_flags.keys()):
            now = dict_flags[time]
            counts1 = float(now['sensor1_counts'])
            counts2 = float(now['sensor2_counts'])

            # If it is a baseline (otherwise use previous baseline)
            if now['baseline']:
                baseline1_counts = counts1
                baseline2_counts = counts2

            # Calculate the percentages
            def percentage(a, b):
                return (a - b) / b * 100.0

            percentage1 = percentage(counts1, baseline1_counts)
            percentage2 = percentage(counts2, baseline2_counts)

            # Update the log
            def update(value, sensor='1'):
                index = self.log.InsertStringItem(self.logrow, str(time))
                self.log.SetStringItem(index, 1, sensor)
                self.log.SetStringItem(index, 2, "%0.3f%%" % value)
                self.log.SetStringItem(
                    index, 3, "True" if now['baseline'] else '')
                self.log.SetStringItem(index, 4, now['description'])
                self.logrow += 1

            update(percentage1)
            update(percentage2, sensor='2')

            now['sensor1_percent'] = percentage1
            now['sensor2_percent'] = percentage2
            now['subtracted_percent'] = percentage2 - percentage1

        # Need to keep reference to dict_flags
        self.dict_flags = dict_flags

        self.Refresh()


class panel_CalibrateLog_test(wx.App):
    """Class for testing panel_CalibrateLog."""
    def __init__(self, auto_close=True):
        """Place panel in frame and test opening, listing data and closing."""
        super(panel_CalibrateLog_test, self).__init__()

        # Setup frame and sizers
        frame = wx.Frame(None)
        sizer = wx.BoxSizer(wx.VERTICAL)
        log_pan = panel_CalibrateLog(frame)
        sizer.Add(log_pan, 1, wx.EXPAND | wx.ALL, 20)
        frame.Show()
        frame.SetSizerAndFit(sizer)
        frame.Maximize()
        frame.Layout()

        # Create sample dictionary and test displaying it
        dict_flags = {
            0: {'sensor1_counts': 1.5,
                'sensor2_counts': 2.5,
                'baseline': True,
                'description': "Initial baseline",
                },
            2: {'sensor1_counts': 2.5,
                'sensor2_counts': 3.5,
                'baseline': False,
                'description': "Example flag",
                },
            5: {'sensor1_counts': 3.5,
                'sensor2_counts': 4.5,
                'baseline': True,
                'description': "Example baseline",
                },
               }
        log_pan.Draw(dict_flags)

        if auto_close:
            self.TopWindow.Destroy()


if __name__ == '__main__':
    panel_CalibrateLog_test(False).MainLoop()
