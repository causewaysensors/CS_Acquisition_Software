from os.path import dirname, join

import wx

from wxfb.layout import wxFB_dialog_clampCustom


class dialogClampCustom(wxFB_dialog_clampCustom):
    def __init__(self, parent):

        # Initialize parent class
        wxFB_dialog_clampCustom.__init__(self, parent)

        # Use the parent methods
        self.parent = parent

    def OnChoice_Direction(self, event):

        if self.choice_Direction.GetStringSelection() == "Cycle":
            self.spinCtrl_NumberCycles.Enable(True)
        else:
            self.spinCtrl_NumberCycles.Enable(False)

        event.Skip()

    def OnButton_MoveClamp(self, event):

        # Disable everything for the duration
        self.Enable(False)

        # Get values from dialog
        clampDirection = str(self.choice_Direction.GetStringSelection())
        clampCycles = int(self.spinCtrl_NumberCycles.GetValue())
        clampSpeed = int(self.spinCtrl_Speed.GetValue())
        clampRange = str(self.choice_Range.GetStringSelection())
        clampTimeIn = int(self.spinCtrl_TimeIn.GetValue())
        clampTimeOut = int(self.spinCtrl_TimeOut.GetValue())

        # Move the clamp
        if clampDirection == "Cycle":

            for i in range(1, clampCycles+1):

                self.parent.bi = wx.BusyInfo(
                    "Cycle %d of %d - In" % (i, clampCycles),
                    self.parent,
                    )
                self.parent.WriteSerial('clamp|%s|%s|%d|%d\n' % (
                    "In", clampRange, clampSpeed, clampTimeIn))

                # Wait for the clamping to stop
                while True:
                    self.parent.ReadSerial_Blocking(lock=True)

                    # Wait for the calibration results
                    if (self.parent.serialType == "INFO" and
                       self.parent.serialDescription == "clamp" and
                       self.parent.serialValues[0] == "finished"):
                        break

                self.parent.bi = wx.BusyInfo(
                    "Cycle %d of %d - In" % (i, clampCycles),
                    self.parent,
                    )
                self.parent.WriteSerial('clamp|%s|%s|%d|%d\n' % (
                    "In", clampRange, clampSpeed, clampTimeIn))

                # Wait for the clamping to stop
                while True:
                    self.parent.ReadSerial_Blocking(lock=True)

                    # Wait for the calibration results
                    if (self.parent.serialType == "INFO" and
                       self.parent.serialDescription == "clamp" and
                       self.parent.serialValues[0] == "finished"):
                        break

        else:

            self.parent.bi = wx.BusyInfo("Moving clamp", self.parent)

            if clampDirection == "In":
                self.parent.WriteSerial('clamp|%s|%s|%d|%d\n' % (
                    "In", clampRange, clampSpeed, clampTimeIn))
            else:
                self.parent.WriteSerial('clamp|%s|%s|%d|%d\n' % (
                    "Out", clampRange, clampSpeed, clampTimeOut))

            # Wait for the clamping to stop
            while True:
                self.parent.ReadSerial_Blocking(lock=True)

                # Wait for the calibration results
                if (self.parent.serialType == "INFO" and
                   self.parent.serialDescription == "clamp" and
                   self.parent.serialValues[0] == "finished"):
                    break

        del self.parent.bi

        # Re-enable the dialog
        self.Enable(True)

        event.Skip()

    def OnButton_SetAndExit(self, event):

        fileName = join(dirname(__file__), 'config/config.ini')

        clampSpeed = int(self.spinCtrl_Speed.GetValue())
        clampRange = str(self.choice_Range.GetStringSelection())
        clampTimeIn = int(self.spinCtrl_TimeIn.GetValue())
        clampTimeOut = int(self.spinCtrl_TimeOut.GetValue())

        # Write this to config file
        self.parent.config['Clamp'] = {'range': clampRange,
                                       'speed': clampSpeed,
                                       'timeIn': clampTimeIn,
                                       'timeOut': clampTimeOut}

        with open(fileName, 'w') as configfile:
            self.parent.config.write(configfile)

        # Set these values for use from now on
        self.parent.clampRange = clampRange
        self.parent.clampSpeed = clampSpeed
        self.parent.clampTimeIn = clampTimeIn
        self.parent.clampTimeOut = clampTimeOut

        # Exit dialog
        self.Destroy()

        event.Skip()

    def OnButton_Exit(self, event):

        self.Destroy()

        event.Skip()


class dialogClampCustom_test(wx.App):
    '''Class to test dialogClampCustom.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close automatically'''
        super(dialogClampCustom_test, self).__init__()

        # Depending on auto_close, dialog needs opened differently
        if auto_close:
            dialogClampCustom(None).Show()
        else:
            dialogClampCustom(None).ShowModal()
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogClampCustom_test(False).MainLoop()
