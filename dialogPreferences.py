from os.path import dirname, join

import wx

from wxfb.layout import wxFB_dialog_preferences


class dialogPreferences(wxFB_dialog_preferences):
    def __init__(self, parent, autosave_interval, timer_sound):

        # Initialize parent class
        wxFB_dialog_preferences.__init__(self, parent)

        self.timer_sound = timer_sound

        # Set the values from parent
        self.spinCtrl_AutoSave.SetValue(autosave_interval / 60)
        self.choice_TimerSound.SetStringSelection(self.timer_sound)

    def OnTimerSound(self, event):

        filename = join(
            dirname(__file__), 'assets/sounds/%s.wav' % self.timer_sound
            )

        self.sound = wx.Sound(filename)
        if self.sound.IsOk():
            self.sound.Play(wx.SOUND_ASYNC)

        event.Skip()


class dialogPreferences_test(wx.App):
    '''Class for testing dialogPreferences.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, optionally to close automatically'''
        super(dialogPreferences_test, self).__init__()

        # Create dialog with sample data
        dialog = dialogPreferences(
            parent=None, autosave_interval=60, timer_sound="Exit Cue")

        # Depending on auto_close, dialog needs opened differently
        if auto_close:
            dialog.Show()
        else:
            dialog.ShowModal()
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogPreferences_test(False).MainLoop()
