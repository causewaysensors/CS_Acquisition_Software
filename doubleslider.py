from decimal import Decimal

import wx
from wx.lib.newevent import NewCommandEvent


class DoubleSlider(wx.Panel):
    """
    Double slider class
    - Horizontal or vertical
    - Handles cannot pass each other
    - Can set min difference between handles
    - Can be labbelled or unlabbelled
    """
    def __init__(
            self,
            parent,
            orientation='horizontal',
            minvalue=0,
            maxvalue=100,
            value1=0,
            value2=100,
            mindifference=0,
            stepsize=1,
            minupdatebehaviour='none',
            maxupdatebehaviour='none',
            showlabels=0,
            showrange=0,
            sliderthickness=30,
            handlethickness=10,
            barspace=5,
            textspace=3,
            linethickness=1,
            fontsize=10,
            ):
        """
        :param parent:
        :param orientation:       'horizontal' or 'vertical'
        :param minvalue:          min of slider
        :param maxvalue:          max of slider
        :param value1:            value of min handle
        :param value2:            value of max handle
        :param mindifference:     minimum difference between handles.
            Must be <= range
        :param stepsize:          step size of the slider
        :param minupdatebehaviour control how the min handle
            e.g. always go to maximum and minimum
        :param maxupdatebehaviour control how the max handle
            e.g. always go to maximum and minimum
        :param showlabels:        show text on left, right and bar
        :param sliderthickness:   horizontal/vertical - height/width of slider
        :param handlethickness:   horizontal/vertical - width/height of handles
        :param barspace:          cosmetic gap at sides of bar
        :param textspace:         combined space left around all text
            e.g. textspace/2 above, left, right, below
        :param linethickness:     thickness of outlines round handles
        :param fontsize:
        """

        # Initialise panel
        wx.Panel.__init__(
            self, parent=parent, id=wx.ID_ANY, style=wx.NO_BORDER)

        # Create the new events that can affect the slider
        self.Event_ValueChange, self.EVT_VALUECHANGE = NewCommandEvent()
        self.Event_UpdateChange, self.EVT_UPDATECHANGE = NewCommandEvent()

        # Variables from parent (allows customisation)
        self.orientation = orientation
        self.minvalue = minvalue
        self.maxvalue = maxvalue
        self.value1 = value1
        self.value2 = value2
        self.mindifference = mindifference
        self.stepsize = stepsize
        self.minupdatebehaviour = minupdatebehaviour
        self.maxupdatebehaviour = maxupdatebehaviour
        self.showlabels = showlabels
        self.showrange = showrange
        self.sliderthickness = sliderthickness
        self.handlethickness = handlethickness
        self.barspace = barspace
        self.textspace = textspace
        self.linethickness = linethickness
        self.fontsize = fontsize

        # Variables to be accessed by parent (are the values updating or
        # constant)
        self.value1_update = 0
        self.value2_update = 1
        # x-update: increasing with time, y-update: max values

        # Calculate panel height and width and set
        if self.orientation == 'vertical':
            self.panelwidth = self.sliderthickness
            self.panelheight = -1

            self.minwidth = self.sliderthickness
            self.minheight = 0

        elif self.orientation == 'horizontal':
            self.panelwidth = -1
            self.panelheight = self.sliderthickness

            self.minwidth = 0
            self.minheight = self.sliderthickness

        self.SetMinSize((self.minwidth, self.minheight))
        self.SetSize([self.panelwidth, self.panelheight])

        # Variables for drawing

        self.mintextspace = 0  # Space taken up by min text label
        self.maxtextsize = 0  # Space taken up by max text label
        self.fontfamily = wx.FONTFAMILY_DEFAULT
        self.fontstyle = wx.FONTSTYLE_NORMAL
        self.fontweight = wx.FONTWEIGHT_NORMAL

        # Create symbol and slider location variables

        self.slider_top = 0  # Extents of the whole slider
        self.slider_bottom = 0
        self.slider_left = 0
        self.slider_right = 0

        self.value1_top = 0  # Value 1 handle
        self.value1_bottom = 0
        self.value1_left = 0
        self.value1_right = 0

        self.value2_top = 0  # Value 2 handle
        self.value2_bottom = 0
        self.value2_left = 0
        self.value2_right = 0

        self.bar_top = 0  # Bar between handles
        self.bar_bottom = 0
        self.bar_left = 0
        self.bar_right = 0

        # Other initializations
        self.activesymbol = None

        self.clickposition = (0, 0)  # Dragging will be relative to this
        self.clickvalue1 = 0
        self.clickvalue2 = 0

        # Default Colours
        self.SetColours(
            background='White',
            outline=(0, 0, 0),
            foreground=(200, 200, 200),
            bar=(128, 128, 128),
            update=(255, 0, 0),
            hold=(0, 255, 0),
            )

        # Bind all events
        self.Bind(wx.EVT_RIGHT_DOWN, self.RightMouseDown)
        self.Bind(wx.EVT_LEFT_DOWN, self.LeftMouseDown)
        self.Bind(wx.EVT_LEFT_UP, self.LeftMouseUp)
        self.Bind(wx.EVT_MOTION, self.MouseMotion)
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.OnPass)  # Stops flicker
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_SIZE, self.OnSize)

    def SetColours(
            self,
            background='White',
            outline=(0, 0, 0),
            foreground=(200, 200, 200),
            bar=(128, 128, 128),
            update=(255, 0, 0),
            hold=(0, 255, 0),
            ):
        """
        :param outline:         Colour of outline around handles
        :param bg:              Colour of slider background
        :param bar:             Colour of bar
        :param update:          Colour of handle when updating
        :param hold:            Colour of handle when holding
        """
        self.SetBackgroundColour(background)
        self.colour_outline = wx.Colour(*outline)
        self.colour_foreground = wx.Colour(*foreground)
        self.colour_bar = wx.Colour(*bar)
        self.colour_update = wx.Colour(*update)
        self.colour_hold = wx.Colour(*hold)
        self.Refresh()

    def SetMinDifference(self, mindifference):
        """
        :param mindifference:      User can set min difference between handles
        """
        self.mindifference = mindifference
        self.Refresh()

        # Post the event
        event = self.Event_ValueChange(self.GetId())
        wx.PostEvent(self, event)

    def GetMinDifference(self):
        return self.mindifference

    def SetMinValue(self, minvalue):
        self.minvalue = minvalue

        # Reset both handles, in case the range is now too small
        self.UpdateValues(self.value1, 'value1')
        self.UpdateValues(self.value2, 'value2')

        self.Refresh()

        # Post the event
        event = self.Event_ValueChange(self.GetId())
        wx.PostEvent(self, event)

    def SetMaxValue(self, maxvalue):
        """
        Values will constantly be changed by data acquisition
        """
        self.maxvalue = maxvalue

        # Reset both handles, in case the range is now too small
        self.UpdateValues(self.value1, 'value1')
        self.UpdateValues(self.value2, 'value2')

        self.Refresh()

        # Post the event
        event = self.Event_ValueChange(self.GetId())
        wx.PostEvent(self, event)

    def GetRange(self):
        return self.minvalue, self.maxvalue

    def SetValue1(self, value):
        self.UpdateValues(value, 'value1')
        self.Refresh()

        # Post the event
        event = self.Event_ValueChange(self.GetId())
        wx.PostEvent(self, event)

    def SetValue2(self, value):
        self.UpdateValues(value, 'value2')
        self.Refresh()

        # Post the event
        event = self.Event_ValueChange(self.GetId())
        wx.PostEvent(self, event)

    def GetValues(self):
        return self.value1, self.value2

    def GetValue1(self):
        return self.value1

    def GetValue2(self):
        return self.value2

    def SetStepSize(self, stepsize):
        self.stepsize = stepsize
        self.Refresh()

    def RightMouseDown(self, event):
        """
        :param event:     Right-click event
        """

        # Get position of click
        x, y = event.GetPosition()

        # If in value1 symbol
        if (x >= self.value1_left
                and x <= self.value1_right
                and y <= self.value1_bottom
                and y >= self.value1_top):

            # May cause behaviours
            if self.value1_update:
                self.value1_update = False
            else:
                self.value1_update = True
                if self.minupdatebehaviour == "extremes":
                    self.UpdateValues(self.minvalue, "value1")

            self.Refresh()

            # Post the event
            event = self.Event_UpdateChange(self.GetId())
            wx.PostEvent(self, event)

        # If in value2 symbol
        if (x >= self.value2_left
                and x <= self.value2_right
                and y <= self.value2_bottom
                and y >= self.value2_top):

            if self.value2_update:
                self.value2_update = False
            else:
                self.value2_update = True
                if self.maxupdatebehaviour == "extremes":
                    self.UpdateValues(self.maxvalue, "value2")

            self.Refresh()

            # Post the event
            event = self.Event_UpdateChange(self.GetId())
            wx.PostEvent(self, event)

        # Stop event propagating
        event.Skip()

    def LeftMouseDown(self, event):
        # Get position of click
        self.clickposition = event.GetPosition()

        # If in value1 symbol
        if (self.clickposition[0] >= self.value1_left and
           self.clickposition[0] <= self.value1_right and
           self.clickposition[1] <= self.value1_bottom and
           self.clickposition[1] >= self.value1_top):

            self.activesymbol = 'value1'
            self.clickvalue1 = self.value1
            self.CaptureMouse()
            self.Refresh()

        # If in value2 symbol
        elif (self.clickposition[0] >= self.value2_left and
              self.clickposition[0] <= self.value2_right and
              self.clickposition[1] <= self.value2_bottom and
              self.clickposition[1] >= self.value2_top):

            self.activesymbol = 'value2'
            self.clickvalue2 = self.value2
            self.CaptureMouse()
            self.Refresh()

        # If in bar
        elif (self.clickposition[0] >= self.bar_left and
              self.clickposition[0] <= self.bar_right and
              self.clickposition[1] <= self.bar_bottom and
              self.clickposition[1] >= self.bar_top):

            self.activesymbol = 'bar'
            self.clickvalue1 = self.value1
            self.clickvalue2 = self.value2

            self.CaptureMouse()
            self.Refresh()

        # Stop event propagating
        event.Skip()

    def LeftMouseUp(self, event):
        if self.HasCapture() and self.activesymbol is not None:

            valuechange = self.ChangeFromPixels(
                event.GetPosition(), self.activesymbol)

            if self.activesymbol == 'value1':
                value = self.clickvalue1 + valuechange
                self.UpdateValues(value, self.activesymbol)
            elif self.activesymbol == 'value2':
                value = self.clickvalue2 + valuechange
                self.UpdateValues(value, self.activesymbol)
            elif self.activesymbol == 'bar':
                value1 = self.clickvalue1 + valuechange
                value2 = self.clickvalue2 + valuechange
                self.UpdateValues([value1, value2], self.activesymbol)

            self.activesymbol = None
            self.ReleaseMouse()
            self.Refresh()

            # Post the event
            event = self.Event_ValueChange(self.GetId())
            wx.PostEvent(self, event)

        # Stop event propagating
        event.Skip()

    def MouseMotion(self, event):
        if (event.Dragging()
                and event.LeftIsDown()
                and self.HasCapture()
                and self.activesymbol is not None):

            valuechange = self.ChangeFromPixels(
                event.GetPosition(), self.activesymbol)

            if self.activesymbol == 'value1':
                value = self.clickvalue1 + valuechange
                self.UpdateValues(value, self.activesymbol)
            elif self.activesymbol == 'value2':
                value = self.clickvalue2 + valuechange
                self.UpdateValues(value, self.activesymbol)
            elif self.activesymbol == 'bar':
                value1 = self.clickvalue1 + valuechange
                value2 = self.clickvalue2 + valuechange
                self.UpdateValues([value1, value2], self.activesymbol)

            self.Refresh()

            # Post the event
            event = self.Event_ValueChange(self.GetId())
            wx.PostEvent(self, event)

        # Stop event propagating
        event.Skip()

    def OnSize(self, event):
        # Make sure the widget does not get squashed
        panel_w, panel_h = self.GetSize()

        if panel_w < self.minwidth:
            panel_w = self.minwidth
        if panel_h < self.minheight:
            panel_h = self.minheight

        self.SetSize((panel_w, panel_h))
        self.Refresh()

        # Stop event propagating
        event.Skip()

    def OnPaint(self, event):

        # All values should be calculated from absolutes and not from variables
        # like panelw and panelh (which change)

        # Get size of panel
        panelw, panelh = self.GetSize()

        # Clear to background colour
        dc = wx.BufferedPaintDC(self)
        dc.Clear()

        # >>> Calculate size of space for text if applicable, and draw text >>>

        dc.SetFont(wx.Font(
            self.fontsize, self.fontfamily, self.fontstyle, self.fontweight))

        # No labels
        if self.showlabels == 0:
            self.minmaxtextspace = 0

        # Horizontal with labels
        elif self.orientation == 'horizontal' and self.showlabels == 1:

            stepdp = Decimal(str(self.stepsize))
            numberdp = stepdp.as_tuple().exponent

            if numberdp == 0:
                minformat = 'Min = %d'
                maxformat = 'Max = %d'
                format_ = '%d'
            elif numberdp == -1:
                minformat = 'Min = %0.1f'
                maxformat = 'Max = %0.1f'
                format_ = '%0.1f'
            else:
                minformat = 'Min = %0.2f'
                maxformat = 'Max = %0.2f'
                format_ = '%0.2f'

            min_w, min_h = dc.GetTextExtent(minformat % self.minvalue)
            max_w, max_h = dc.GetTextExtent(maxformat % self.maxvalue)
            value1_w, value1_h = dc.GetTextExtent(format_ % self.value1)
            value2_w, value2_h = dc.GetTextExtent(format_ % self.value2)

            # Calculate space on each side. This also determines min size of
            # widget
            self.minmaxtextspace = max([min_w, max_w]) + self.textspace * 2
            self.minwidth = self.minmaxtextspace * 2 + self.handlethickness * 2
            self.minheight = self.sliderthickness

            # MIN TEXT
            horzcent = self.minmaxtextspace / 2  # Centre for min text
            textw = min_w  # Draw minvalue
            texth = min_h  # "
            vertcent = (
                self.sliderthickness / 2 - self.textspace / 2 - texth / 2)
            text = minformat % self.minvalue
            dc.DrawText(text, horzcent - textw / 2, vertcent - texth / 2)
            textw = value1_w  # Draw value1
            texth = value1_h  # "
            vertcent = panelh / 2 + self.textspace / 2 + texth / 2
            text = format_ % self.value1
            dc.DrawText(text, horzcent - textw / 2, vertcent - texth / 2)

            # MAX TEXT
            horzcent = panelw - self.minmaxtextspace / 2  # Centre for max text
            textw = max_w
            texth = max_h
            vertcent = panelh / 2 - self.textspace / 2 - texth / 2
            text = maxformat % self.maxvalue
            dc.DrawText(text, horzcent - textw / 2, vertcent - texth / 2)
            textw = value2_w  # Draw value2
            texth = value2_h  #
            vertcent = panelh / 2 + self.textspace / 2 + texth / 2
            text = format_ % self.value2
            dc.DrawText(text, horzcent - textw / 2, vertcent - texth / 2)

        # Vertical with labels
        elif self.orientation == 'vertical' and self.showlabels == 1:

            stepdp = Decimal(str(self.stepsize))
            numberdp = stepdp.as_tuple().exponent

            if numberdp == 0:
                minformat = 'Min = %d'
                maxformat = 'Max = %d'
                format_ = '%d'
            elif numberdp == -1:
                minformat = 'Min = %0.1f'
                maxformat = 'Max = %0.1f'
                format_ = '%0.1f'
            else:
                minformat = 'Min = %0.2f'
                maxformat = 'Max = %0.2f'
                format_ = '%0.2f'

            # Go down sizes if text doesn't fit
            for i in range(0, self.fontsize-1, 1):

                dc.SetFont(wx.Font(
                    self.fontsize - i,
                    self.fontfamily,
                    self.fontstyle,
                    self.fontweight,
                    ))

                min_w, min_h = dc.GetTextExtent(minformat % self.minvalue)
                max_w, max_h = dc.GetTextExtent(maxformat % self.maxvalue)
                value1_w, value1_h = dc.GetTextExtent(format_ % self.value1)
                value2_w, value2_h = dc.GetTextExtent(format_ % self.value2)

                if max_w < self.sliderthickness:
                    break

            # Calculate space on each side
            self.minmaxtextspace = max([min_h + value1_h, max_h + value2_h])
            self.minmaxtextspace += self.textspace * 2
            self.minwidth = self.sliderthickness
            self.minheight = self.minmaxtextspace * 2
            self.minheight += self.handlethickness * 2

            # MIN TEXT
            horzcent = self.sliderthickness / 2  # Centre for min text
            textw = min_w  # Draw minvalue
            texth = min_h
            vertcent = panelh - self.minmaxtextspace / 2
            vertcent += self.textspace / 2 + texth / 2
            text = minformat % self.minvalue
            dc.DrawText(text, horzcent - textw / 2, vertcent - texth / 2)
            textw = value1_w  # Draw value1
            texth = value1_h  # "
            vertcent = panelh - self.minmaxtextspace / 2 - self.textspace / 2
            vertcent -= texth / 2
            text = format_ % self.value1
            dc.DrawText(text, horzcent - textw / 2, vertcent - texth / 2)

            # MAX TEXT
            horzcent = self.sliderthickness / 2  # Centre for max text
            textw = max_w  # Draw maxvalue
            texth = max_h  # "
            vertcent = self.minmaxtextspace / 2 - self.textspace / 2
            vertcent -= texth / 2
            text = maxformat % self.maxvalue
            dc.DrawText(text, horzcent - textw / 2, vertcent - texth / 2)
            textw = value2_w  # Draw value2
            texth = value2_h  # "
            vertcent = self.minmaxtextspace / 2 + self.textspace / 2
            vertcent += texth / 2
            text = format_ % self.value2
            dc.DrawText(text, horzcent - textw / 2, vertcent - texth / 2)

        # Draw the background of the slider
        dc.SetPen(wx.Pen(
            self.colour_foreground,
            width=self.linethickness,
            style=wx.TRANSPARENT,
            ))
        dc.SetBrush(wx.Brush(self.colour_foreground))

        if self.orientation == 'vertical':

            self.slider_left = 0
            self.slider_right = self.sliderthickness
            self.slider_top = self.minmaxtextspace
            self.slider_bottom = panelh - self.minmaxtextspace

            dc.DrawRectangle(self.slider_left,
                             self.slider_top,
                             self.slider_right - self.slider_left,
                             self.slider_bottom - self.slider_top)

            self.bar_left = self.barspace
            self.bar_right = self.slider_right - self.barspace

        elif self.orientation == 'horizontal':

            self.slider_left = self.minmaxtextspace
            self.slider_right = panelw - self.minmaxtextspace
            self.slider_top = 0
            self.slider_bottom = panelh

            dc.DrawRectangle(self.slider_left,
                             self.slider_top,
                             self.slider_right - self.slider_left,
                             self.slider_bottom - self.slider_top)

            self.bar_top = self.slider_top + self.barspace
            self.bar_bottom = self.slider_bottom - self.barspace

        # Calculate handle and bar extents

        # Value1 handle + bar
        if self.orientation == 'vertical':

            self.value1_top = self.PixelFromValue(self.value1)
            self.value1_bottom = self.value1_top + self.handlethickness

            self.value1_left = 0
            self.value1_right = self.sliderthickness

            self.bar_bottom = self.value1_top + 1  # Leave extra pixel

        elif self.orientation == 'horizontal':

            self.value1_right = self.PixelFromValue(self.value1)
            self.value1_left = self.value1_right - self.handlethickness

            self.value1_top = 0
            self.value1_bottom = self.sliderthickness

            self.bar_left = self.value1_right - 1

        # Value2 slider + bar
        if self.orientation == 'vertical':

            self.value2_bottom = self.PixelFromValue(self.value2)
            self.value2_top = self.value2_bottom - self.handlethickness

            self.value2_left = 0
            self.value2_right = self.sliderthickness

            self.bar_top = self.value2_bottom - 1

        elif self.orientation == 'horizontal':

            self.value2_left = self.PixelFromValue(self.value2)
            self.value2_right = self.value2_left + self.handlethickness

            self.value2_top = 0
            self.value2_bottom = self.sliderthickness

            self.bar_right = self.value2_left + 1

        # -- Draw handles and bar ---------------------------------------------

        # Bar
        dc.SetPen(wx.Pen(
            self.colour_foreground,
            width=self.linethickness,
            style=wx.TRANSPARENT,
            ))
        dc.SetBrush(wx.Brush(self.colour_bar))

        dc.DrawRectangle(self.bar_left,
                         self.bar_top,
                         self.bar_right - self.bar_left,
                         self.bar_bottom - self.bar_top)

        # Outline of handles
        dc.SetPen(wx.Pen(
            self.colour_outline,
            width=self.linethickness,
            style=wx.SOLID,
            ))

        # Handle 1
        if self.value1_update:
            dc.SetBrush(wx.Brush(self.colour_hold))
        else:
            dc.SetBrush(wx.Brush(self.colour_update))

        dc.DrawRectangle(self.value1_left,
                         self.value1_top,
                         self.value1_right - self.value1_left,
                         self.value1_bottom - self.value1_top)

        # Handle 2
        if self.value2_update:
            dc.SetBrush(wx.Brush(self.colour_hold))
        else:
            dc.SetBrush(wx.Brush(self.colour_update))

        dc.DrawRectangle(self.value2_left,
                         self.value2_top,
                         self.value2_right - self.value2_left,
                         self.value2_bottom - self.value2_top)

        # Write range in middle of bar (IF IT FITS)

        if self.showrange == 1:

            # Reset font in case it has been changed
            dc.SetFont(wx.Font(
                self.fontsize,
                self.fontfamily,
                self.fontstyle,
                self.fontweight,
                ))
            range_w, range_h = dc.GetTextExtent(
                format_ % (self.value2 - self.value1))

            if range_w < (self.bar_right - 1) - (self.bar_left + 1):
                # Draw max with text
                text = format_ % (self.value2 - self.value1)
                horzcent = ((self.bar_right - 1) - (self.bar_left + 1)) / 2
                horzcent += (self.bar_left + 1) - range_w / 2
                vertcent = (self.bar_bottom - self.bar_top) / 2 + self.bar_top
                dc.DrawText(text, horzcent, vertcent - range_h / 2)

        # Stop event propagating
        event.Skip()

    def OnPass(self, event):
        '''Stop event propagating'''
        event.Skip()

    def ChangeFromPixels(self, newposition, activesymbol):
        if self.orientation == 'vertical':

            # Area that the mouse can possibly be in
            if activesymbol == 'value1':
                area_top = self.slider_top
                area_bottom = self.slider_bottom - self.handlethickness
            elif activesymbol == 'value2':
                area_top = self.slider_top + self.handlethickness
                area_bottom = self.slider_bottom
            elif activesymbol == 'bar':
                area_top = self.slider_top + self.handlethickness
                area_bottom = self.slider_bottom - self.handlethickness

            pixel = newposition[1]  # VERTICAL - positions are x, y

            if pixel < area_top:
                difference = self.maxvalue
            elif pixel > area_bottom:
                difference = self.minvalue
            else:
                lengtharea = area_bottom - area_top
                pixelsfrommin = self.clickposition[1] - pixel
                percentage = float(pixelsfrommin) / lengtharea
                difference = percentage * (self.maxvalue - self.minvalue)

        elif self.orientation == 'horizontal':

            # Area that the mouse can possibly be in
            if activesymbol == 'value1':
                area_left = self.slider_left
                area_right = self.slider_right - self.handlethickness
            elif activesymbol == 'value2':
                area_left = self.slider_left + self.handlethickness
                area_right = self.slider_right
            elif activesymbol == 'bar':
                area_left = self.slider_left + self.handlethickness
                area_right = self.slider_right - self.handlethickness

            pixel = newposition[0]  # HORIZONTAL - positions are x, y

            if pixel > self.slider_right:
                difference = self.maxvalue
            elif pixel < self.slider_left:
                difference = self.minvalue
            else:
                lengtharea = area_right - area_left
                # Difference in pixel values
                pixelsfrommin = pixel - self.clickposition[0]
                percentage = float(pixelsfrommin) / lengtharea
                difference = percentage * (self.maxvalue - self.minvalue)

        # Round this to the desired step size and return
        difference = difference / self.stepsize
        difference = round(difference, 0)
        difference = difference * self.stepsize

        return difference

    def PixelFromValue(self, value):
        # Need to handle zeros
        if self.maxvalue - self.minvalue == 0:
            percentage = 0.5
        elif value - self.minvalue == self.maxvalue - self.minvalue:
            percentage = 1
        elif value - self.minvalue == 0:
            percentage = 0
        else:
            numerator = float(value - self.minvalue)
            denominator = (self.maxvalue - self.minvalue)
            percentage = numerator / denominator

        if self.orientation == 'vertical':
            area_top = self.slider_top + self.handlethickness
            area_bottom = self.slider_bottom - self.handlethickness
            lengtharea = area_bottom - area_top
            pixelsfrommin = percentage * lengtharea
            pixel = area_bottom - pixelsfrommin
        elif self.orientation == 'horizontal':
            area_left = self.slider_left + self.handlethickness
            area_right = self.slider_right - self.handlethickness
            lengtharea = area_right - area_left
            pixelsfrommin = percentage * lengtharea
            pixel = area_left + pixelsfrommin

        return pixel

    def UpdateValues(self, value, activesymbol):
        """
        Update the values
        Depending on update and behaviour, values may be locked
        """

        if activesymbol == 'value1':

            # Behaviour - Extremes - Min
            if (self.value1_update == 1
                    and self.minupdatebehaviour == 'extremes'):
                self.value1 = self.minvalue

            # Behaviour - Other
            elif self.minupdatebehaviour == 'otherbehaviour':
                pass

            # Normal behaviour
            else:

                # Set value first
                self.value1 = value

                # Make sure this value is valid
                if self.value1 > self.maxvalue - self.mindifference:
                    self.value1 = self.maxvalue - self.mindifference

                # This may push value1 under min
                if self.value1 < self.minvalue:
                    self.value1 = self.minvalue

                # If required, push value2 up
                if self.value2 < self.value1 + self.mindifference:
                    self.value2 = self.value1 + self.mindifference

                # Make sure the value is not pushing value 2 over max
                if self.value2 > self.maxvalue:
                    self.value2 = self.maxvalue

        elif activesymbol == 'value2':

            # Behaviour - Extremes - Max
            if (self.value2_update == 1
                    and self.maxupdatebehaviour == 'extremes'):
                self.value2 = self.maxvalue

            # Behaviour - Other
            elif self.maxupdatebehaviour == 'otherbehaviour':
                pass

            # Normal behaviour
            else:

                # Set value first
                self.value2 = value

                # Make sure this value is valid
                if self.value2 < self.minvalue + self.mindifference:
                    self.value2 = self.minvalue + self.mindifference

                # This may push value2 over max
                if self.value2 > self.maxvalue:
                    self.value2 = self.maxvalue

                # If required, push value1 down
                if self.value1 > self.value2 - self.mindifference:
                    self.value1 = self.value2 - self.mindifference

                # Make sure the value is not pushing value1 under min
                if self.value1 < self.minvalue:
                    self.value1 = self.minvalue

        elif activesymbol == 'bar':

            if not ((self.value1_update == 1 or self.value2_update == 1) and
               self.minupdatebehaviour == 'extremes'):
                # Want to keep this constant
                olddifference = self.value2 - self.value1
                # Set value first
                self.value1 = value[0]
                self.value2 = value[1]

                if self.value2 > self.maxvalue:
                    self.value2 = self.maxvalue
                    self.value1 = self.value2 - olddifference
                elif self.value1 < self.minvalue:
                    self.value1 = self.minvalue
                    self.value2 = self.value1 + olddifference


class doubleslider_test(wx.App):
    '''Class to test doubleslider'''
    def __init__(self, auto_close=True):
        """Opens a test frame with a vertical and horizontal slider

        Horizontal slider snaps to max only
        Vertical slider snaps to both extremes
        """
        super(doubleslider_test, self).__init__()

        frame = wx.Frame(None)
        sizer = wx.BoxSizer(wx.VERTICAL)

        # Horizontal doubleslider which snaps to max on r-click only
        h_slider = DoubleSlider(
            parent=frame,
            orientation='horizontal',
            maxupdatebehaviour='extremes',
            showlabels=1,
            showrange=1,
            sliderthickness=50,
            )

        # Vertical doubleslider which snaps to extremes on r-click
        v_slider = DoubleSlider(
            parent=frame,
            orientation='vertical',
            minupdatebehaviour='extremes',
            maxupdatebehaviour='extremes',
            showlabels=1,
            showrange=1,
            sliderthickness=50,
            )

        sizer.Add(h_slider, 0, wx.EXPAND | wx.ALL, 20)
        sizer.Add(v_slider, 1, wx.CENTRE | wx.ALL, 20)

        frame.Show()
        frame.SetSizerAndFit(sizer)
        frame.Maximize()

        if auto_close:
            self.TopWindow.Destroy()


if __name__ == '__main__':
    doubleslider_test(False).MainLoop()
