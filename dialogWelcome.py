from time import sleep

from wx import App

from wxfb.layout import wxFB_dialog_welcome


class dialogWelcome(wxFB_dialog_welcome):
    def __init__(self, parent, message=""):

        # Initialize parent class
        wxFB_dialog_welcome.__init__(self, parent)

        self.text_welcome.SetLabelText(message)

    def ChangeMessage(self, newmessage):

        self.text_welcome.SetLabelText(newmessage)
        self.Layout()
        self.Refresh()


class dialogWelcome_test(App):
    '''Class to test dialogWelcome.py'''
    def __init__(self, auto_close=True):
        '''Setup the test object, run tests and close'''
        super(dialogWelcome_test, self).__init__()

        # Create dialog with sample
        dialog = dialogWelcome(parent=None, message="")
        dialog.Show()

        # List of functions to run [function, arguments]
        listTests = [
            [dialog.ChangeMessage, ["New message"]],
            [dialog.ChangeMessage, ["Another new message"]],
            [dialog.ChangeMessage, ["Final message"]],
            ]
        for function, arguments in listTests:
            sleep(0 if auto_close else 1)
            function(*arguments)

        # Welcome dialog has no close button so automatically destroy
        self.TopWindow.Destroy()


if __name__ == '__main__':
    dialogWelcome_test(False).MainLoop()
