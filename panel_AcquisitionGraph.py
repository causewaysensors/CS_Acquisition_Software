"""Graph panel for plotting data during acquisition from sensor."""
from math import ceil, floor
from time import sleep

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigCanvas
from matplotlib.figure import Figure

from pylab import setp

import wx
from wx.lib.newevent import NewCommandEvent

from doubleslider import DoubleSlider


class panel_AcquisitionGraph(wx.Panel):
    """Panel for displaying acquisition data including control doublesliders

    This panel contains the canvas and figure for plotting the graphs as well
    as the methods for resizing the figure and displaying the current x,y
    position on the status bar.
    """
    def __init__(
            self,
            parent,
            id=wx.ID_ANY,
            position=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=0,
            name="",
            ):

        wx.Panel.__init__(self, parent, id, position, size, style, name)

        self.parent = parent

        # Set background colour
        self.SetBackgroundColour("White")

        # Set variables
        self.dpi = 100
        self.figure_gap = 0.7

        # Set up MPL stuff
        self.figure = Figure(None, dpi=self.dpi)

        # Create canvas and graphpan
        self.canvas = FigCanvas(self, wx.ID_ANY, self.figure)
        self.canvas.SetMinSize([-1, 200])

        # Set up scroll bars
        self.vscroll = DoubleSlider(
            parent=self,
            orientation='vertical',
            minupdatebehaviour='extremes',
            maxupdatebehaviour='extremes',
            mindifference=0.01,
            stepsize=0.01,
            showlabels=1,
            showrange=0,
            sliderthickness=50,
            )

        self.hscroll = DoubleSlider(
            parent=self,
            orientation='horizontal',
            maxupdatebehaviour='extremes',
            showlabels=1,
            showrange=1,
            sliderthickness=50,
            )

        # Use sizer to arrange things
        self.flexsizer1 = wx.FlexGridSizer(2, 2, 0, 0)
        self.flexsizer1.AddGrowableCol(1)
        self.flexsizer1.AddGrowableRow(0)

        self.flexsizer1.Add(self.vscroll, 0, wx.EXPAND | wx.ALL, 10)
        self.flexsizer1.Add(self.canvas, 0, wx.EXPAND | wx.ALL, 10)
        self.flexsizer1.AddSpacer((0, 0), 0, wx.EXPAND | wx.ALL, 10)
        self.flexsizer1.Add(self.hscroll, 0, wx.EXPAND | wx.ALL, 10)

        self.SetSizerAndFit(self.flexsizer1)

        # Method so it can be called again
        self.Reset()

        # Set multipler
        self.multiplier_2 = 1.0

        # SET UP EVENTS INSIDE CANVAS
        self.leftpress = None
        self.rightpress = None

        # Detect clicks
        self.canvas.mpl_connect('button_press_event', self.OnCanvasButtonPress)

        # Detect movement
        self.canvas.mpl_connect('motion_notify_event', self.OnCanvasMouseMove)

        # Detect releases
        self.canvas.mpl_connect(
            'button_release_event', self.OnCanvasButtonRelease)

        # Add bind events for the double sliders
        self.Bind(self.hscroll.EVT_VALUECHANGE, self.OnValueChangeHorizontal)
        self.Bind(self.vscroll.EVT_VALUECHANGE, self.OnValueChangeVertical)

        self.Bind(self.hscroll.EVT_UPDATECHANGE, self.OnUpdateChangeHorizontal)
        self.Bind(self.vscroll.EVT_UPDATECHANGE, self.OnUpdateChangeVertical)

        # Creat events that it will send on to mainwindow
        # To adjust text
        self.Event_CrosshairChange, self.EVT_CROSSHAIRCHANGE = \
            NewCommandEvent()

        # Bind resize event
        self.Bind(wx.EVT_SIZE, self.OnSize)

    def OnSize(self, event):
        """Adjust layout of mpl figure on resize."""
        try:
            self.figure.tight_layout(pad=self.figure_gap, w_pad=0, h_pad=0)
        except ValueError:
            pass

        # Let the event propagate
        event.Skip()

    def OnCanvasButtonPress(self, event):
        """Handle mouse clicks."""
        if event.inaxes:
            if event.button == 1:
                x0 = event.xdata
                y0 = event.ydata
                self.leftpress = x0, y0

            elif event.button == 3:
                x0 = event.xdata
                y0 = event.ydata
                self.rightpress = x0, y0

    def OnCanvasMouseMove(self, event):
        """Handle mouse dragging events."""
        # Left mouse down inside axes
        if self.leftpress is None and self.rightpress is None:
            return

        # Right mouse held down inside axes
        elif self.rightpress is not None and event.inaxes:

            # capture mouse motion
            x = event.xdata

            if len(self.array_time) > 0:
                # Find the time that is closest to the right click
                self.crosshair_time = min(
                    self.array_time,
                    key=lambda y: abs(y - x),
                    )
            else:
                self.crosshair_time = 0

            self.MoveCrosshair()

    def MoveCrosshair(self):
        """Redraw crosshair after move and pass event to update status."""
        # Hides any previously drawn crosshair
        if hasattr(self, "crosshairvert"):
            self.crosshairvert.set_visible(False)

        self.crosshairvert = self.axes.axvline(
            self.crosshair_time, linewidth=1, color="Green")

        # Find y values
        if len(self.array_time) > 0:
            index = self.array_time.index(self.crosshair_time)

            self.crosshair_y1 = self.array_y1[index]
            self.crosshair_y2 = self.array_y2[index]
            self.crosshair_y2y1 = self.array_y2y1[index]
            self.crosshair_y1_counts = self.array_y1_counts[index]
            self.crosshair_y2_counts = self.array_y2_counts[index]
        else:
            self.crosshair_y1 = 0
            self.crosshair_y2 = 0
            self.crosshair_y2y1 = 0
            self.crosshair_y1_counts = 0
            self.crosshair_y2_counts = 0

        # Post the event so the GUI redraws the text
        event = self.Event_CrosshairChange(self.GetId())
        wx.PostEvent(self, event)

        # Redraw
        self.Draw()

    def OnCanvasButtonRelease(self, event):
        """Reset all mouse clicks."""
        # Reset mouse clicks
        self.leftpress = None
        self.rightpress = None

    def OnValueChangeHorizontal(self, event):
        """Change xrange of displayed range."""
        self.display_xmin, self.display_xmax = self.hscroll.GetValues()

        # Adjust the vscroll
        self.AdjustVScroll()

        self.Draw()

        # Stop event propagating
        event.Skip()

    def OnValueChangeVertical(self, event):
        """Change yrange of displayed data."""
        self.display_ymin, self.display_ymax = self.vscroll.GetValues()

        self.Draw()

        # Stop event propagating
        event.Skip()

    def OnUpdateChangeHorizontal(self, event):
        """Auto update xrange if horizontal slider has update behaviour."""
        if self.hscroll.value2_update == 1:
            self.display_xmax = self.hscroll.maxvalue
            self.AdjustVScroll()

        self.Draw()

        # Stop event propagating
        event.Skip()

    def OnUpdateChangeVertical(self, event):
        """Auto update yrange if vertcal slider has 'update' behaviour."""
        # Goes to max or min if green
        if self.vscroll.value1_update == 1:
            self.display_ymin = self.vscroll.minvalue

        if self.vscroll.value2_update == 1:
            self.display_ymax = self.vscroll.maxvalue

        self.Draw()

        # Stop event propagating
        event.Skip()

    def ReceiveTwoValues(self, time, y1, y2):
        """Receive time and counts for both sensors from GUI loop

        - Treat first value differently as this must be a baseline
        - Add values to all relevant arrays
        - Save first value in all arrays
        - If this is larger than xmax increase xslider by xbuffer
        - If this is larger or smaller than ylimits, adjust the y-slider by
          round up or down (-2 = nearest hundred).
        """
        # Multiply counts by correction factor
        y2 = y2 * self.multiplier_2

        # Create fake data to eliminate zero counts
        if y2 == 0:
            y2 = 1
        if y1 == 0:
            y1 = 1

        # If first value
        if self.firstValue:
            self.baseline_y1_counts = y1
            self.baseline_y2_counts = y2

            self.array_baseline_times.append(time)

            self.firstValue = False

        # Calculate the percentages
        numerator = y1 - self.baseline_y1_counts
        y1_percent = numerator / self.baseline_y1_counts * 100.0
        numerator = y2 - self.baseline_y2_counts
        y2_percent = numerator / self.baseline_y2_counts * 100.0

        # Add everything to right arrays and check if it needs plotted
        self.dataadded = False

        self.array_time_1.append(time)  # x1 - Always add

        self.array_y1_counts_1.append(y1)
        self.array_y2_counts_1.append(y2)

        self.array_y1_1.append(y1_percent)
        self.array_y2_1.append(y2_percent)
        self.array_y2y1_1.append(y2_percent-y1_percent)

        lengthArray = len(self.array_time_1)

        if self.displayratio == "x1":
            self.dataadded = True

            self.array_time.append(time)

            self.array_y1_counts.append(y1)
            self.array_y2_counts.append(y2)

            self.array_y1.append(y1_percent)
            self.array_y2.append(y2_percent)
            self.array_y2y1.append(y2_percent-y1_percent)

        if lengthArray % 2 == 1:                                # x2
            self.array_time_2.append(time)

            self.array_y1_counts_2.append(y1)
            self.array_y2_counts_2.append(y2)

            self.array_y1_2.append(y1_percent)
            self.array_y2_2.append(y2_percent)
            self.array_y2y1_2.append(y2_percent-y1_percent)

            if self.displayratio == "x2":
                self.dataadded = True

                self.array_time.append(time)

                self.array_y1_counts.append(y1)
                self.array_y2_counts.append(y2)

                self.array_y1.append(y1_percent)
                self.array_y2.append(y2_percent)
                self.array_y2y1.append(y2_percent-y1_percent)

        if lengthArray % 4 == 1:                                # x4
            self.array_time_4.append(time)

            self.array_y1_counts_4.append(y1)
            self.array_y2_counts_4.append(y2)

            self.array_y1_4.append(y1_percent)
            self.array_y2_4.append(y2_percent)
            self.array_y2y1_4.append(y2_percent-y1_percent)

            if self.displayratio == "x4":
                self.dataadded = True
                self.array_time.append(time)

                self.array_y1_counts.append(y1)
                self.array_y2_counts.append(y2)

                self.array_y1.append(y1_percent)
                self.array_y2.append(y2_percent)
                self.array_y2y1.append(y2_percent-y1_percent)

        if lengthArray % 8 == 1:                                # x8
            self.array_time_8.append(time)

            self.array_y1_counts_8.append(y1)
            self.array_y2_counts_8.append(y2)

            self.array_y1_8.append(y1_percent)
            self.array_y2_8.append(y2_percent)
            self.array_y2y1_8.append(y2_percent - y1_percent)

            if self.displayratio == "x8":
                self.dataadded = True
                self.array_time.append(time)

                self.array_y1_counts.append(y1)
                self.array_y2_counts.append(y2)

                self.array_y1.append(y1_percent)
                self.array_y2.append(y2_percent)
                self.array_y2y1.append(y2_percent - y1_percent)

        if lengthArray % 16 == 1:                               # x16
            self.array_time_16.append(time)

            self.array_y1_counts_16.append(y1)
            self.array_y2_counts_16.append(y2)

            self.array_y1_16.append(y1_percent)
            self.array_y2_16.append(y2_percent)
            self.array_y2y1_16.append(y2_percent - y1_percent)

            if self.displayratio == "x16":
                self.dataadded = True
                self.array_time.append(time)

                self.array_y1_counts.append(y1)
                self.array_y2_counts.append(y2)

                self.array_y1.append(y1_percent)
                self.array_y2.append(y2_percent)
                self.array_y2y1.append(y2_percent - y1_percent)

        # Do we need to plot this point
        if not self.dataadded:
            return

        # Does the max time need increased
        if time > self.extents_xmax:

            self.extents_xmax = self.extents_xmax + self.xbuffer
            self.hscroll.SetMaxValue(self.extents_xmax)

            # This may cause the text to grow
            self.figure.tight_layout(pad=self.figure_gap, w_pad=0, h_pad=0)

            # Move handles as required
            if self.hscroll.value1_update == 1:

                self.display_xmin = self.display_xmin + self.xbuffer
                self.hscroll.SetValue1(self.display_xmin)

            if self.hscroll.value2_update == 1:
                self.display_xmax = self.extents_xmax
                self.hscroll.SetValue2(self.display_xmax)

        # Adjust the vscroll
        self.AdjustVScroll()

        # Draw graph
        self.Draw()

    def CalculateYSpace(self, y1, y2):
        """Choose spacing around data as percentage change increases."""
        # WIP - Should return instead of using self.
        difference = max(y1, y2) - min(y1, y2)

        if difference < 20:
            self.ytonearest = 1.0
        elif difference < 500:
            self.ytonearest = 10.0
        elif difference < 5000:
            self.ytonearest = 100.0
        elif difference < 50000:
            self.ytonearest = 1000.0
        else:
            self.ytonearest = 5000.0

    def ChangeDisplayRatio(self, newratio):
        """Reduce plotting frequency to improve performance

        Can display every 2nd/4th/8th/16th data point to reduce plotting in
        large data sets. Not currently able to modify from x1.
        """
        # WIP - Take out or fix
        self.displayratio = newratio

        # Update plot array
        if self.displayratio == "x1":
            self.array_time = self.array_time_1

            self.array_y1 = self.array_y1_1
            self.array_y2 = self.array_y2_1
            self.array_y2y1 = self.array_y2y1_1

            self.array_y1_counts = self.array_y1_counts_1
            self.array_y2_counts = self.array_y2_counts_1

        elif self.displayratio == "x2":
            self.array_time = self.array_time_2

            self.array_y1 = self.array_y1_2
            self.array_y2 = self.array_y2_2
            self.array_y2y1 = self.array_y2y1_2

            self.array_y1_counts = self.array_y1_counts_2
            self.array_y2_counts = self.array_y2_counts_2

        elif self.displayratio == "x4":
            self.array_time = self.array_time_4

            self.array_y1 = self.array_y1_4
            self.array_y2 = self.array_y2_4
            self.array_y2y1 = self.array_y2y1_4

            self.array_y1_counts = self.array_y1_counts_4
            self.array_y2_counts = self.array_y2_counts_4

        elif self.displayratio == "x8":
            self.array_time = self.array_time_8

            self.array_y1 = self.array_y1_8
            self.array_y2 = self.array_y2_8
            self.array_y2y1 = self.array_y2y1_8

            self.array_y1_counts = self.array_y1_counts_8
            self.array_y2_counts = self.array_y2_counts_8

        elif self.displayratio == "x16":
            self.array_time = self.array_time_16

            self.array_y1 = self.array_y1_16
            self.array_y2 = self.array_y2_16
            self.array_y2y1 = self.array_y2y1_16

            self.array_y1_counts = self.array_y1_counts_16
            self.array_y2_counts = self.array_y2_counts_16

        # May have changed y axis
        self.AdjustVScroll()

        # Crosshair may need moved (baseline vertical lines can stay)
        if self.crosshair_time not in self.array_time:

            # Get new time
            closestvalue = min(
                self.array_time,
                key=lambda x: abs(x - self.crosshair_time),
                )
            index = self.array_time.index(closestvalue)
            self.crosshair_time = closestvalue

            # Hides any previously drawn crosshair
            if hasattr(self, "crosshairvert"):
                self.crosshairvert.set_visible(False)

            # Draw the new one
            self.crosshairvert = self.axes.axvline(
                self.crosshair_time, linewidth=1, color="Green")

            # Get new y1 and y2 values
            self.crosshair_y1 = self.array_y1[index]
            self.crosshair_y2 = self.array_y2[index]
            self.crosshair_y2y1 = self.array_y2y1[index]
            self.crosshair_y1_counts = self.array_y1_counts[index]
            self.crosshair_y2_counts = self.array_y2_counts[index]

        # Redraw
        self.Draw()

    def AdjustVScroll(self):
        """Calculate yrange based on the data in xrange

        Invoked in two instances
        1) When the x-range is changed manually calculate the parts of the
           array required
        2) When new data is added
        Change in xrange may mean current yrange is no longer applicable so
        adjust.
        """
        # If array is blank, default to
        if len(self.array_time_1) == 0:
            minvalue = 0
            maxvalue = 0

        # Treat first value differently (get min off zero)
        elif len(self.array_time_1) == 1:

            if self.graphtype == "Both sensors":
                minvalue = min(self.array_y1[0], self.array_y2[0])
                maxvalue = max(self.array_y1[0], self.array_y2[0])
            elif self.graphtype == "Sensor 1 only":
                minvalue = self.array_y1[0]
                maxvalue = self.array_y1[0]
            elif self.graphtype == "Sensor 2 only":
                minvalue = self.array_y2[0]
                maxvalue = self.array_y2[0]
            elif self.graphtype == "Subtracted (2-1)":
                minvalue = self.array_y2y1[0]
                maxvalue = self.array_y2y1[0]
            elif self.graphtype == "Raw counts":
                minvalue = min(
                    self.array_y1_counts[0], self.array_y2_counts[0])
                maxvalue = max(
                    self.array_y1_counts[0], self.array_y2_counts[0])

        # All other values - Need to work out what is displayed
        else:

            try:  # xBuffer is not in array yet so try
                minindex = self.array_time.index(self.display_xmin)
            except ValueError:
                minindex = 0

            try:  # xBuffer is not in array yet so try
                maxindex = self.array_time.index(self.display_xmax)
            except ValueError:
                maxindex = None

            if self.graphtype == "Both sensors":
                minvalue = min(
                    min(self.array_y1[minindex:maxindex]),
                    min(self.array_y2[minindex:maxindex]),
                    )
                maxvalue = max(
                    max(self.array_y1[minindex:maxindex]),
                    max(self.array_y2[minindex:maxindex]),
                    )
            elif self.graphtype == "Sensor 1 only":
                minvalue = min(self.array_y1[minindex:maxindex])
                maxvalue = max(self.array_y1[minindex:maxindex])
            elif self.graphtype == "Sensor 2 only":
                minvalue = min(self.array_y2[minindex:maxindex])
                maxvalue = max(self.array_y2[minindex:maxindex])
            elif self.graphtype == "Subtracted (2-1)":
                minvalue = min(self.array_y2y1[minindex:maxindex])
                maxvalue = max(self.array_y2y1[minindex:maxindex])
            elif self.graphtype == "Raw counts":
                minvalue = min(
                    min(self.array_y1_counts[minindex:maxindex]),
                    min(self.array_y2_counts[minindex:maxindex]),
                    )
                maxvalue = max(
                    max(self.array_y1_counts[minindex:maxindex]),
                    max(self.array_y2_counts[minindex:maxindex]),
                    )

        # Calculate spacings
        self.CalculateYSpace(minvalue, maxvalue)

        denominator = self.ytonearest
        self.extents_ymax = (ceil(maxvalue / self.ytonearest)) * denominator
        self.extents_ymin = (floor(minvalue / self.ytonearest)) * denominator

        # Exclude zeros and add small space in case of perfect round
        if self.extents_ymax == maxvalue:
            self.extents_ymax = maxvalue + self.ytonearest
        if self.extents_ymin == minvalue:
            self.extents_ymin = minvalue - self.ytonearest

        self.vscroll.SetMinValue(self.extents_ymin)
        self.vscroll.SetMaxValue(self.extents_ymax)

        # Move handles as required
        if self.vscroll.value1_update == 1:
            self.display_ymin = self.extents_ymin
            self.vscroll.SetValue1(self.display_ymin)

        if self.vscroll.value2_update == 1:
            self.display_ymax = self.extents_ymax
            self.vscroll.SetValue2(self.display_ymax)

    def Reset(self):
        """Clear all data arrays and clear mpl figure."""
        # Things that need accessed from outside the program
        self.firstValue = True

        # Both sensors, Sensor 1 only, Sensor 2 only, Subtracted (2-1)
        self.graphtype = "Both sensors"

        # Maximum number of displayed point for performance
        self.displayratio = "x1"
        self.xbuffer = 60  # Iterate up the xrange by buffer
        self.ytonearest = 1.0  # Space above and below lines
        self.data_time = 0  # For the timer
        self.crosshair_time = 0  # Where is the crosshair
        self.crosshair_y1 = 0
        self.crosshair_y2 = 0
        self.crosshair_y2y1 = 0
        self.crosshair_y1_counts = 0
        self.crosshair_y2_counts = 0

        self.text_time = "-"  # Text that is displayed
        self.text_y1 = "-"
        self.text_y2 = "-"
        self.text_y2y1 = "-"

        # Arrays to store all the data
        self.array_time_1 = []
        self.array_time_2 = []
        self.array_time_4 = []
        self.array_time_8 = []
        self.array_time_16 = []

        self.array_y1_counts_1 = []
        self.array_y1_counts_2 = []
        self.array_y1_counts_4 = []
        self.array_y1_counts_8 = []
        self.array_y1_counts_16 = []

        self.array_y2_counts_1 = []
        self.array_y2_counts_2 = []
        self.array_y2_counts_4 = []
        self.array_y2_counts_8 = []
        self.array_y2_counts_16 = []

        self.array_y1_1 = []
        self.array_y1_2 = []
        self.array_y1_4 = []
        self.array_y1_8 = []
        self.array_y1_16 = []

        self.array_y2_1 = []
        self.array_y2_2 = []
        self.array_y2_4 = []
        self.array_y2_8 = []
        self.array_y2_16 = []

        self.array_y2y1_1 = []
        self.array_y2y1_2 = []
        self.array_y2y1_4 = []
        self.array_y2y1_8 = []
        self.array_y2y1_16 = []

        # Choose the correct array for the displayed data
        self.array_time = []
        self.array_y1 = []
        self.array_y2 = []
        self.array_y2y1 = []
        self.array_y1_counts = []
        self.array_y2_counts = []

        # Baseline stuff
        self.array_baseline_times = []  # Times of all the baselines

        self.baseline_y1_counts = 0  # Current baseline values
        self.baseline_y2_counts = 0

        self.array_flag_times = []
        self.array_flag_colours = []

        # Limits of the graph
        self.extents_xmin = 0
        self.extents_xmax = self.xbuffer

        self.display_xmin = 0
        self.display_xmax = self.xbuffer

        # Reset the updates on the sliders
        self.hscroll.value1_update = 0
        self.hscroll.value2_update = 1

        self.vscroll.value1_update = 1
        self.vscroll.value2_update = 1

        # Set mindifference, stepsize
        self.hscroll.SetMinDifference(self.xbuffer)
        self.hscroll.SetStepSize(self.xbuffer)

        self.hscroll.SetMinDifference(self.xbuffer)
        self.hscroll.SetStepSize(self.xbuffer)

        # Set max/min and values for h-slider
        self.hscroll.SetMinValue(self.extents_xmin)
        self.hscroll.SetMaxValue(self.extents_xmax)
        self.hscroll.SetValue1(self.extents_xmin)
        self.hscroll.SetValue2(self.extents_xmax)

        # Vertical slider is calculated
        self.AdjustVScroll()

        self.ClearGraph()

    def ClearGraph(self):
        """Clear graph except labels and figure."""
        # If axis exists, clear it, else create
        if hasattr(self, 'axes'):
            self.axes.clear()
        else:
            self.axes = self.figure.add_subplot(111)

        # Setup axis labels depending on counts or percentage displayed
        if self.graphtype == "Raw counts":
            self.axes.set_ylabel('Raw Counts', size=8)
        else:
            self.axes.set_ylabel('Percentage Change (%)', size=8)
        self.axes.set_xlabel('Time (s)', size=8)
        self.axes.grid(True, color='Gray')

        # Set font size of ticks
        setp(self.axes.get_xticklabels(), fontsize=6)
        setp(self.axes.get_yticklabels(), fontsize=6)

        # Plot the initial data
        def plot(colour="Red"):
            return self.axes.plot([0], [0], linewidth=1, color=(colour))[0]
        self.plot_y1 = plot("Red")
        self.plot_y2 = plot("Blue")
        self.plot_y2y1 = plot("Black")
        self.plot_y1_counts = plot("Red")
        self.plot_y2_counts = plot("Blue")

        self.axes.set_xlim(self.display_xmin, self.display_xmax)
        self.axes.set_ylim(self.display_ymin, self.display_ymax)

        # Finally, tight_layout it all
        self.figure.tight_layout(pad=self.figure_gap, w_pad=0, h_pad=0)

        self.Draw()

    def AddFlag(self, flagTime, baseline=False, colour="Red"):
        """Add reference flag to data at current crosshair time."""
        # Log the array and draw
        self.array_flag_times.append(flagTime)
        self.array_flag_colours.append(colour)
        self.flagvert = self.axes.axvline(flagTime, linewidth=1, color=colour)

        # If the flag is a baseline, may need to recalculate everything
        if baseline:

            self.array_baseline_times.append(flagTime)
            self.array_baseline_times = list(set(self.array_baseline_times))
            self.array_baseline_times.sort()

            # If last time -
            # New baseline values for future calculations
            # Recalculate from baseline to end
            if self.array_baseline_times[-1] == flagTime:

                index = self.array_time.index(flagTime)
                self.baseline_y1_counts = self.array_y1_counts[index]
                self.baseline_y2_counts = self.array_y2_counts[index]

            # Recalculate everything
            self.RecalculateArrays()

            self.Draw()

    def RemoveFlag(self, time):
        """Remove flag from all arrays and delete from figure

        Invoked very infrequently so we will simply delete everything and
        redraw.
        """
        # In case there are no flags or the time is wrong
        if time in self.array_flag_times:
            index = self.array_flag_times.index(time)
            self.array_flag_times.pop(index)
            self.array_flag_colours.pop(index)
        else:
            return

        # May also be a baseline
        if time in self.array_baseline_times:
            self.array_baseline_times.sort()
            index = self.array_baseline_times.index(time)

            # If this is the last the baseline for new values
            if time == self.array_baseline_times[-1]:

                # Find the next baseline back
                newBaselineTime = self.array_baseline_times[-2]

                newIndex = self.array_time_1.index(newBaselineTime)

                self.baseline_y1_counts = self.array_y1_counts_1[newIndex]
                self.baseline_y2_counts = self.array_y2_counts_1[newIndex]

            # Finally delete the baseline
            self.array_baseline_times.pop(index)

            # Recalculate if a baseline is removed
            self.RecalculateArrays()

        # Create blank graph
        self.ClearGraph()

        # Redraw the data
        self.Draw()

        # Redraw the crosshair
        self.crosshairvert = self.axes.axvline(
            self.crosshair_time, linewidth=1, color="Green")

        # Draw all the horizontal lines
        for index in range(len(self.array_flag_times)):
            self.axes.axvline(
                self.array_flag_times[index],
                linewidth=1,
                color=self.array_flag_colours[index],
                )

    def RecalculateArrays(self):
        """Recalculate data arrays against new retrospective baselines."""
        # Clear arrays
        # Arrays to store all the data
        self.array_y1_1 = []
        self.array_y1_2 = []
        self.array_y1_4 = []
        self.array_y1_8 = []
        self.array_y1_16 = []

        self.array_y2_1 = []
        self.array_y2_2 = []
        self.array_y2_4 = []
        self.array_y2_8 = []
        self.array_y2_16 = []

        self.array_y2y1_1 = []
        self.array_y2y1_2 = []
        self.array_y2y1_4 = []
        self.array_y2y1_8 = []
        self.array_y2y1_16 = []

        self.array_y1 = []
        self.array_y2 = []
        self.array_y2y1 = []

        # For each value in the counts_1 array, calculate and add to
        # appropriate arrays
        baseline_y1_counts = 0
        baseline_y2_counts = 0

        for i in range(len(self.array_time_1)):

            y1_counts = self.array_y1_counts_1[i]
            y2_counts = self.array_y2_counts_1[i]

            # Check if this time is a baseline
            if self.array_time_1[i] in self.array_baseline_times:
                baseline_y1_counts = y1_counts
                baseline_y2_counts = y2_counts

            # Calculate new percentages
            numerator = y1_counts - baseline_y1_counts
            y1_percent = numerator / float(baseline_y1_counts) * 100.0
            numerator = y2_counts - baseline_y2_counts
            y2_percent = numerator / float(baseline_y2_counts) * 100.0
            y2y1_percent = y2_percent - y1_percent

            # Add everything to the correct array
            self.array_y1_1.append(y1_percent)
            self.array_y2_1.append(y2_percent)
            self.array_y2y1_1.append(y2y1_percent)

            if self.displayratio == "x1":

                self.array_y1.append(y1_percent)
                self.array_y2.append(y2_percent)
                self.array_y2y1.append(y2y1_percent)

            # Length of the full array
            lengthArray = len(self.array_y1_1)

            if lengthArray % 2 == 1:  # x2

                self.array_y1_2.append(y1_percent)
                self.array_y2_2.append(y2_percent)
                self.array_y2y1_2.append(y2y1_percent)

                if self.displayratio == "x2":

                    self.array_y1.append(y1_percent)
                    self.array_y2.append(y2_percent)
                    self.array_y2y1.append(y2y1_percent)

            if lengthArray % 4 == 1:  # x4

                self.array_y1_4.append(y1_percent)
                self.array_y2_4.append(y2_percent)
                self.array_y2y1_4.append(y2y1_percent)

                if self.displayratio == "x4":

                    self.array_y1.append(y1_percent)
                    self.array_y2.append(y2_percent)
                    self.array_y2y1.append(y2y1_percent)

            if lengthArray % 8 == 1:  # x8

                self.array_y1_8.append(y1_percent)
                self.array_y2_8.append(y2_percent)
                self.array_y2y1_8.append(y2y1_percent)

                if self.displayratio == "x8":

                    self.array_y1.append(y1_percent)
                    self.array_y2.append(y2_percent)
                    self.array_y2y1.append(y2y1_percent)

            if lengthArray % 16 == 1:  # x16

                self.array_y1_16.append(y1_percent)
                self.array_y2_16.append(y2_percent)
                self.array_y2y1_16.append(y2y1_percent)

                if self.displayratio == "x16":

                    self.array_y1.append(y1_percent)
                    self.array_y2.append(y2_percent)
                    self.array_y2y1.append(y2y1_percent)

    def ChangeGraphType(self, newtype):
        """Display counts, single sensor etc."""
        # Clear any graphs that are currently showing
        self.plot_y1.set_data(([0], [0]))
        self.plot_y2.set_data(([0], [0]))
        self.plot_y2y1.set_data(([0], [0]))
        self.plot_y1_counts.set_data(([0], [0]))
        self.plot_y2_counts.set_data(([0], [0]))

        self.graphtype = newtype
        self.AdjustVScroll()
        self.ClearGraph()
        self.Draw()

    def Draw(self, blit=False):
        """Plot data depending on graph display type."""
        self.axes.set_xlim(self.display_xmin, self.display_xmax)
        self.axes.set_ylim(self.display_ymin, self.display_ymax)

        if self.graphtype == "Both sensors":
            self.plot_y1.set_data((self.array_time, self.array_y1))
            self.plot_y2.set_data((self.array_time, self.array_y2))
        elif self.graphtype == "Sensor 1 only":
            self.plot_y1.set_data((self.array_time, self.array_y1))
        elif self.graphtype == "Sensor 2 only":
            self.plot_y2.set_data((self.array_time, self.array_y2))
        elif self.graphtype == "Subtracted (2-1)":
            self.plot_y2y1.set_data(self.array_time, self.array_y2y1)
        elif self.graphtype == "Raw counts":
            self.plot_y1.set_data((self.array_time, self.array_y1_counts))
            self.plot_y2.set_data((self.array_time, self.array_y2_counts))
        self.canvas.draw()


class panel_AcquisitionGraph_test(wx.App):
    """Class for testing panel_AcquisitionGraph."""
    def __init__(self, auto_close=True):
        """Place panel in frame and test opening, plotting data and closing."""
        super(panel_AcquisitionGraph_test, self).__init__()

        # Setup frame and sizers
        frame = wx.Frame(None)
        sizer = wx.BoxSizer(wx.VERTICAL)
        graph_pan = panel_AcquisitionGraph(frame, wx.ID_ANY)
        sizer.Add(graph_pan, 1, wx.EXPAND | wx.ALL, 20)
        frame.Show()
        frame.SetSizerAndFit(sizer)
        frame.Maximize()
        frame.Layout()  # Displays the figure correctly

        # List of functions to run [function, arguments]
        tests = [
            [graph_pan.ReceiveTwoValues, (1, 1000, 1000, )],
            [graph_pan.ReceiveTwoValues, (2, 1001, 999, )],
            [graph_pan.ReceiveTwoValues, (3, 1002, 998, )],
            [graph_pan.ReceiveTwoValues, (4, 1003, 997, )],
            [graph_pan.ReceiveTwoValues, (5, 1004, 996, )],
            [graph_pan.AddFlag, (2, True, )],
            [graph_pan.AddFlag, (4, True, )],
            [graph_pan.RemoveFlag, (2, )],
            [graph_pan.ChangeGraphType, ('Sensor 1 only', )],
            [graph_pan.ChangeGraphType, ('Sensor 2 only', )],
            [graph_pan.ChangeGraphType, ('Subtracted (2-1)', )],
            [graph_pan.ChangeGraphType, ('Raw counts', )],
            [graph_pan.ChangeGraphType, ('Both sensors', )],
            ]
        for function, arguments in tests:
            sleep(0 if auto_close else 1)
            function(*arguments)

        if auto_close:
            self.TopWindow.Destroy()


if __name__ == '__main__':
    panel_AcquisitionGraph_test(False).MainLoop()
